﻿using Dodsorsaker;
using Dodsorsaker.Model;
using System.Text;
using System.Text.Json;

var DEATHFILEPATH = @"c:\RCC\INCA_DOD_2023.csv";
var ORG = "SBR";
var ROLE = "extInrapportor";
var USER = "EXT_mig";
var THUMBPRINT = "0f823e4ebb56cd8c6a85f8c3f5f713d74642129a";
var URLVDSTRING = "https://psykiatri.incanet.se:8443/api/External/Registers/SBR3/Forms/direkt-inrapp/ValueDomainLists/dodsorsaker/Values";
var URLREGISTERRECORDSTRING = "https://psykiatri.incanet.se:8443/api/External/Registers/SBR3/Forms/direkt-inrapp/RegisterRecords";


try
{
    var clientCert = CertClient.GetCertificateFromSITHSCard(THUMBPRINT);
    var vdClient = CertClient.CreateHttpClient(clientCert, ORG, USER, ROLE, "Demo - Demo1 - Demo1");
    var vdResponse = await vdClient.PostAsync(new Uri(URLVDSTRING), null);
    var vdResponseString = await vdResponse.Content.ReadAsStringAsync();
    var deathPersonsVD = JsonSerializer.Deserialize<List<IncaDeathVD>>(vdResponseString);

    List<DeathCauseFile> deathCausesFile = Dodsorsaker.Utils.ReadDodsOrsak(DEATHFILEPATH);
    List<INCA> result = new List<INCA>();

    var drugsAlcoholList = new List<ValueList>()
    {
        new ValueList() { id = 18309, value = "0"},
        new ValueList() { id = 18310, value = "1"},
        new ValueList() { id = 18311, value = "2"},
        new ValueList() { id = 18312, value = "3"}
    };

    var codeList = new List<ValueList>()
    {
        new ValueList() { id = 18056, value = "1"},
        new ValueList() { id = 18057, value = "2"},
        new ValueList() { id = 18058, value = "3"},
        new ValueList() { id = 18059, value = "4"},
        new ValueList() { id = 18060, value = "5"},
        new ValueList() { id = 18061, value = "0"}
    };

    foreach (var deathCauseFile in deathCausesFile)
    {
        foreach (var deathPersonVD in deathPersonsVD)
        {
            if(deathCauseFile.pnr == deathPersonVD.data.PERSNR)
            {
                var inca = new INCA() { position = deathPersonVD.data.CREATEDBY_FULLCODE, root = new List<RootIncaDeathJson>() };

                var regvars = new IncaDeathJsonRegvars()
                {
                    registerRecordId = deathPersonVD.data.REGISTERRECORD_ID,
                    regvars = new IncaDeathJsonData()
                    {
                        dodsorsak_datum = new Regvar() { value = deathCauseFile.dodsdatum },
                        dodsorsak_droger = new Regvar() { value = Utils.GetId(drugsAlcoholList, deathCauseFile.drog) },
                        dodsorsak_kod = new Regvar() { value = Utils.GetId(codeList, deathCauseFile.kod) },
                    }
                };

                var rootIncaDeath = new RootIncaDeathJson(){ data = new List<IncaDeathJsonRegvars>() };

                rootIncaDeath.data.Add(regvars);
                inca.root.Add(rootIncaDeath);
                result.Add(inca);

                break;
            }
        }
    }

    deathCausesFile = null;
    deathPersonsVD = null;
    vdClient = null;

    foreach(var record in result)
    {
        try
        {
            var restClient = CertClient.CreateHttpClient(clientCert, ORG, USER, ROLE, record.position);
            var jsonData = JsonSerializer.Serialize(record.root);
            var jsonContent = new StringContent(jsonData, Encoding.UTF8, "application/json");
            var response = await restClient.PatchAsync(new Uri(URLREGISTERRECORDSTRING), jsonContent);
            var responseString = await response.Content.ReadAsStringAsync();
            Console.WriteLine("ok");

        }
        catch (Exception exx)
        {
            using (StreamWriter sWr = new StreamWriter("Exceptions.txt", true))
            {
                sWr.WriteLine("ID: " + record.root[0].data[0].registerRecordId + " " + exx.Message);
            }
        }
    }
    Console.WriteLine(result);
}
catch(Exception ex)
{
    Console.WriteLine(ex.Message);
}

