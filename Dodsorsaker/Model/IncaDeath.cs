﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dodsorsaker.Model
{
    public class ValueList
    {
        public int id { get; set; }
        public string value { get; set; }
    }

    public class IncaDeathVD
    {
        public int id { get; set; }
        public IncaDeathVDData data { get; set; }
    }

    public class IncaDeathVDData
    {
        public string PERSNR { get; set; }
        public string CREATEDBY_FULLCODE { get; set; }
        public int REGISTERRECORD_ID { get; set; }
    }

    public class DeathCauseFile
    {
        public string pnr { get; set; }
        public string dodsdatum { get; set; }
        public string drog { get; set; }
        public string kod { get; set; }
    }

    public class INCA
    {
        public string position { get; set; }
        public List<RootIncaDeathJson> root { get; set; }
    }

    public class Patient
    {
        public string socialSecurityNumber { get; set; }
    }
    public class RootIncaDeathJson
    {
     //   public Patient patient { get; set; }
        public List<IncaDeathJsonRegvars> data { get; set; }
    }

    public class IncaDeathJsonRegvars
    {
        public int registerRecordId { get; set; }
        public IncaDeathJsonData regvars { get; set; }

    }
    public class IncaDeathJsonData
    {
        public Regvar dodsorsak_datum { get; set; }
        public Regvar dodsorsak_droger { get; set; }
        public Regvar dodsorsak_kod { get; set; }
    }

    public class Regvar
    {
        public string value { get; set; }
    }
}
