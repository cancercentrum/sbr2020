﻿using Dodsorsaker.Model;

namespace Dodsorsaker
{
    internal class Utils
    {
        public static string GetId(List<ValueList> valueList, string value)
        {
            var res = -1;
            foreach (var item in valueList)
            {
                if (item.value == value)
                {
                    res = item.id;
                    break;
                }
            }
            return res != -1 ? res.ToString() : "";
        }
        public static List<DeathCauseFile> ReadDodsOrsak(string filepath)
        {
            string[] dodsOrsakVars = new string[4]
            {
                "PNR",
                "DODSDAT",
                "droger_alkohol",
                "dodsorsak_kod",
            };
            var dodsorsaker = new List<DeathCauseFile>();
            var counter = 0;
            string[] cols = null;
            foreach (string line in System.IO.File.ReadLines(filepath))
            {
                if (counter == 0)
                {
                    cols = line.Split(',');
                }
                if (counter > 0)
                {
                    string[] row = line.Split(',');
                    var record = new DeathCauseFile();
                    var pnr = row[Array.IndexOf(cols, "PNR")];
                    record.pnr = pnr.Substring(0, 8) + "-" + pnr.Substring(8, 4);
                    record.dodsdatum = row[Array.IndexOf(cols, "DODSDAT")];
                    record.drog = row[Array.IndexOf(cols, "droger_alkohol")];
                    var kod = row[Array.IndexOf(cols, "dodsorsak_kod")];

                    if(kod == "1")
                    {
                        record.kod = "2";
                    }
                    else if(kod == "2")
                    {
                        record.kod = "1";
                    }
                    else
                    {
                        record.kod = kod;
                    }

                    dodsorsaker.Add(record);
                }
                counter++;
            }
            return dodsorsaker;
        }
    }
}
