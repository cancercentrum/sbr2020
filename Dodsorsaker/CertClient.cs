﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Cryptography.X509Certificates;


namespace Dodsorsaker
{
    internal class CertClient
    {
        public static HttpClient CreateHttpClient(X509Certificate2 clientCert, string org, string user, string role, string? position)
        {
            var handler = new HttpClientHandler();
            handler.ClientCertificates.Add(clientCert);

            var client = new HttpClient(handler);

            client.DefaultRequestHeaders.Add("Organization", org);
            client.DefaultRequestHeaders.Add("User", user);
            client.DefaultRequestHeaders.Add("Role", role);
            client.DefaultRequestHeaders.Add("PositionFullCode", position);

            return client;
        }

        public static X509Certificate2 GetCertificateFromSITHSCard(string thumbprint)
        {
            X509Certificate2 cert = null;
            X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
            try
            {
                cert = store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false).First<X509Certificate2>();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            store.Close();
            return cert;
        }
    }
}
