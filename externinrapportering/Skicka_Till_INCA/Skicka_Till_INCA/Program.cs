using Skicka_Till_INCA;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddTransient<Deliver>();
        services.AddTransient<DeliveryInfo>();
    })
    .Build();

var my = host.Services.GetRequiredService<DeliveryInfo>();
var skickaTillINCA = host.Services.GetRequiredService<Deliver>();

skickaTillINCA.Run(args);

