﻿using System;
using System.Xml;
using System.Xml.Schema;

namespace Skicka_Till_INCA
{
    class XmlSchemaValidator
    {
        private string path;

        public XmlSchemaValidator(string path)
        {
            this.path = path;
        }
        public void Validate()
        {
            XmlReaderSettings booksSettings = new XmlReaderSettings();
            booksSettings.Schemas.Add("http://schemas.datacontract.org/2004/07/inca.external", "incaExternal.xsd");
            booksSettings.ValidationType = ValidationType.Schema;
            booksSettings.ValidationEventHandler += new ValidationEventHandler(booksSettingsValidationEventHandler);

            XmlReader books = XmlReader.Create(path, booksSettings);

            while (books.Read()) { }
        }

        void booksSettingsValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Warning)
            {
                Console.Write("WARNING: ");
                
                Console.WriteLine(e.Message);
            }
            else if (e.Severity == XmlSeverityType.Error)
            {

                throw new XmlSchemaValidationException("\nERROR: " + e.Message + "@ line: " + e.Exception.LineNumber);

            }
        }
    }
}


