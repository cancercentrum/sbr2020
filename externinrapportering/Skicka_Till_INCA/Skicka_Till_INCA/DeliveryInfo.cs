using INCA;
using Newtonsoft.Json;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Text.Json;
using System.Xml;
using System.Xml.Linq;

namespace Skicka_Till_INCA
{
    public class DeliveryInfo
    {
        private readonly ILogger<DeliveryInfo> _logger;

        public DeliveryInfo(ILogger<DeliveryInfo> logger)
        {
            _logger = logger;

        }
        static void printTableDefinition(TableDefinition td, int indent)
        {
            string indentString = new string(' ', indent);

            Console.WriteLine(indentString);
            Console.WriteLine("[" + td.TableName + "]");

            foreach (var v in td.VariableDefinitions)
            {
                Console.WriteLine(indentString);
                Console.WriteLine(" -" + v.Name);

                if (v.GetType().Name == "ListVariableDefinition")
                {
                    var list = (ListVariableDefinition)v;
                    foreach (var element in list.ListValues)
                        Console.WriteLine(" --" + element.Description + ":" + element.Value);
                }
            }

            foreach (var child in td.Children)
            {
                printTableDefinition(child, indent + 1);
            }
        }

        static void printXML(TableDefinition td, XElement curr)
        {
     
            var node = new XElement(td.TableName);


            //curr.Add(new XElement(td.TableName + "s", node));
            curr.Add(node);



            foreach (var v in td.VariableDefinitions)
            {
                node.Add(new XElement(v.Name, v.GetType().Name.Replace("VariableDefinition", "")));
               

                /*if (v.GetType().Name == "ListVariableDefinition")
                {
                    var list = (ListVariableDefinition)v;
                    foreach (var element in list.ListValues)
                        Console.WriteLine(" --" + element.Description + ":" + element.Value);
                }*/
            }
            
            foreach (var child in td.Children)
            {
                printXML(child, node);
            }
        }

        static void toFormData(TableDefinition td, FormData fd)
        {

            fd.TableName = td.TableName;


            //curr.Add(new XElement(td.TableName + "s", node));
           

            List<Variable> variables = new List<Variable>();

            foreach (var v in td.VariableDefinitions)
            {
                variables.Add(new Variable() { Name = v.Name, Value = v.GetType().Name.Replace("VariableDefinition", "") });


                /*if (v.GetType().Name == "ListVariableDefinition")
                {
                    var list = (ListVariableDefinition)v;
                    foreach (var element in list.ListValues)
                        Console.WriteLine(" --" + element.Description + ":" + element.Value);
                }*/
            }
            fd.Variables = variables.ToArray();
            foreach (var child in td.Children)
            {
                var c = new FormData();
                List<FormData> l = new List<FormData>();

                if (fd.Children != null)
                {
                    l = fd.Children.ToList<FormData>();

                }

                l.Add(c);
                fd.Children = l.ToArray<FormData>();
                toFormData(child, c);
            }
        }

        private X509Certificate2 GetCertificateFromSITHSCard(string thumbprint)
        {
            X509Certificate2 cert;
            X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
            try
            {



                cert = store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false).First<X509Certificate2>();

            }
            catch (Exception e)
            {

                throw new Exception($"Could not find certificate with thumbprint: {thumbprint} ex: {e}");

            }

            store.Close();
            return cert;
        }


        public void Go(string[] args, Action<string[]> service)
        {
            using (var client = new FormServiceClient())
            {
                service(args);

                var cert = GetCertificateFromSITHSCard("6dcf5f6ed579b42ab9f0d3759a0fd6e89b328706");
                client.ClientCredentials.ClientCertificate.Certificate = cert;
                TableDefinition TD;
                ValueDomainValue[] VD; 
                string hej;
                ConnectionDefinition pc;
                ConnectionDefinition cd;
                client.Endpoint.Address = new EndpointAddress("https://psykiatri.incanet.se/Services/V2/FormService.svc");
                client.Endpoint.Binding.SendTimeout = TimeSpan.FromSeconds(30);
                client.Endpoint.Binding.ReceiveTimeout = TimeSpan.FromSeconds(30);
                var resp =client.GetFormStructure(
                    "SBR", 
                    "Demo1", 
                    null, 
                    "Inrapportör (Tjänstekontrakt)", 
                    "EXT_SBR",
                    "SBR_2020_EXT", 
                    out hej, 
                    out TD,
                    out pc,
                    out cd
                );
                /*var vd = client.GetValueDomainContent(
                    "SBR",
                    "Demo1",
                    null,
                    "Inrapportör (Tjänstekontrakt)",
                    "EXT_SBR",
                    "SBR_2020_EXT", null, null, "diagnosVD", out hej, out VD
                   );*/

                FormData FD = new FormData();


                toFormData(TD, FD);
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(FD.GetType());
                StringWriter s = new StringWriter();
                x.Serialize(s, FD);
                printTableDefinition(TD, 2);
                XElement n = new XElement("INCA");
                n.Add(new XElement("Läkemedel"));
                var root = new XElement("INCA");
                printXML(TD, root);

                Console.WriteLine(root);

                //doc.LoadXml(s.ToString());
                //string jsonText = JsonConvert.SerializeXmlNode(doc);

                File.WriteAllText("FD.xml", s.ToString());
                root.Save("INCA.xml");

                Console.ReadLine();


            }
                Console.WriteLine("Hej");
        }

     
    }
}