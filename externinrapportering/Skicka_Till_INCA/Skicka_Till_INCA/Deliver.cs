﻿using INCA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Skicka_Till_INCA
{
    public class Deliver
    {
        private  readonly ILogger<Deliver> _logger;
        private  readonly IConfiguration _conf;
        private  bool _AutoClose = false;
        private  FormServiceClient client;
        private  Encoding _Encoding;

        public  Deliver(ILogger<Deliver> logger, IConfiguration conf)
        {
            _logger = logger;
            _conf = conf;
            client = new FormServiceClient();

        }

        public  void Run (string[] args)
        {
            try { App_Start(args); } catch (Exception ex) { Console.WriteLine("Ett fel har inträffat och programmet måste avslutas. " + ex.Message); }
            if (!_AutoClose)
            {
                Console.WriteLine("");
                Console.WriteLine("Tryck på valfri tangent för att avsluta.");
                Console.ReadKey();
            }
        }
        public  void App_Start(string[] args)
        {
            var fileList = new List<INCAFile>();
            var password = string.Empty;
            var encoding_str = "ISO-8859-1";

            Console.WriteLine("INCA överföringsprogram, version " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());

            if (args.Length == 0)
            {
                string exeName = System.IO.Path.GetFileName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                Console.WriteLine("Användning:");
                Console.WriteLine(exeName + " fil1 [/autoclose] [/encoding:{teckenkodning}]");
                Console.WriteLine("Exempel: " + exeName + @" ""C:\CAS111.xml"" ""C:\CAS112.xml"" /autoclose /encoding:ISO-8859-1");
                Console.WriteLine("============");
                Console.WriteLine("[/autoclose] - Stänger automatiskt programmet vid avslut.");
                Console.WriteLine("[/encoding:{teckenkodning}] - Teckenkodning, standardvärde: ISO-8859-1");
                return;
            }

            foreach (var arg in args)
            {
                var arg_l = arg.ToLower();
                if (arg_l == "/autoclose")
                {
                    _AutoClose = true;
                }
                else if (arg_l.StartsWith("/encoding:"))
                {
                    encoding_str = GetArgValue(arg, "encoding");
                }
                else
                {
                    try
                    {
                        fileList.Add(new INCAFile(arg));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(arg + " | " + ex.Message);
                    }

                }
            }

            try
            {
                _Encoding = Encoding.GetEncoding(encoding_str);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return;
            }

            if (fileList.Count == 0)
            {
                Console.WriteLine("Inga filer att skicka.");
            }
            else
            {
                X509Certificate2 cert;
                try
                {
                    cert = GetCertificateFromSITHSCard(_conf["thumbprint"]);
                    Console.WriteLine("Certifikatet är giltigt till " + cert.NotAfter.ToString());

                    var baseUri = new Uri(_conf["BaseUrl"]);
                    var uri = new Uri(baseUri, "/Services/V2/FormService.svc");
                    var url = uri.ToString();
                    //client;
                    client.ClientCredentials.ClientCertificate.Certificate = cert;
                    client.Endpoint.Address = new System.ServiceModel.EndpointAddress(url);

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
                    return;
                }

                for(int i = 0; i < fileList.Count; i++) 
                {
                    var file = fileList[i];
                    try
                    {
                        Console.WriteLine($"Skickar {file.FileName} (fil {i + 1} av {fileList.Count})");
                        SendFile(file);
                        Console.WriteLine(file.FileName + " | Överföring lyckades.");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(file.FileName + " | " + ex.Message);
                    }
                }
            }

        }

        private static string GetArgValue(string arg, string name)
        {
            return arg.Substring(name.Length + 2);
        }

        private static X509Certificate2 GetCertificate(string password)
        {
            return new X509Certificate2();
        }

        private static X509Certificate2 GetCertificateFromSITHSCard(string thumbprint)
        {
            X509Certificate2 cert;
            X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
            try
            {
                cert = store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false).First<X509Certificate2>();
            }
            catch (Exception e)
            {

                throw new Exception($"Could not find certificate with thumbprint: {thumbprint} ex: {e}");

            }

            store.Close();
            return cert;
        }
     

        private  void SendFile(INCAFile f)
        {
            var xmlstr = string.Empty;
            using (var rdr = new StreamReader(f.FileName, _Encoding))
            {
                xmlstr = rdr.ReadToEnd();
            }

            ResultCodeEnum result = ResultCodeEnum.ERROR;

            Patient p = new Patient() { CountryCode = "SE", SocialSecurityNumber = "191212121212" };
            FormData fd = new FormData();
            INCAFoo po;
            XmlSerializer serializer = new XmlSerializer(typeof(INCAFoo));
            TextReader ms = new StringReader(xmlstr);
            //var reader = new System.Xml.XmlTextReader(ms) { Namespaces = false };
            var demo = new XmlSchemaValidator(f.FileName);
            demo.Validate();
            po = (INCAFoo) serializer.Deserialize(ms);

            

            string comm = "";
            string[] vali = {};
            try
            {
                for (int i = 0; i < po.Posts.Count; i++)
                {
                    result = client.SubmitFormData(
                   _conf["Organization"],
                   po.Posts[i].Position,
                   null,
                   _conf["Role"],
                   _conf["User"],
                   _conf["FormContractId"],
                   new Patient() { SocialSecurityNumber= po.Posts[i].Patient, CountryCode="SE"},
                   po.Posts[i].fd,
                   null,
                   ref comm,
                   null,
                   out vali
               );
                    Console.WriteLine($"Skickar post {i + 1} av {po.Posts.Count}");
                }
                //cytburken = System.Threading.Timeout.Infinite;
               // cytburken.SkickaLeverans(f.Type, f.LabId, f.DeliveryNumber, xmlstr);*/
            }
            catch (Exception ex)
            {
                throw new Exception("Fel vid anrop av SkickaLeverans: " + ex.Message);
            }

            if (result == ResultCodeEnum.ERROR)
            {
                throw new Exception("Överföring misslyckades:\n" + string.Join("\n", vali));
            }

        }

        
    }
}
[XmlRoot(
    Namespace = "http://schemas.datacontract.org/2004/07/inca.external", 
    ElementName = "INCAFoo"
)]
public class INCAFoo
{
    [XmlElement("Post")]
    public List<Post> Posts { get; set; }
}
[Serializable]
[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.datacontract.org/2004/07/inca.external")]
public class Post
{
    [XmlElement(IsNullable =false)]
    public string Position { get; set; }
    public string Patient { get; set; }
    [XmlElement("FormData")]
 
    public FormData fd { get; set; }

}
public class INCAFile
{
    public string FileName { get; set; }

    public List<Post> posts { get; set;}

    public INCAFile() { }
    public INCAFile(string arg)
    {
        if (File.Exists(arg))
        {
            this.FileName = arg;
        }
        else
        {
            throw new Exception($"Filen: {arg} kunde inte hittas.");
        }
    }

}
