﻿using DataToInca.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataToInca
{
    internal class Utils
    {
        public static List<DeathCause> ReadDodsOrsak(string filepath)
        {
            string[] dodsOrsakVars = new string[4]
            {
                "PNR",
                "DODSDAT",
                "droger_alkohol",
                "dodsorsak_kod",
            };
            var dodsorsaker = new List<DeathCause>();
            var counter = 0;
            string[] cols = null;
            foreach (string line in System.IO.File.ReadLines(filepath))
            {
                if(counter == 0)
                {
                    cols = line.Split(',');
                }
                if (counter > 0)
                {
                    string[] row = line.Split(',');
                    var dOrsak = new DeathCause();
                    var pnr = row[Array.IndexOf(cols, "PNR")];
                    dOrsak.pnr = pnr.Substring(0, 8) + "-" + pnr.Substring(8, 4);
                    dOrsak.datum = row[Array.IndexOf(cols, "DODSDAT")];
                    dOrsak.droger = row[Array.IndexOf(cols, "droger_alkohol")];
                    dOrsak.kod = row[Array.IndexOf(cols, "dodsorsak_kod")]; ;
                    dodsorsaker.Add(dOrsak);
                }
                counter++;
            }
            return dodsorsaker;
        }

        public static List<IncaDeath> ReadIncaDeaths(string filepath)
        {
            string[] incaVars = new string[3]
            {
                "CREATEDBY_FULLCODE",
                "PERSNR",
                "R2080T28275_ID",
            };
            var incaDeaths = new List<IncaDeath>();
            var counter = 0;
            string[] cols = null;
            foreach (string line in System.IO.File.ReadLines(filepath))
            {
                if (counter == 0)
                {
                    cols = line.Split(',');
                }
                if (counter > 0)
                {
                    string[] row = line.Split(';');
                    var death = new IncaDeath();
                    death.personnummer = row[Array.IndexOf(cols, "PERSNR")];
                    death.pk = row[Array.IndexOf(cols, "R2080T28275_ID")];
                    death.position = row[Array.IndexOf(cols, "CREATEDBY_FULLCODE")];
                    incaDeaths.Add(death);
                }
                counter++;
            }
            return incaDeaths;
        }
    }

}
