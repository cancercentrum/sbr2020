﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataToInca
{
    internal class IncaDeath
    {
        public string personnummer { get; set; }
        public string position { get; set; }
        public string pk { get; set; }
        public string dodsorsak_datum { get; set; }
        public string dodsorsak_droger { get; set; }
        public string dodsorsak_kod { get; set; }
    }
}
