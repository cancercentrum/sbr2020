﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataToInca.Model
{
    internal class DeathCause
    {
        public string pnr { get; set; }
        public string datum { get; set; }
        public string kod { get; set; }
        public string droger { get; set; }
    }
}
