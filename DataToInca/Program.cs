﻿using Psykiatri;
using DataToInca;
using DataToInca.Model;

var THUMBPRINT = "0f823e4ebb56cd8c6a85f8c3f5f713d74642129a";
var INCAFILEPATH = @"c:\RCC\incadeaths.csv";
var DEATHFILEPATH = @"c:\RCC\INCA_DOD_2023-01-13.csv";

List<DeathCause> harjeRes = Utils.ReadDodsOrsak(DEATHFILEPATH);
List<IncaDeath> incaRes = Utils.ReadIncaDeaths(INCAFILEPATH);
List<IncaDeath> res = new List<IncaDeath>();


for (var i = 0; i < harjeRes.Count; i++)
{
    for (var j = 0; j < incaRes.Count; j++)
    {
        if (harjeRes[i].pnr == incaRes[j].personnummer)
        {
            var deathPatient = new IncaDeath()
            {
                pk = incaRes[j].pk,
                position = incaRes[j].position,
                personnummer = incaRes[j].personnummer,
                dodsorsak_datum = harjeRes[i].datum,
                dodsorsak_droger = harjeRes[i].droger,
            };
            if (harjeRes[i].kod == "1")
                deathPatient.dodsorsak_kod = "2";
            else if (harjeRes[i].droger == "2")
                deathPatient.dodsorsak_kod = "1";
            else
                deathPatient.dodsorsak_kod = harjeRes[i].kod;
            res.Add(deathPatient);
            break;
        }
    }
}

incaRes = null;

for (var i =0; i< res.Count; i++) 
{ 
    try
    {
        var rootVariables = new List<Variable>();
        rootVariables.Add(new Variable { Name = "dodsorsak_datum", Value = res[i].dodsorsak_datum });
        rootVariables.Add(new Variable { Name = "dodsorsak_kod", Value = res[i].dodsorsak_kod });
        rootVariables.Add(new Variable { Name = "dodsorsak_droger", Value = res[i].dodsorsak_droger });
        var formdata = new FormData { TableName = "BehandlingUppfoljning", Variables = rootVariables.ToArray() };
        var formdataRequest = new SubmitFormDataRequest()
        {
            Organization = "SBR",
            Role = "Inrapportör (Tjänstekontrakt)",
            User = "EXT_mig",
            PositionFullCode = res[i].position,
            FormContractId = "BB_Dodsorsaker",
            ConnectionData = new ConnectionData
            {
                ConnectionType = ConnectionType.Update,
                ConnectionTypeSpecified = true,
                ConnectionVariables = new Variable[]
                {
                    new Variable { Name = "R2080T28275_ID", Value = res[i].pk }
                }
            },
            Patient = new()
            {
                CountryCode = "SE",
                SocialSecurityNumber = res[i].personnummer,
            },
            FormData = formdata,
        };
        await ExtService.SubmitFormData(formdataRequest, THUMBPRINT);
    }
    catch(Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
}