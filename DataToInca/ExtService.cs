﻿using Psykiatri;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;

namespace DataToInca
{
    public static class ExtService
    {
        private static X509Certificate2 GetCertificateFromSITHSCard(string thumbprint)
        {
            X509Certificate2 cert;
            X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
            try
            {
                cert = store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false).First<X509Certificate2>();
            }
            catch (Exception e)
            {
                throw new ExtServiceException($"Could not find certificate with thumbprint: {thumbprint} ex: {e}");
            }

            store.Close();
            return cert;
        }

        private static FormServiceClient CreateFormServiceClient(string thmbprnt)
        {
            X509Certificate2 clientCert;
            clientCert = GetCertificateFromSITHSCard(thmbprnt);

            var baseUri = new Uri("https://psykiatri.incanet.se/");
            var uri = new Uri(baseUri, "/Services/V2/FormService.svc");
            var url = uri.ToString();
            var client = new FormServiceClient();

            client.Endpoint.Address = new EndpointAddress(url);
            client.ClientCredentials.ClientCertificate.Certificate = clientCert;
            client.Endpoint.Binding.SendTimeout = TimeSpan.FromSeconds(60);
            client.Endpoint.Binding.ReceiveTimeout = TimeSpan.FromSeconds(60);

            return client;
        }

        public static async Task<Dictionary<string, string>[]> GetValueDomainContent(GetValueDomainContentRequest request, string thumbprint)
        {
            using var client = CreateFormServiceClient(thumbprint);

            var resp = await client.GetValueDomainContentAsync(request);

            if (resp.ResultCode == ResultCodeEnum.OK)
            {
                return resp.Values.Select(x => x.Data.ToDictionary(y => y.Name, y => y.Value)).ToArray();
            }

            throw new GetValueDomainException(resp.ResultCode, resp.Comment);
        }

        public static async Task SubmitFormData(SubmitFormDataRequest request, string thumbprint)
        {
            using var client = CreateFormServiceClient(thumbprint);

            var resp = await client.SubmitFormDataAsync(request);

            if (resp.ResultCode != ResultCodeEnum.OK)
            {
                throw new SubmitFormDataException(resp.ResultCode, resp.Comment)
                {
                    ValidationMessages = resp.ValidationMessages ?? Array.Empty<string>(),
                };
            }
            else
            {
                Console.WriteLine(resp.ResultCode);
            }
        }
    }

    public class ExtServiceException : Exception
    {
        public ExtServiceException(string message) : base(message)
        {
        }

        public override string Message => $"Message={base.Message}";
    }

    public class GetValueDomainException : Exception
    {
        public ResultCodeEnum ResultCode { get; }

        public GetValueDomainException(ResultCodeEnum resultCode, string message) : base(message)
        {
            ResultCode = resultCode;
        }

        public override string Message => $"ResultCode={ResultCode}, Message={base.Message}";
    }

    public class SubmitFormDataException : GetValueDomainException
    {
        public string Comment { get; }
        public string[] ValidationMessages { get; set; }

        public SubmitFormDataException(ResultCodeEnum resultCode, string comment) : base(resultCode, comment)
        {
            Comment = comment;
        }

        public override string Message => $"ResultCode={ResultCode}, Comment={Comment}, ValidationMessages={string.Join(";", ValidationMessages)}";
    }
}
