Data som krävs:
1. Fil från Härje med följande variabler: "PNR","DODSDAT", "droger_alkohol","dodsorsak_kod"
2. Fil från INCA med följande variabler: "CREATEDBY_FULLCODE", "PERSNR", "R2080T28275_ID" OBS alla poster i BB!

Hur scriptet funkar:
1. Scriptet läser in Härjes csv fil i programmet via en funktion i Utils.cs
2. Sedan läses INCA filen in på samma sätt i en annan funktion i Utils.cs
3. Sen matchas pnr från INCA med pnr i Härjes fil i en loop
4. När vi får en match sparar vi över dödsorsakerna och inca variablerna i en ny resultatlista
4. Vi skickar sedan data till INCA med metoden SubmitFormDataRequest med request objektet

Hur man använder det:
1. Öppna repot DataToInca i VS2020
2. Ändra CONSTANTER THUMBPRINT, DEATHFILEPATH, INCAFILEPATH till dina specifika
3. Kör igång NetID med ditt SHITSkort
4. Bygg och kör!