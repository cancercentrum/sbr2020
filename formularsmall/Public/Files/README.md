# Information
_prestanda_ branchen på bitbucket presenterar Produktion på INCA
_demo_ branchen på bitbucket presenterar Demo på INCA

# Deploya /Lägga upp filer på intCA125
-- När nya ändringar görs kör grunt i terminalen

#Publika filer
_Produktion_
-- gyn.js läggs i public files (den tidigare tas bort)
_Demo_
-- bytnamn på gyn.js till gyn-demo.js och lägg upp den i publika filer  (den tidigare tas bort)


#Formulär
_Produktion_
-- klipp ut script stycket --> se till att den länkar till gyn.js
-- Kopiera din formular.html fil och klipp ut script stycket och kopiera in det ovan

_Demo_
-- klipp ut script stycket --> se till att den länkar till gyn-demo.js
-- Kopiera din formular.html fil och klipp ut script stycket och kopiera in det ovan
