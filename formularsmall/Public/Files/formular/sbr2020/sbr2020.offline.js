/*! sbr2020 | 2021-11-16 14:17:14 */
window.inca = {
    offline: true,
    user: {
        firstName: 'Test',
        lastName: 'Testsson',
        role: { isReviewer: false },
        region: { id: -1, name: 'Väst' },
        position: { id: -1, fullNameWithCode: 'OC Demo (0) - SU/Sahlgrenska (500010) - Medicinkliniken (501)' }
    },
    on: function () {
    },
    form:
        {
            isReadOnly: false,
            getRegisterRecordData:
                function () {
                    throw new Error('unimplemented');
                },
            getValueDomainValues:
                function (opt) {
                    if (opt.success) {
                        setTimeout(
                            function () {
                                switch (opt.vdlist) {
                                    case 'VD_ICD10_Kapitel':
                                        opt.success(recording.getValueDomainValues["[[\"error\",null],[\"success\",null],[\"vdlist\",\"VD_ICD10_Kapitel\"]]"][0]);
                                    break;
                                    case 'VD_ICD10_Kapitelkoder':
                                        opt.success(recording.getValueDomainValues["[[\"error\",null],[\"parameters\",[[\"kapitel\"," + opt.parameters.kapitel + "]]],[\"success\",null],[\"vdlist\",\"VD_ICD10_Kapitelkoder\"]]"][0]);
                                    break;
                                }
                            }, (1 + Math.random()) * 250);
                    }
                }
        },
    errand: {
        status: {
            val: function () {
                return 'Nytt ärende';
            }
        }, action: {
            selectedText: function () {
                return 'Klar, sänd till RCC';
            }
        }
    },
    serverDate: new Date()
};

inca.form.env = $.extend({}, inca.form.env, { _DISTRICT: '20' });


(function(f){f.RCC={metadataProperty:"rcc",bindingsPrefix:"rcc-"}})(window);(function(f,d,b){var a=b.bindingsPrefix+"included";b.BindingHandlers={};b.BindingHandlers.nearestVariable=function(b,a){for(var c=[].concat(a.$data,a.$parents,a.$root),d=0;d<c.length;d++)if(c[d].hasOwnProperty(b))return c[d][b]};var c=b.bindingsPrefix+"var";d.bindingHandlers[c]={init:function(c,h,g,p,l){var j=h();if(!j[b.metadataProperty])throw Error('The binding "'+b.bindingsPrefix+'var" requires an observable from RCC.ViewModel as argument.');"checkbox"==c.type||"radio"==c.type?d.utils.registerEventHandler(c,
"change",function(){j[b.metadataProperty].accessed(!0)}):(d.utils.registerEventHandler(c,"blur",function(){j[b.metadataProperty].accessed(!0)}),d.utils.registerEventHandler(c,"focusout",function(){j[b.metadataProperty].accessed(!0)}));var k={},n=f(c),q=function(b,a){if(!f.fn.qtip)return{destroy:function(){}};var c=b.data("qtip");b.removeData("qtip");var d=b.qtip(f.extend({},a)).qtip("api");b.data("qtip",c);return d},m={};d.utils.arrayForEach(["$form","$user","$opt"],function(a){m[a]=b.BindingHandlers.nearestVariable(a,
l)});if(!m.$form||!m.$user)throw Error("Unable to find $form or $user in binding context.");m.$opt||(m.$opt={});m.$form.isReadOnly?n.prop(n.is('select, option, button, input[type="checkbox"], input[type="radio"]')?"disabled":"readOnly",!0):m.$user.role.isReviewer&&(k.monitor=q(n,{position:{my:"middle left",at:"middle right"},content:{text:'<input type="checkbox" class="rcc-monitor-include" title="Inkludera">'},suppress:!1,events:{render:function(a,c){d.applyBindingsToNode(c.elements.tooltip.find(".rcc-monitor-include")[0],
{checked:j[b.metadataProperty].include},l)}},hide:{event:!1,inactive:3E3}}),d.computed(function(){d.utils.toggleDomNodeCssClass(c,a,j[b.metadataProperty].include())}));d.computed(function(){if(d.utils.unwrapObservable(m.$opt.displayNames)&&n.is(":visible")){if(!k.name)k.name=q(n,{position:{my:"middle left",at:"middle right"},content:{text:f("<div>").text(j[b.metadataProperty].regvarName).html()},suppress:false,show:{ready:true},hide:{event:false}})}else if(k.name){k.name.destroy();delete k.name}});
"tooltip"in j[b.metadataProperty]&&(j[b.metadataProperty].tooltip.hasOwnProperty("html")?k.tooltip=q(n,{position:{my:"top center",at:"bottom center"},content:{text:'<div class="rcc-tooltip"></div>'},events:{render:function(a,c){d.applyBindingsToNode(c.elements.tooltip.find(".rcc-tooltip")[0],{html:j[b.metadataProperty].tooltip.html},l)}},show:{delay:1E3},hide:{event:"mouseleave"}}):j[b.metadataProperty].tooltip.hasOwnProperty("text")&&n.attr("title",j[b.metadataProperty].tooltip.text));d.utils.domNodeDisposal.addDisposeCallback(c,
function(){for(var a in k)k.hasOwnProperty(a)&&k[a].destroy()})}};d.utils.arrayForEach(["text","value","checked","textInput"],function(a){d.bindingHandlers[b.bindingsPrefix+a]={init:function(h,g,f,l,j){l=g();if(!d.isObservable(l)||!l[b.metadataProperty])throw Error(b.bindingsPrefix+a+" requires an observable from RCC.ViewModel as argument.");var k={};d.utils.arrayForEach([a,c],function(c){if(c in f())throw Error("Cannot bind "+b.bindingsPrefix+a+" and "+c+" to the same element.");k[c]=g()});d.applyBindingsToNode(h,
k,j)}}});d.bindingHandlers[b.bindingsPrefix+"list"]={init:function(a,h,g,f,l){var j=h(),h=j[b.metadataProperty];if(!h||!h.term||!h.term.listValues)throw Error('The binding "'+b.bindingsPrefix+'list" requires a list observable from RCC.ViewModel as argument.');g=g();h=h.term.listValues.filter(function(a){var b=new Date(a.validFrom),c=new Date,d=new Date(a.validTo);return j()&&j().id===a.id||!a.validFrom||!a.validTo||b<=c&&d>=c});j[b.metadataProperty].validation&&j[b.metadataProperty].validation.add(d.computed(function(){var a=
j();if(a&&a.validFrom&&a.validTo){var b=new Date(a.validFrom),c=new Date,a=new Date(a.validTo),d=function(a){var b=function(a){return 10>a?"0"+a:a};return a.getFullYear()+"-"+b(a.getMonth())+"-"+b(a.getDate())};if(b>c)return{result:"failed",fatal:!1,message:"Listv\u00e4rdet b\u00f6rjar g\u00e4lla f\u00f6rst "+d(b)};if(a<c)return{result:"failed",fatal:!1,message:"Listv\u00e4rdet utgick "+d(a)}}}));h={value:j,options:h,optionsText:"text",optionsCaption:"\u2013 V\u00e4lj \u2013"};h[c]=j;for(var k in h)h.hasOwnProperty(k)&&
g.hasOwnProperty(k)&&(h[k]=g[k]);d.applyBindingsToNode(a,h,l)}}})(window.jQuery,window.ko,window.RCC);(function(f,d,b){var a={};b.ViewModel=function(b){b||(b={});b.incaForm||(b.incaForm=f.form);if(!("rootTable"in b)){var e=[],h={},g;for(g in b.incaForm.metadata)if(b.incaForm.metadata.hasOwnProperty(g)){e.push(g);for(var p=b.incaForm.metadata[g].subTables,l=0;l<p.length;l++)h[p[l]]=!0}e=d.utils.arrayFilter(e,function(b){return!h[b]});if(1==e.length)b.rootTable=e[0];else throw Error('Must supply "rootTable" option for RCC.ViewModel ('+(0==e.length?"no candidates found":"multiple candidates found: "+
e.join(", "))+").");}b.incaUser||(b.incaUser=f.user);b.events||(b.events={});b.registerVariableDocumentation||(b.registerVariableDocumentation=d.observableArray(),b.registerId&&(e=f.getBaseUrl()+"api/Registers/"+b.registerId+"/RegisterVariables/Documentation",$.get(e).done(function(a){typeof a=="string"&&(a=JSON.parse(a));b.registerVariableDocumentation(a)})));e=a.createViewModel(b.rootTable,b.incaForm.data,b.incaForm.metadata,b.incaForm.createDataRow.bind(b.incaForm),[],b);e.$events=b.events;e.$env=
b.incaForm.env;e.$form=b.incaForm;e.$user=b.incaUser;e.$opt=b.opt||{};b.validation&&(e.$validation=b.validation,b.validation.track(e));return e};b.ViewModel.fill=function(b){return a.fillViewModel(b.target,b.data)};b.ViewModel.prototype.fill=function(b){return a.fillViewModel(this,b)};(function(a){var e,h,g,f,l,j,k,n,q,m,r,v,s,t;e="$$";h="$vd";g=b.metadataProperty;f="add";l="remove";j="include";k="accessed";n="regvar";q="regvarName";m="term";r="tooltip";v="documentation";s="dependencies";t="dependenciesFulfilled";
var u=function(){function b(a,c,e,h,i){var f=c[h].value,i=i||h;"list"===e.dataType?f=d.utils.arrayFirst(e.listValues,function(a){return a.id===f})||void 0:"decimal"===e.dataType&&"number"===typeof f&&(f=f.toFixed(e.precision).replace(/\./,","));a[i]=d.observable(f);a[i].subscribe(function(a){c[h].value=a&&"list"===e.dataType?a.id:a});if(g in a[i])throw Error('The created ko.observable already has a property with name "'+g+'"');a[i][g]={};a[i][g][j]=d.observable(c[h].include);a[i][g][j].subscribe(function(a){c[h].include=
a});a[i][g][k]=d.observable(!1);a[i][g][s]=d.observableArray();a[i][g][t]=d.computed(function(){var b=a[i][g][s]();return b&&b.reduce(function(a,b){return a&&b()},!0)});a[i][g][t].subscribe(function(b){b||(a[i](null),a[i][g][k](!1))})}function a(c,e,h,f){var i,j;for(i in h.regvars)h.regvars.hasOwnProperty(i)&&(j=h.terms[h.regvars[i].term],b(c,e.regvars,j,i,void 0),c[i][g][n]=h.regvars[i],c[i][g][m]=j,c[i][g][q]=i,function(){var a=i;c[a][g][v]=d.computed(function(){return _.find(f.registerVariableDocumentation(),
function(b){return b.shortName==a})})}(),h.regvars[i].description&&f&&f.opt&&("html"==f.opt.tooltips?c[i][g][r]={html:d.observable(h.regvars[i].description)}:"text"==f.opt.tooltips&&(c[i][g][r]={text:h.regvars[i].description})),f&&f.validation&&f.validation.init(c[i],{source:{viewModel:c[i],name:i,type:"regvar"}}))}function c(a,d,e,f){var i,j;a[h]={};for(i in e.vdlists)e.vdlists.hasOwnProperty(i)&&(j=e.terms[e.vdlists[i].term],b(a[h],d.vdlists,j,i,i),a[h][i][g][m]=e.terms[e.vdlists[i].term],f&&f.validation&&
f.validation.init(a[h][i],{source:{name:i,type:"vdlist"}}))}function x(a,b,c,h,i,j,k){var o,m,n,q;for(m=0;m<c.subTables.length;m+=1){o=c.subTables[m];a[e][o]=d.observableArray();a[e][o][g]={};a[e][o][g][f]=w.createAdd(a,o,h,i,j,k);a[e][o][g][l]=w.createRemove(a,o);for(n=0;n<b.subTables[o].length;n+=1)q=u(o,b.subTables[o][n],h,i,j,k),a[e][o].push(q)}}var w={createAdd:function(a,b,d,c,h,f){return function(g){g=g||u(b,void 0,d,c,h,f);a.$__data.subTables[b].push(g.$__data);a[e][b].push(g);return g}},
createRemove:function(a,b){return function(c){c.$__data.id?c.$__data._destroy=!0:d.utils.arrayRemoveItem(a.$__data.subTables[b],c.$__data);a[e][b].remove(c)}}};return function(b,h,f,g,i,j){var k=f[b],l={},p=!1;if(void 0===k)throw Error('Metadata for table "'+b+'" not found');void 0===h&&(h=g(b),p=!0);l.$__data=h;l[e]={};a(l,h,k,j);c(l,h,k,j);x(l,h,k,f,g,i.concat(l),j);p&&j&&j.events&&"rowCreated"in j.events&&d.utils.arrayForEach([].concat(j.events.rowCreated[b]||[]),function(a){a.call(l,l,i.slice(0))});
j&&j.events&&"rowAdded"in j.events&&d.utils.arrayForEach([].concat(j.events.rowAdded[b]||[]),function(a){a.call(l,l,i.slice(0))});return l}}();a.createViewModel=u;a.fillViewModel=function(a,b){var c,e;for(c in b)if(b.hasOwnProperty(c)&&d.isWriteableObservable(a[c]))if(e=a[c],"list"===e[g].term.dataType)a[c](d.utils.arrayFirst(e[g].term.listValues,function(a){return a.id==b[c]})||void 0);else if("decimal"===e[g].term.dataType&&"number"===typeof b[c])a[c](b[c].toFixed(e[g].term.precision).replace(/\./,
","));else a[c](b[c])}})(a)})(window.inca,window.ko,window.RCC,window);(function(f,d){d.Utils={};d.Utils.createDummyForm=function(b){function a(b,c){h[b]={regvars:{},subTables:{},vdlists:{}};d[b]={regvars:{},vdlists:{},subTables:[],terms:{}};f.each(["regvars","vdlists"],function(a,k){f.each(c[k]||{},function(a,c){g++;h[b][k][a]={include:!0,value:null};d[b][k][a]={term:g};d[b].terms[g]=f.isArray(c)?{description:"term for "+a,dataType:"list",listValues:f.map(c,function(a){return"object"==typeof a?a:{id:a,value:a,text:a}})}:c.term?c.term:{description:"term for "+a,dataType:c}})});
f.each(c.subTables||{},function(c,f){d[b].subTables.push(c);h[b].subTables[c]=[];a(c,f)})}var c={env:{_PERSNR:"19111111-1111",_FIRSTNAME:"Test",_SURNAME:"Testsson",_INREPNAME:"Rapport\u00f6r Rapport\u00f6rsson",_INUNITNAME:"OC Demo (0)",_SEX:"M",_ADDRESS:"Testgatan 1",_POSTALCODE:"12345",_CITY:"Teststad",_LKF:"123456",_SECRET:!1,_DATEOFDEATH:"",_REPORTERNAME:"",_LAN:"12",_KOMMUN:"34",_FORSAMLING:"56",_PATIENTID:1}},d={},h={},g=Math.round(1E3*Math.random());a(b.rootTable,{regvars:b.regvars,subTables:b.subTables});
c.metadata=d;c.createDataRow=function(a){return f.extend(!0,{},h[a])};c.data=c.createDataRow(b.rootTable);c.getContainer=function(){return document.getElementsByTagName("body")[0]};return c};d.Utils.parseYMD=function(b){return(b=(b||"").match(/^(\d{4})(-?)(\d\d)\2(\d\d)$/))?new Date(b[1],parseInt(b[3].replace(/^0/,""),10)-1,b[4].replace(/^0/,"")):void 0};d.Utils.ISO8601=function(b){function a(a){return("0"+a).slice(-2)}return[b.getFullYear(),a(b.getMonth()+1),a(b.getDate())].join("-")}})(window.jQuery,
window.RCC,window);/*

 Portions of this software may utilize the following copyrighted material, 
 the use of which is hereby acknowledged.

 Knockout Validate v0.1-pre
 (c) Martin Land?lv - http://mlandalv.github.com/knockout-validate/)
 MIT license
*/
(function(f,d){d.Validation=function(b){b||(b={});this.viewModels=f.observableArray([]);this.errors=f.computed(d.Validation.prototype._getErrors.bind(this));this.requiredDefault="requiredDefault"in b?b.requiredDefault:!0;this.includedDefault="includedDefault"in b?b.includedDefault:!0;"addDefaultValidations"in b||(b.addDefaultValidations=!0);this.addDefaultValidations=b.addDefaultValidations;this._dummyModel={dummy:{}};this._dummyModel.dummy[d.metadataProperty]={term:{dataType:"rcc-dummy"}};this._dummyModel.dummy[d.metadataProperty].validation=
this.init(this._dummyModel.dummy,{source:{type:"unbound-errors"}});this.track(this._dummyModel)};d.Validation.prototype.add=function(){this._dummyModel.dummy[d.metadataProperty].validation.add.apply(this,Array.prototype.slice.call(arguments,0))};d.Validation.prototype.track=function(b){this.viewModels.push(b)};d.Validation.prototype.init=function(b,a){var c=this,e={};e.errors=f.observableArray([]);e.tests=f.observableArray([]);e.info=a;e.add=function(b){b=new d.Validation.Test({errorsTo:e.errors,
test:b,info:a});e.tests.push(b);return b};var h=b[d.metadataProperty];if(h&&(h.validation=e,c.addDefaultValidations)){var g=h.term;"datetime"==g.dataType&&!g.includeTime?e.add(new d.Validation.Tests.Date(b,{ignoreMissingValues:!0,fatal:!0})):"integer"==g.dataType?e.add(new d.Validation.Tests.Integer(b,{min:g.min,max:g.max,ignoreMissingValues:!0,fatal:!0})):"decimal"==g.dataType&&e.add(new d.Validation.Tests.Decimal(b,{decimalPlaces:g.precision,min:g.min,max:g.max,ignoreMissingValues:!0,fatal:!0}));
void 0!==g.maxLength&&null!==g.maxLength&&e.add(new d.Validation.Tests.Length(b,{max:g.maxLength,treatMissingLengthAsZero:!0,ignoreMissingValues:!0,fatal:!0}));void 0!==g.validCharacters&&null!==g.validCharacters&&e.add(new d.Validation.Tests.ValidCharacters(b,{characters:g.validCharacters,ignoreMissingValues:!0,fatal:!0}));f.utils.arrayForEach(["required","included"],function(a){var b=f.observable(void 0),d=f.observable(!1);e[a]=f.computed({read:function(){return d()?b():f.utils.unwrapObservable(c[a+
"Default"])},write:function(a){b(a);d(!0)}})});0>f.utils.arrayIndexOf(["boolean","rcc-dummy"],h.term.dataType)&&e.add(new d.Validation.Tests.NotMissing(b))}return e};d.Validation.prototype._forEachVariable=function(b,a){f.utils.arrayForEach(b,function e(b){for(var d in b)"$"!=d.substring(0,1)&&b.hasOwnProperty(d)&&b[d]&&a(b[d]);if(b.$vd)for(var p in b.$vd)b.$vd.hasOwnProperty(p)&&b.$vd[p]&&a(b.$vd[p]);if(b.$$)for(var l in b.$$)b.$$.hasOwnProperty(l)&&b.$$[l]&&f.utils.arrayForEach(b.$$[l](),e)})};
d.Validation.prototype._getErrors=function(){var b=[];this._forEachVariable(this.viewModels(),function(a){(a=a[d.metadataProperty])&&a.validation&&a.validation.errors&&f.utils.unwrapObservable(a.validation.included)&&b.push(a.validation.errors())});return f.utils.arrayGetDistinctValues(Array.prototype.concat.apply([],b))};d.Validation.prototype.markAllAsAccessed=function(){this._forEachVariable(this.viewModels(),function(b){(b=b[d.metadataProperty])&&b.accessed&&b.accessed(!0)})};d.Validation.prototype.markAllAsAccessedIfDependenciesAreFulfilled=
function(){this._forEachVariable(this.viewModels(),function(b){(b=b[d.metadataProperty])&&b.accessed&&b.dependenciesFulfilled()&&b.accessed(!0)})}})(window.ko,window.RCC,window);/*

 Portions of this software may utilize the following copyrighted material, 
 the use of which is hereby acknowledged.

 Knockout Validate v0.1-pre
 (c) Martin Land?lv - http://mlandalv.github.com/knockout-validate/)
 MIT license
*/
(function(f,d){d.Validation.Test=function(b){var a=this;a._disposed=f.observable(!1);a._error={info:b.info};a._testReferences=[];a._targets=f.utils.arrayMap([].concat(b.errorsTo),function(b){var e=b[d.metadataProperty];return e&&e.validation?(a._testReferences.push(e.validation.tests),e.validation.tests.push(a),e.validation.errors):b});a._test=f.isSubscribable(b.test)?b.test:f.computed({read:b.test,disposeWhen:a._disposed});f.computed({read:a._update.bind(a),disposeWhen:a._disposed})};d.Validation.Test.prototype.dispose=
function(){var b=this;b._disposed(!0);f.utils.arrayForEach(b._testReferences,function(a){a.remove(b)});b._removeErrorFromTargets()};d.Validation.Test.prototype._removeErrorFromTargets=function(){var b=this;f.utils.arrayForEach(b._targets,function(a){a.remove(b._error)});delete b._error.message};d.Validation.Test.prototype._update=function(){var b=this;if(b._disposed())b._removeErrorFromTargets();else{var a=b._test();if("string"==typeof a)a={result:"failed",message:a};else if("undefined"==typeof a)a=
{result:"ok"};else if("object"!=typeof a)throw Error("Validation test returned bad value: "+a);if("failed"==a.result)b._error.message=a.message,b._error.fatal=a.fatal,b._error.data=a.data,f.utils.arrayForEach(b._targets,function(a){a.remove(b._error);a.push(b._error)});else if("ok"==a.result)b._removeErrorFromTargets();else throw Error("Validation test returned bad result: "+a.result);}}})(window.ko,window.RCC,window);/*

 Portions of this software may utilize the following copyrighted material, 
 the use of which is hereby acknowledged.

 Knockout Validate v0.1-pre
 (c) Martin Land?lv - http://mlandalv.github.com/knockout-validate/)
 MIT license
*/
(function(f,d){d.Validation.Tests={};d.Validation.Tests._hasValue=function(b,a){return void 0!==b&&null!==b&&(!a||"string"!=typeof b||0<b.length)};d.Validation.Tests.Date=function(b,a){a||(a={});a.hasOwnProperty("fatal")||(a.fatal=!1);return function(){var c=b();if(a.ignoreMissingValues&&!d.Validation.Tests._hasValue(c,true))return{result:"ok"};var e=d.Utils.parseYMD(c);return!e?{result:"failed",message:"Datumet kunde inte tolkas (\u00e4r inte p\u00e5 formen \u00c5\u00c5\u00c5\u00c5-MM-DD).",fatal:a.fatal}:
d.Utils.ISO8601(e)!=c?{result:"failed",message:"Felaktigt datum (inte p\u00e5 formen \u00c5\u00c5\u00c5\u00c5-MM-DD).",fatal:a.fatal}:{result:"ok"}}};d.Validation.Tests.Integer=function(b,a){a||(a={});a.hasOwnProperty("fatal")||(a.fatal=!1);return function(){var c=b();if(a.ignoreMissingValues&&!d.Validation.Tests._hasValue(c,true))return{result:"ok"};if(!d.Validation.Tests._hasValue(c)||!c.toString().match(/^[-+]?(?:\d|[1-9]\d*)$/))return{result:"failed",message:"Ogiltigt heltal.",fatal:a.fatal};
c=parseInt(c,10);return d.Validation.Tests._hasValue(a.min)&&c<a.min?{result:"failed",message:"Minsta till\u00e5tna v\u00e4rde: "+a.min,fatal:a.fatal}:d.Validation.Tests._hasValue(a.max)&&c>a.max?{result:"failed",message:"St\u00f6rsta till\u00e5tna v\u00e4rde: "+a.max,fatal:a.fatal}:{result:"ok"}}};d.Validation.Tests.Decimal=function(b,a){a.hasOwnProperty("fatal")||(a.fatal=!1);return function(){var c=b();if(a.ignoreMissingValues&&!d.Validation.Tests._hasValue(c,true))return{result:"ok"};var e=RegExp("^[-+]?(?:\\d|[1-9]\\d*),\\d{"+
a.decimalPlaces+"}$");if(!d.Validation.Tests._hasValue(c)||!c.toString().match(e))return{result:"failed",message:"Ej decimaltal med "+a.decimalPlaces+" decimal"+(a.decimalPlaces==1?"":"er")+".",fatal:a.fatal};c=parseFloat(c.toString().replace(/,/,"."));return d.Validation.Tests._hasValue(a.min)&&c<a.min?{result:"failed",message:"Minsta till\u00e5tna v\u00e4rde: "+a.min,fatal:a.fatal}:d.Validation.Tests._hasValue(a.max)&&c>a.max?{result:"failed",message:"St\u00f6rsta till\u00e5tna v\u00e4rde: "+a.max,
fatal:a.fatal}:{result:"ok"}}};d.Validation.Tests.Length=function(b,a){a||(a={});a.hasOwnProperty("fatal")||(a.fatal=!1);return function(){var c=b();if(a.ignoreMissingValues&&!d.Validation.Tests._hasValue(c,true))return{result:"ok"};if(!d.Validation.Tests._hasValue(c)||typeof c.length=="undefined")if(a.treatMissingLengthAsZero)c=0;else return{result:"failed",message:"L\u00e4ngd saknas.",fatal:a.fatal};else c=c.length;return d.Validation.Tests._hasValue(a.min)&&c<a.min?{result:"failed",message:"Minsta till\u00e5tna l\u00e4ngd: "+
a.min,fatal:a.fatal}:d.Validation.Tests._hasValue(a.max)&&c>a.max?{result:"failed",message:"St\u00f6rsta till\u00e5tna l\u00e4ngd: "+a.max,fatal:a.fatal}:{result:"ok"}}};d.Validation.Tests.ValidCharacters=function(b,a){a.hasOwnProperty("fatal")||(a.fatal=!1);return function(){var c=b();if(a.ignoreMissingValues&&!d.Validation.Tests._hasValue(c,true))return{result:"ok"};if(typeof c!="string")return{result:"failed",message:"Inte en textstr\u00e4ng.",fatal:a.fatal};for(var e=0;e<c.length;e++)if(a.characters.indexOf(c[e])<
0)return{result:"failed",message:"Otill\u00e5tet tecken: "+c[e],fatal:a.fatal};return{result:"ok"}}};d.Validation.Tests.NotMissing=function(b){return function(){var a=b[d.metadataProperty].validation.required();if(!a)return{result:"ok"};var c=b();if(!d.Validation.Tests._hasValue(c)||"string"==typeof c&&0==c.length){var e={result:"failed",message:"V\u00e4rde saknas"};"object"==typeof a&&null!==a&&f.utils.arrayForEach(["fatal","data"],function(b){a.hasOwnProperty(b)&&(e[b]=a[b])});return e}return{result:"ok"}}}})(window.ko,
window.RCC,window);

(function ( inca, window )
{
	var Utils = {};

	if ( !window.RCC )
		window.RCC = {};
	if ( !window.RCC.Vast )
		window.RCC.Vast = {};
	if ( !window.RCC.Vast.Utils )
		window.RCC.Vast.Utils = Utils;

	/**
	 * Beräknar antalet "fyllda år" för en person, där någon född på skottdagen
	 * anses fylla år på skottdagen vid skottår och annars den 1 mars.
	 *
	 * @param {Date} dateOfBirth Födelsedatum, i lokala tidszonen.
	 * @param {Date} date Datum vid vilken åldern ska beräknas, i lokala tidszonen.
	 * @returns {Integer} Antal fyllda år.
	 */
	Utils.calculateAge = function ( dateOfBirth, date )
	{
		if ( date.getTime() < dateOfBirth.getTime() )
			return undefined;

		function y ( date ) { return date.getFullYear(); };
		function m ( date ) { return date.getMonth(); };
		function d ( date ) { return date.getDate(); };

		// Beräkna först åldern vid årsslut (31 december).
		var age = y(date) - y(dateOfBirth);

		// Dra bort 1 år om aktuellt års födelsedag inte passerats. (Avgör hur skottårspersoner hanteras.)
		if ( m(date) < m(dateOfBirth) || (m(date) == m(dateOfBirth) && d(date) < d(dateOfBirth)) )
			age -= 1;

		return age;
	};

	/**
	 * Raderar alla rader i en tabell.
	 * @param table Tabell vars rader ska raderas.
	 */
    Utils.removeAllRows = function ( table )
    {
        $.each( table().slice(0), function (i, o)
        {
            table.rcc.remove(o);
        });

    };

	/**
	 * Radera eller lägg till rader i undertabeller baserat på en variabels värde.
	 * Samma undertabell får inte hanteras i flera villkor.
	 *
	 * @param row Raden vars undertabeller ska administreras.
	 * @param currentValue Värdet som villkoren ska jämföras med. En ko.subscribable unwrappas 
	 *				först, och ett objekt reduceras till värdet av egenskapen "value".
	 * @param {Object[]} conditions Villkor av typen { table, value }, där tabellen table töms
	 *				om (i) aktuella värdet är skilt (!==) från value, eller (ii) om value är en array
	 *				skilt (!==) från varje element i arrayn, eller (iii) om value är en funktion och
	 *				denna funktion returnerar sant när den anropas med currentValue som argument.
	 *				I annat fall tillgodoses att tabellen har minst en rad.
	 */
	Utils.subscribeHelper = function ( row, currentValue, conditions )
	{
		// Unwrap observables and objects.
		currentValue = ko.utils.unwrapObservable(currentValue);
		if ( typeof currentValue == 'object' && currentValue !== null )
			currentValue = currentValue.value;

		$.each( conditions,
			function ( unused, condition )
			{
				var table = row.$$[condition.table];
				var shouldDelete;
				if ( $.isFunction(condition.value) )
					shouldDelete = !condition.value(currentValue, row);
				else
					shouldDelete = ($.inArray( currentValue, [].concat(condition.value) ) < 0);

				if ( shouldDelete )
					RCC.Vast.Utils.removeAllRows(table);
				else if ( table().length == 0 )
					table.rcc.add();
			} );
	};
	
	/**
	* Lägg till en rad i given tabell om denna saknar rader.
	*
	* @param table Tabellen vars rader ska administreras (en ko.observableArray med egenskap "rcc").
	* @returns {undefined|Object} Raden som lades till, eller undefined om ingen rad lades till.
	*/
	Utils.addRowIfEmpty = function ( table ) {
		if (table().length == 0) {
			return table.rcc.add();
		}
	};


	/**
	 * Tar fram vilken tabell som är rottabell, utgående från given metadata.
	 *
 	 * @param {Object} [metadata=inca.form.metadata] Metadataobjektet.
	 * @returns {String|undefined} Rottabellens namn, eller undefined om en unik rottabell 
	 * 			inte kan bestämmas.
	 */
	Utils.rootTable = function ( metadata )
	{
		if ( !metadata )
			metadata = inca.form.metadata;

		var tables = [], isSubTable = {};
		for ( var table in metadata )
		{
			if ( metadata.hasOwnProperty(table) )
			{
				tables.push( table );

				var subTables = metadata[table].subTables;
				for ( var i = 0; i < subTables.length; i++ )
					isSubTable[ subTables[i] ] = true;
			}
		}

		var rootTables = ko.utils.arrayFilter( tables, function ( table ) { return !isSubTable[table]; } );

		return rootTables.length == 1 ? rootTables[0] : undefined;
	};

	Utils.SubTableConditionHandler = function(rowAddedEventHandler, conditions) {

		// Sortera in villkor i en tabellstruktur och få ett objekt som visar vilka tabeller som ska få ett nytt rowAdded-event.
		var n_cond = {}
		$.each(inca.form.metadata, function (tableName, tableObj) {
			$.each(tableObj.regvars, function (regvarName) {
				if (conditions[regvarName]) {
					if (!n_cond[tableName]) { n_cond[tableName] = []; }
					n_cond[tableName].push({ name: regvarName, conditions: $.isFunction(conditions[regvarName]) ? conditions[regvarName]() : conditions[regvarName] });
				}
			});
		});

		// Skapa rowAdded-event.
		$.each(n_cond, function (tableName, tableConditions) {
			function subTableConditionHandlerEvent(row) {
				$.each(tableConditions, function (i, condition) {
					row[condition.name].subscribe(function () {
						Utils.subscribeHelper(row, row[condition.name], condition.conditions);
					});
				});
			}

			if (rowAddedEventHandler[tableName]) {
				rowAddedEventHandler[tableName] = [].concat(rowAddedEventHandler[tableName], subTableConditionHandlerEvent);
			} else {
				rowAddedEventHandler[tableName] = subTableConditionHandlerEvent;
			}
		});

		/**
		* Hämtar värdet på ett villkor.
		*
		* @param {String} regvarName Namn på egenskap.
		* @param {String} tableName Namn på tabellen där raderna hanteras av subscribeHelper.
		* @returns {String|String[]} Värdet av villkoret.
		*/

		this.getConditionValue = function (regvarName, tableName) {
			var rval;
			var regvarConditions = $.isFunction(conditions[regvarName]) ? conditions[regvarName]() : conditions[regvarName];
			$.each(regvarConditions, function (i, o) {
				if (o.table == tableName) {
					rval = o.value;
					return false;
				}
			});
			return rval;
		};

	};

	/**
	* Returnerar en array med samtliga value-egenskaper i angiven listvariabel.
	* @param {String} varName Namn på listvariabel.
	* @param {String|String[]} excludeValue Värde/värden som ska exkluderas.
	*/

	Utils.getListValuesArray = function(varName, excludeValue)
	{
		var listValues = (function() {
			var rv = null;
			$.each(inca.form.metadata, function(n, o) {
				if (rv) return false;
				$.each(o.regvars, function(nn, oo) {
					if (varName == nn)
					{
						rv = o.terms[oo.term].listValues;
						return false;
					}
				});
			});
			return rv;
		})();

		if (!listValues) throw new Error('List values for ' + varName + ' not found.');
		
		var rv = [];
		excludeValue = (excludeValue ? [].concat(excludeValue) : []);

		$.each(listValues, function(i, o) {
			if ($.inArray(o.value, excludeValue) == -1)
			{
				rv.push(o.value)
			}
		});

		return rv;
	};

	/**
	 * Markera given observable som "required" och "fatal". Felets dataobjekt innehåller
	 * objekt med egenskap "canBeSaved" satt till true.
	 *
	 * @param {ko.observable} Observable från en RCC.ViewModel, med .rcc.validation-egenskap.
	 */

	Utils.markAsFatal = function(observable) {
		observable.rcc.validation.required({ fatal: true, data: { canBeSaved: true } });
	};

	/**
	 * Lägg till validering som kontrollerar att ett numeriskt värde ligger inom
	 * givna gränser, inklusivt. Valideringen görs endast om det verkligen är ett
	 * giltigt numeriskt värde.
	 * 
	 * @param {ko.observable} observable Observable från en RCC.ViewModel, med .rcc.validation-egenskap.
	 * @param {Number} min Minsta tillåtna värde.
	 * @param {Number} max Största tillåtna värde.
	 * @param {Boolean} [isFatal=false] Om sant markeras felet som "fatal".
	 */

	Utils.addNumMinMaxValidation = function(observable, min, max, isFatal) {
		observable.rcc.validation.add(function () {
			var v = observable();
			if (typeof v == 'string')
				v = parseFloat(v.replace(/,/g, '.'));
			isFatal = !!isFatal;

			// Annan validering fångar felaktiga numeriska värden.
			if (v === undefined || v === null || isNaN(v) || !isFinite(v))
				return;
			else if (v > max)
				return { result: 'failed', message: 'Största tillåtna värde: ' + max.toString().replace('.', ','), fatal: isFatal };
			else if (v < min)
				return { result: 'failed', message: 'Minsta tillåtna värde: ' + min.toString().replace('.', ','), fatal: isFatal };
			else
				return;
		});
	};


	/**
	 * Lägg till validering, markerad som "fatal", som kontrollerar att en 
	 * observable innehåller ett värde som "rimligen" representerar ett årtal.
	 * @param {ko.observable} Observable från en RCC.ViewModel,  med .rcc.validation-egenskap.
	 */

	Utils.addYearValidation = function(observable) {
		observable.rcc.validation.add(function () {
			var v = observable();
			if (v !== null && v !== undefined && v !== '') {
				if (!(v + '').match(/^\d{4}$/)) {
					return { result: 'failed', message: 'Felaktigt årtal', fatal: true };
				}
			}
		});

	};

	/**
	 * Returnera en funktion som kan användas som inca.on('validation')-callback.
	 *
	 * @param {RCC.Validation} validation Valideringsobjektet.
	 * @param {Object} [actions={}] Åtgärder.
	 * @param {String[]} [actions.abort=[]] Åtgärder som inte kräver någon validering.
	 * @param {String[]} [actions.pause=[]] Åtgärder som inte kräver strikt validering.
	 * @returns {Function} Funktion som kan användas som inca.on('validation')-callback.
	 */
	Utils.getFormValidator = function ( validation, actions ) {
		if ( !actions ) actions = {};
		if ( !actions.pause ) actions.pause = [];
		if ( !actions.abort ) actions.abort = [];

		return function ( ) {
			var selectedAction = (function() {
				if (inca.errand && inca.errand.action && inca.errand.action.find)
					return inca.errand.action.find(':selected').text();
			})();

			if ($.inArray(selectedAction, actions.abort) >= 0)
				return true;

			var errors = validation.errors();

			if (errors.length > 0) {
				validation.markAllAsAccessed();

				var isPause = ($.inArray(selectedAction, actions.pause) >= 0);
				var fatalErrors = $.grep(errors,
					function (error) {
						if (isPause && error.data && error.data.canBeSaved )
							return false;
						else
							return error.fatal;
					});

				if (fatalErrors.length > 0) {
					alert("Formuläret innehåller " + fatalErrors.length + " fel.");
					return false;
				} else if (isPause && fatalErrors.length == 0) return true;

			   return confirm('Valideringen av frågosvaren gav ' + errors.length + ' varning' + (errors.length == 1 ? '' : 'ar') + '.\r\nVill du gå vidare ändå?');
			}

			return true;
		};
	};

	/**
	 * Extraherar innehållet av den första kommentaren i funktionens kod.
	 *
	 * @param {Function} func Funktion som innehåller utkommenterad kod.
	 * @returns {String}
	 */
	Utils.extractSource = function(func) {
		var m = func.toString().match(/\/\*(?:\*\s*@(?:preserve|license))?([\s\S]*?)\*\//);
		return m && m[1];
	};
	
	/**
	 * Knockouts metod för att detektera version av Internet Explorer. Från knockout-3.2.0.debug.js
	 *
	 * @returns {Number|undefined}
	 */
    Utils.ieVersion = document && (function() {
        var version = 3, div = document.createElement('div'), iElems = div.getElementsByTagName('i');

        // Keep constructing conditional HTML blocks until we hit one that resolves to an empty fragment
        while (
            div.innerHTML = '<!--[if gt IE ' + (++version) + ']><i></i><![endif]-->',
            iElems[0]
        ) {}
        return version > 4 ? version : undefined;
    }());

})( window['inca'], window );

(function (RCC, ko, _) {
    ko.bindingHandlers[RCC.bindingsPrefix + 'decimal'] = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var val = valueAccessor(),
                opt = _.defaults((ko.isObservable(val) ? { valueObservable: val } : val), { allowPointAsDecimalSeparator: false, padWithZeros: true }),
                metadata = opt.valueObservable[RCC.metadataProperty];

            if (!metadata || !metadata.term || metadata.term.dataType != "decimal")
                throw new Error('The binding "' + RCC.bindingsPrefix + 'decimal" requires a decimal observable from RCC.ViewModel as argument.');

            var precision = opt.valueObservable[RCC.metadataProperty].term.precision;
            var bindings = {
                event: {
                    change: function () {
                        var value = opt.valueObservable();
                        if (value) {
                            if (opt.allowPointAsDecimalSeparator) value = value.replace(/\./g, ',');
                            if (opt.padWithZeros && new RegExp("^-?(?:\\d+|\\d*,\\d{1," + precision + "})$").exec(value))
                                value = parseFloat(value.replace(/,/g, '.')).toFixed(precision).toString().replace(/\./g, ',');
                            opt.valueObservable(value);
                        }
                        return true;
                    }
                }
            };
            var valueBinding = {};
            valueBinding[RCC.bindingsPrefix + 'value'] = opt.valueObservable;
            ko.applyBindingsToNode(element, valueBinding, bindingContext);
            ko.applyBindingsToNode(element, bindings, bindingContext);
        }
    };
})(window['RCC'], window['ko'], window['_']);
(function ($, _, RCC, ko, moment) {
    ko.components.register('date-picker', {
        synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
        viewModel: function(params) {
            if (!params.value) throw new Error('Parameter "value" is required.');
            _.defaults(params, { autoOpen: false, closable: true, readOnly: false });
            
            var self = this, today = (params.dateToday ? Moment(params.dateToday) : Moment());
            self.visible = ko.observable(params.autoOpen);
            self.pickerMonth = ko.observable();

            if (params.closable) {
                self.close = function() {
                    self.visible(false);
                };
                self.toggle = function() {
                    self.visible(!self.visible());
                };
            } else {
                self.toggle = function() {
                    self.visible(true);
                };
            }

            function Moment(args) {
                return moment(args).lang("sv");
            }

            function PickerMonth(date) {
                var observableDate = (function () {
                    var v = params.value(), m;
                    if (v) m = Moment(v);
                    if (m && m.isValid()) return m;
                })();

                var manualDate = (date ? Moment(date) : undefined);

                var baseDate = (function() {
                    if (manualDate) return manualDate;
                    else if (observableDate) return observableDate;
                    else return today;
                })();

                var pm = this;         
                
                pm.title = (function(monthName, year) {
                    return monthName.substr(0, 1).toUpperCase() + monthName.substr(1) + ' ' + year;
                })(baseDate.format('MMMM'), baseDate.year());

                pm.prevMonthClick = function () {
                    self.pickerMonth(new PickerMonth(moment(baseDate).subtract('months', 1)));
                };

                pm.nextMonthClick = function () {
                    self.pickerMonth(new PickerMonth(moment(baseDate).add('months', 1)));
                };

                pm.weeks = (function (baseDateMonth, startAt) {
                    function PickerWeek(days) {
                        this.days = days;
                    }

                    function PickerDay(moment) {
                        this.date = moment.date();
                        this.isCurrentMonth = (baseDateMonth == moment.month());
                        this.isToday = today.isSame(moment, 'day');
                        this.isSelected = observableDate && observableDate.isSame(moment, 'day');
                        this.click = function() {
                            if (!params.readOnly) params.value(moment.format('YYYY-MM-DD'));
                            if (params.closable) self.visible(false);
                        };
                    }

                    startAt.subtract('days', startAt.isoWeekday() - 1);
                    return _.map(_.range(6), function(week) {
                        return new PickerWeek(_.map(_.range(7), function(day) {
                            return new PickerDay(moment(startAt).add('days', (week * 7) + day));
                        }));
                    });

                })(baseDate.month(), moment(baseDate).set('date', 1)); 
            }

            self.pickerMonth(new PickerMonth());

            params.value.subscribe(function () {
                self.pickerMonth(new PickerMonth());
            });
        },
        template: RCC.Vast.Utils.extractSource(function() {/**@preserve
            <div class="rcc-datepicker-wrapper">
                <button class="btn btn-xs btn-default rcc-datepicker-toggle" data-bind="click: toggle, css: { 'active': visible }">
                    <span class="glyphicon glyphicon-calendar"></span>
                </button>
                <div class="rcc-datepicker" data-bind="with: pickerMonth, visible: visible">
                        <!-- ko if: $parent.close -->
                            <div class="div-close">
                                <button data-bind="click: $parent.close" class="close" aria-hidden="true">&times;</button>
                            </div>
                        <!-- /ko -->
                    <table class="table-condensed">
                        <thead>
                            <tr>
                                <th class="prev" data-bind="click: prevMonthClick">«</th>
                                <th class="month" colspan="5" data-bind="text: title"></th>
                                <th class="next" data-bind="click: nextMonthClick">»</th>
                            </tr>
                            <tr data-bind="foreach: ['Må', 'Ti', 'On', 'To', 'Fr', 'Lö', 'Sö']">
                                <th data-bind="text: $data"></th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: weeks">
                            <tr data-bind="foreach: days">
                                <td data-bind="text: date, click: click, css: { 'diff-month': !isCurrentMonth, 'today': isToday, 'selected': isSelected }"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        */return undefined;})
    });

    ko.bindingHandlers[RCC.bindingsPrefix + 'datepicker'] = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            ko.applyBindingsToNode($('<date-picker></date-picker>').insertAfter(element)[0], { component: { name: 'date-picker', params: valueAccessor() } }, bindingContext);            
            ko.applyBindingsToNode(element, { 'rcc-value': valueAccessor().value }, bindingContext);
        }
    };

})(jQuery, _, RCC, ko, moment);
var setPopover = function (element, options) {
    if (options) {
        var o = _.defaults(options, {
            trigger: 'focus'
        });
        $(element).popover(o);
    }
};

window.ko.bindingHandlers.popover = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        if (valueAccessor && valueAccessor()) {
            var val = valueAccessor();
            if (ko.isObservable(val)) {
                val.subscribe(function () {
                    setPopover(element, val());
                });
                setPopover(element, val());
            } else {
                setPopover(element, val);
            }
        }
    }
};

(function ($, _, RCC, ko) {
	function AutoCompleteViewModel(valueAccessor, element) {
		var va = valueAccessor(),
			self = this,
			_options = _.defaults(_.clone(va), {
				format: function(o) { return o; },
				hideOnSelectRow: true,
				hideOnBlur: true,
				hideOnEsc: true,
				showOnFocus: true,
				visibleRowCount: { initial: 10, delta: 10 },
				autoMarkFirstRow: false,
				selectOnTab: false
			});

		if (!isNaN(_options.visibleRowCount))
			_options.visibleRowCount = { initial: _options.visibleRowCount, delta: _options.visibleRowCount };

		function AutoCompleteRow(data, formatted) {
			var me = this;
			me.data = data;
			me.formatted = formatted;
			me.active = ko.observable(false);
			me.select = function() {
				_options.selectRow(me);
				if (_options.hideOnSelectRow) self.visible(false);
			};
		}

		self.visibleRowCount = ko.observable(_options.visibleRowCount.initial);
		self.visible = ko.observable(false);
		self.showMoreVisible = ko.observable(false);
		self.showMoreEvent = {
			mouseover: function() { self.visibleRowCount(self.visibleRowCount() + _options.visibleRowCount.delta); }
		};

		self.filteredData = ko.computed(function() {
			var r = _.map(_.filter(ko.unwrap(_options.data), function(x) { return _options.filter(x) === true; }),
				function(x, index) { return new AutoCompleteRow(x, _options.format(x)); });

			self.visible(r.length > 0);
			if (_options.autoMarkFirstRow && r.length > 0) r[0].active(true);
			return r;
		});

		self.filteredData.subscribe(
			function ( ) {
				self.visibleRowCount(_options.visibleRowCount.initial);
			} );
		
		var keyCodes = {
			ArrowUp: 38,
			ArrowDown: 40,
			Enter: 13,
			Escape: 27,
			Tab: 9
		};

		var elementEvent = {};

		elementEvent.keydown = function(a, b) {

			if (!_.contains(keyCodes, b.keyCode) || b.metaKey || b.shiftKey || b.altKey || b.ctrlKey) {
				return true; 
			}

			var col = self.filteredData();
			if (col.length == 0) return true;

			var useDefaultAction = false;

			function selectActiveRow() {
				if (self.visible()) {
					var active = _.find(col, function(x) { return x.active(); });
					if (active) active.select();
				}
			}

			switch(b.keyCode) {
				case keyCodes.ArrowDown:
				case keyCodes.ArrowUp:
					var activeIndex = _.indexOf(_.invoke(col, 'active'), true),
						newIndex = 0,
						l = Math.min(self.visibleRowCount(), col.length);
				
					if (activeIndex == -1) newIndex = (b.keyCode == keyCodes.ArrowDown ? 0 : l - 1);
					else {
						col[activeIndex].active(false);
						newIndex = ((activeIndex + l) + (b.keyCode == keyCodes.ArrowDown ? 1 : -1)) % l;
					}
					col[newIndex].active(true);
					break;
				case keyCodes.Enter:
					selectActiveRow();
					break;
				case keyCodes.Tab:
					if (_options.selectOnTab) selectActiveRow();
					useDefaultAction = true;
					break;
				case keyCodes.Escape:
					if (_options.hideOnEsc) self.visible(false);
					break;
			}

			return useDefaultAction;
		};

		if (_options.hideOnBlur) elementEvent.blur = function() { self.visible(false); }
		if (_options.showOnFocus) elementEvent.focus = function() { if (self.filteredData().length > 0) self.visible(true); }
		
		ko.applyBindingsToNode(element, { 'event': elementEvent });
	}

	if (!RCC.Vast.Components) RCC.Vast.Components = {};
	RCC.Vast.Components.AutoComplete = {
		Filters: {
			containsEachWord: function(searchFor, searchIn) {
				if (searchFor && searchIn) {
					searchIn = searchIn.toLowerCase();
					return _.every(searchFor.toLowerCase().replace(/^\s+|\s+$/g, '').split(/\s+/),
						function(s) {
							var containsWord = (searchIn.indexOf(s.replace(/^-/,'')) != -1);
							return s.match(/^-/) ? !containsWord || s.length == 1 : containsWord;
						});
				}
				return false;
			}
		}
	};
	RCC.AutoCompleteFilters = RCC.Vast.Components.AutoComplete.Filters;

	$(function () {
		$('head').append(RCC.Vast.Utils.extractSource(function() {/**@preserve
			<script type="text/html" id="rcc-autocomplete">
		
				<div class="rcc-autocomplete-inner" data-bind="visible: visible">
					<div class="items" data-bind="foreach: filteredData().slice(0, visibleRowCount())">
						<div data-bind="html: formatted, css: { 'active': active }, event: { mousedown: select } "></div>
					</div>
					<div class="show-more" data-bind="visible: filteredData().length > visibleRowCount(), event: showMoreEvent">Visa fler...</div>
				
				</div>

			</script>
		*/}));
	});

	ko.bindingHandlers[RCC.bindingsPrefix + 'autocomplete'] = {
		init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
			ko.applyBindingsToNode($('<div></div>').addClass('rcc-autocomplete').insertAfter(element)[0], { 'template': { name: 'rcc-autocomplete' } }, new AutoCompleteViewModel(valueAccessor, element));
		}
	};

})(jQuery, _, RCC, ko);

(function ( inca, ko, window )
{
/**
 * @namespace
 */
var ICD10 = {};

/**
 * Name of the value domains.
 * @private
 */
ICD10.valueDomainNames =
{
	code: 'VD_ICD10_Kod',
	chapters: 'VD_ICD10_Kapitel',
	chapterCodes: 'VD_ICD10_Kapitelkoder'
};

/**
 * Cached codes; id is key and value is the object that
 * ICD10.informationForId returns.
 * @private
 */
ICD10._cache = {};

/**
 * Return an object containing observables describing the code with the given id.
 * @param id {Number} The number.
 * @returns {Object} An object with properties (ko.observable:s)
 *		- text: human readable representation
 *		- error: true if an error occurred and false if not, undefined if pending.
 *		- pending: true if the request is pending, false otherwise.
 *		- [obj]: raw data object from value domain
 */
ICD10.informationForId =
function ( id )
{
	if ( !ICD10._cache.hasOwnProperty(id) )
	{
		var info =
		{
			text: ko.observable('Laddar...'),
			error: ko.observable(undefined),
			pending: ko.observable(true),
			obj: ko.observable(undefined)
		};

		ICD10._cache[id] = info;

		inca.form.getValueDomainValues(
			{
				vdlist: ICD10.valueDomainNames.code,
				parameters: { kod: id },
				success:
					function ( list )
					{
						info.pending(false);

						if ( list.length != 1 )
						{
							info.error(true);
							info.obj(undefined);
							info.text( 'Oväntat radantal (id ' + id + ', n = ' + list.length + ')' );
						}
						else
						{
							info.error(false);
							info.obj( list[0] );
							info.text( ICD10._formatCode(list[0]) );
						}
					},
				error:
					function ( err )
					{
						info.pending(false);
						info.error(true),
						info.obj(undefined);
						info.text( 'Oväntat fel (id ' + id + '): ' + err );
					}
			} );
	}
	
	return ICD10._cache[id];
};

/**
 * Return a human readable representation of a code object.
 *
 * @param row A single data row describing a code.
 * @returns {String} A human readable representation of the code.
 * @private
 */
ICD10._formatCode =
function ( row )
{
	return row.data.beskrivning + ' (' + row.data.kod + ')'
};

/**
 * Creates a view model suitable for rendering in template 'icd10-picker'.
 * @param opt {Object} Options.
 * @param opt.addCode Callback when a new code is added.
 * @param {String} [opt.defaultChapter] Default chapter to select.
 * @param {Function} [opt.manualFilter] Filter function that takes an array of codes and returns the filtered array.
 * @constructor
 */
ICD10.Picker =
function ( opt )
{
	var self = this;

	self.errors = ko.observableArray([]);
	self.addCode = opt.addCode;

	self.query = ko.observable('').extend( { throttle: 300 } );
	self.maxCodes = ko.observable(10);

	// [ { id: chapter-id, codes: observableArray-of-codes, data: column-data }, ... ]
	self.chapters = ko.observableArray();
	self.chapters(undefined);
	self._loadChapters( opt.defaultChapter );

	self.chapter = ko.observable(undefined);
	self.chapter.subscribe(
		function ( chapter )
		{
			if ( chapter && !chapter.codes() )
				self._loadCodesForSelectedChapter();

			self.query('');
			self.maxCodes(10);
		} );

	self.manualFilter = opt.manualFilter;

	self.filteredCodes = ko.computed( self._filteredCodes, self );
};

/**
 * Return the codes containing the search query. The results are
 * sorted ascending by the code.
 * @private
 */ 
ICD10.Picker.prototype._filteredCodes =
function ( )
{
	var self = this;

	if ( !self.chapter() )
		return undefined;

	var codes = self.chapter().codes();
	if ( !codes )
		return undefined;

	var query = self.query().toLowerCase();

	var filter = ko.utils.arrayFilter( codes,
		function ( code )
		{
			return (code.data.kod + code.data.beskrivning).toLowerCase().indexOf(query) >= 0;
		} ).sort(
			function ( a, b )
			{
				var aa = a.data.kod;
				var bb = b.data.kod;

				return aa > bb ? +1 : (aa < bb ? -1 : 0);
			} );

	return self.manualFilter ? self.manualFilter(filter) : filter;
};

/**
 * Attempts to load a value domain containing the available chapters.
 *
 * @param {String} [defaultChapter] Default chapter to select after loading list of chapters.
 * @private
 */
ICD10.Picker.prototype._loadChapters =
function ( defaultChapter )
{
	var self = this;

	inca.form.getValueDomainValues(
		{
			vdlist: ICD10.valueDomainNames.chapters,
			success:
				function ( list )
				{
					var chapters = 
						ko.utils.arrayMap( list,
							function ( chapter )
							{
								chapter.codes = ko.observableArray();
								chapter.codes(undefined);
								return chapter;
							} );
					chapters.sort(
							function ( a, b )
							{
								var aa = a.data.forstakod;
								var bb = b.data.forstakod;
		
								return aa > bb ? +1 : (aa < bb ? -1 : 0);
							} );
					self.chapters( chapters );
					if ( defaultChapter )
						self.chapter( ko.utils.arrayFirst( chapters, function ( chapter ) { return chapter.data.kapitel == defaultChapter; } ) );
				},
			error: function ( err ) { self.errors.push( 'Lyckades inte ladda kapitlen: ' + JSON.stringify(err) ); }
		} );
};

/**
 * Attempts to load a value domain containing the codes for the currently selected chapter.
 * Retrieved codes are saved to the cache.
 *
 * @private
 */
ICD10.Picker.prototype._loadCodesForSelectedChapter =
function ( )
{
	var self = this;
	var chapterId = self.chapter().id;
	inca.form.getValueDomainValues(
		{
			vdlist: ICD10.valueDomainNames.chapterCodes,
			parameters: { kapitel: chapterId },
			success:
				function ( list )
				{
					self.chapter().codes( list );
					// Populate the cache.
					ko.utils.arrayForEach( list,
						function ( row )
						{
							if ( ICD10._cache.hasOwnProperty(row.id) )
								return;

							ICD10._cache[row.id] =
							{
								text: ko.observable( ICD10._formatCode(row) ),
								error: ko.observable(false),
								pending: ko.observable(false),
								obj: ko.observable(row)
							};
						} );
				},
			error: function ( err ) { self.errors.push( 'Lyckades inte ladda koder för kapitel med id ' + chapterId + ': ' + JSON.stringify(err) ); }
		} );
};

window.ICD10 = ICD10;

})( inca, ko, window );

(function(inca, RCC, ko, _) {
	ko.components.register('icd10-picker', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
            var self = this;
            self.icd10Visible = ko.observable(false);
            self.table = params.table;
            self.icd10Typ = params.icd10Typ;
            self.showValidation = params.showValidation || null;

            self.icd10filter = function(o) {
                return o.data.kod.match(/^F1/) || o.data.kod == "F630";
            };

            self.icd10Picker = new ICD10.Picker({
                defaultChapter: self.icd10Typ == '3' ? null : 'Psykiska sjukdomar och syndrom samt beteendestörningar',
                addCode: function(code) {
                    var row = self.table.rcc.add();
                    row.diagnosVD(code.id);
                    row.diagnostyp(ko.utils.arrayFirst(row.diagnostyp.rcc.term.listValues, function (list) {
                        return list.value === self.icd10Typ;
                    }));
                },
               manualFilter: function(objs) {
                    var res = null;
                    if(self.icd10Typ == '1')
                        res = _.filter(objs, self.icd10filter);
                    else if(self.icd10Typ == '2')
                        res = _.reject(objs, self.icd10filter);
                    else 
                        res = objs;
                    return res;
                }
            });
       },
           template: RCC.Vast.Utils.extractSource(function() {/**@preserve
          <div data-bind="visible: icd10Visible()" class="rcc-modal" tabindex="-1">
            <div class="rcc-modal-overlay"></div>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button data-bind="click: function() { icd10Visible(false); }" class="close">&times;</button>
                        <h4 class="modal-title">Välj diagnos</h4>
                    </div>
                    <div class="modal-body">
                    <!-- ko if: icd10Picker.errors() && icd10Picker.errors().length > 0 -->
                        <h1>Tekniskt fel</h1>
                        <ul class="icd10-errors" data-bind="foreach: icd10Picker.errors">
                            <li data-bind="text: $data"></li>
                        </ul>
                    <!-- /ko -->
                    <!-- ko ifnot: icd10Picker.chapters -->
                        <em>Laddar kapitel...</em>
                    <!-- /ko -->
                    <!-- ko if: icd10Picker.chapters -->
                        <select class="form-control" data-bind="
                            value: icd10Picker.chapter,
                            options: icd10Picker.chapters,
                            optionsText: function ( chapter ) { return chapter.data.kapitel + ' (' + chapter.data.forstakod + '–' + chapter.data.sistakod + ')'; },
                            optionsCaption: '– Välj kapitel –'">
                        </select>
                        <form>
                            <input type="search" style="margin: 10px 0;" class="form-control" placeholder="Sök" data-bind="value: icd10Picker.query, valueUpdate: 'afterkeydown', visible:icd10Picker.chapter" />
                        </form>
                            <!-- ko if: icd10Picker.chapter -->
                                <!-- ko if: icd10Picker.filteredCodes -->
                                    <table class="table table-hover table-condensed">
                                        <thead>
                                            <tr>
                                                <th>Kod</th>
                                                <th>Beskrivning</th>
                                                <th>Åtgärd</th>
                                            </tr>
                                        </thead>
                                        <tbody data-bind="foreach: icd10Picker.filteredCodes().slice( 0, icd10Picker.maxCodes() )">
                                            <tr>
                                                <td data-bind="text: data.kod"></td>
                                                <td data-bind="text: data.beskrivning"></td>
                                                <td><button class="btn btn-primary btn-xs" style="background-color: #330855" data-bind="click: $parent.icd10Picker.addCode">Välj</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <button class="btn btn-default" data-bind="enable: icd10Picker.filteredCodes().length > icd10Picker.maxCodes(), click: function ( ) { icd10Picker.maxCodes( icd10Picker.maxCodes() + 30 ); }">Visa fler</button> 
                                <!-- /ko -->
                                <!-- ko ifnot: icd10Picker.filteredCodes -->
                                    <em>Laddar koder...</em>
                                <!-- /ko -->
                            <!-- /ko -->
                    <!-- /ko -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-bind="click: function() { icd10Visible(false); }">Stäng</button>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-hover table-condensed" style="margin-bottom: 0px">
            <thead>
                <tr>
                    <th>Vald diagnos</th>
                    <th>Åtgärd</th>
                </tr>
            </thead>
            <tbody data-bind="foreach: table">
                <tr>
                    <!-- ko if: diagnostyp() && diagnostyp().value == $parent.icd10Typ -->
                        <td data-bind="text: ICD10.informationForId(diagnosVD()).text"></td>
                        <td><button class="btn btn-primary btn-xs btn-danger" data-bind="enable: !$root.$form.isReadOnly, click: $parent.table.rcc.remove">Ta bort</button></td>
                    <!--/ko-->
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td>
                        <em data-bind="visible: table().length == 0">Inga valda diagnoser.</em>
                    </td>
                    <td>
                        <button data-bind="enable:!$root.$form.isReadOnly, click: function() { icd10Visible(true); }" class="btn btn-primary btn-xs" style="background-color: #330855">
                            <span>Lägg till</span>
                        </button>
                    </td>
                </tr>
            </tfoot>
        </table>
        <!-- ko if: showValidation && showValidation() -->
            <span style="display: inline-block; margin-bottom: 10px">
                <span class="fa fa-exclamation-circle" style="color: red; font-size: 16px; margin-left: 400px"></span>
                <span class="help" data-bind="text: 'Fyll i beroendediagnos'"></span>
            </span>
        <!-- /ko-->
       */return undefined;})
	});
})(window['inca'], window['RCC'], window['ko'], window['_']);

(function(inca, RCC, ko, _) {
	ko.components.register('s-a', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
			if (!params.value && params.controlType != 'staticText') throw new Error('Parameter "value" is required.');
			var self = this;
			_.extend(self, _.omit(_.defaults(params, {
				question: params.value.rcc.regvar.label || params.question,
				info: '',
				controlType: { datetime: 'datepicker', list: 'select', boolean: 'checkbox' }[params.value.rcc && params.value.rcc.term.dataType] || 'text',
				fullWidthText: false,
				placeholder: '',
				showReviewerInclude: true,
				tooltip: params.value.rcc.regvar.description,
				optionsText: 'text',
                optionsCaption: '– Välj –',
                updateOnKeyPress: false
			}), 'controlType', 'controlChange'));

			self.ma = params.ma;

            self.helpPopover = ko.pureComputed(function() {
            	var regvar = params.value && params.value.rcc && params.value.rcc.regvar && params.value.rcc.regvar;
                var helpText = regvar && regvar.helpText ?
					regvar.helpText : params.help ? params.help : self.tooltip;
                return helpText ? {
                        title: undefined,
                        content:  helpText,
                        trigger: 'hover',
                        html: true
                    } : null;
            });

			
		 	self.control = new function() {
				var m = this;
				m.type = (typeof params.controlType == 'string' ? params.controlType : params.controlType.name);
				m.options = _.defaults(params.controlType.options || {}, (function() {
					switch (m.type) {
						case 'datepicker': return { value: self.value, dateToday: inca.serverDate, readOnly: inca.form.isReadonly };
						case 'decimal': return { valueObservable: self.value };
						case 'select': return { value: self.value, caption: self.optionsCaption, text: self.optionsText };
						case 'select-filter': return { value: self.value, caption: self.optionsCaption, text: self.optionsText };
						case 'staticText':
							if( _.isFunction(self.value) && _.isObject(self.value()) && self.value().text ){
								return { value: self.value().text };
							}
							return { value: self.value };
						default: return { value: self.value };
					}
				})());
                m.toggleInclude = function () {
                    self.value.rcc.include(!self.value.rcc.include());
                };
		 	};

			self.createControlEvent = function($parent) {
				return new function() {
					if (params.controlChange) this.change = function() { return params.controlChange($parent); };
				};
			};

			self.hasErrors = ko.computed(function () {
				var errors = params.value && params.value.rcc && params.value.rcc.validation && params.value.rcc.validation.errors();
				return errors && errors.length;
			});

			self.hasFatalErrors = ko.computed(function () {
				var errors = params.value && params.value.rcc && params.value.rcc.validation && params.value.rcc.validation.errors();
				return !!(errors && errors.filter(function (e) { return e.fatal; }).length);
			});
		},
		template: RCC.Vast.Utils.extractSource(function() {/**@preserve
			<!-- ko ifnot: ma -->
			<div class="single-answer-template r" data-bind="attr: { title: tooltip }">
				<label data-bind="css: { 'accessed': value.rcc && value.rcc.accessed(), 'errors': hasErrors, 'fatal': hasFatalErrors }">
					<span class="question" data-bind="html: question +  (info ? '<br /><span class=\'help\'>' + info + '</span>' : ''), css: { 'full-width-text': fullWidthText }"></span>
					<!-- ko if: ko.utils.unwrapObservable($root.showRegvarNames) && value.rcc -->
						<span class="regvar-name" data-bind="text: value.rcc.regvarName"></span>
					<!-- /ko -->
					<!-- ko with: _.extend(control, { event: createControlEvent($parent) }) -->
						<!-- ko if: type == 'select' -->
							<select class="ctrl" data-bind="rcc-list: options.value, optionsCaption: options.caption, event: event, optionsText: options.text"></select>
						<!--/ko-->
						<!-- ko if: type == 'text' -->
							<input class="ctrl" type="text" data-bind="rcc-value: options.value, event: event" />
						<!--/ko-->
						<!-- ko if: type == 'datepicker' -->
							<input class="ctrl" type="text" data-bind="rcc-datepicker: options, event: event" />
						<!--/ko-->
						<!-- ko if: type == 'staticText' -->
							<span data-bind="text: options.value, rcc-var: options.value"></span>
						<!--/ko-->
						<!-- ko if: type == 'decimal' -->
							<input class="ctrl" type="text" data-bind="rcc-decimal: options, event: event" />
						<!--/ko-->
						<!-- ko if: type == 'textarea' -->
							<textarea cols="76" class="ctrl" type="text" data-bind="rcc-value: options.value, event: event" rows="4"></textarea>
						<!--/ko-->
						<!-- ko if: type == 'checkbox' -->
							<input type="checkbox" data-bind="rcc-checked: options.value, event: event">
						<!--/ko-->
					<!--/ko-->
					<!-- ko if: placeholder -->
						<!-- ko text: placeholder --><!--/ko-->
					<!--/ko-->

					<!-- ko if: hasErrors -->
					<div class="error-list" data-bind="foreach: value.rcc.validation.errors" >
						<span class="error-item" data-bind="css: { 'non-fatal': !fatal, 'fatal': fatal }">
							<span class="fa" data-bind="css: { 'fa-exclamation-triangle': !fatal, 'fa-exclamation-circle': fatal }"></span>
							<span data-bind="text: message"></span>
						</span>
					</div>
					<!--/ko-->
				</label>
			</div>
			<!--/ko-->
			<!-- ko if: ma -->
				<label style="display: inline" data-bind="css: { 'accessed': value.rcc && value.rcc.accessed(), 'errors': hasErrors, 'fatal': hasFatalErrors }">
					<span class="question" style="width: 160px; padding: 5px" data-bind="html: question"></span>
				
					<!-- ko if: ko.utils.unwrapObservable($root.showRegvarNames) && value.rcc -->
						<span class="regvar-name" data-bind="text: value.rcc.regvarName"></span>
					<!-- /ko -->
					<!-- ko with: _.extend(control, { event: createControlEvent($parent) }) -->
						<!-- ko if: type == 'select' -->
							<select class="ctrl" data-bind="rcc-list: options.value, optionsCaption: options.caption, event: event, optionsText: options.text"></select>
						<!--/ko-->
						<!-- ko if: type == 'text' -->
							<input class="ctrl" type="text" data-bind="rcc-value: options.value, event: event" />
						<!--/ko-->
						<!-- ko if: type == 'datepicker' -->
							<input class="ctrl" type="text" data-bind="rcc-datepicker: options, event: event" />
						<!--/ko-->
						<!-- ko if: type == 'staticText' -->
							<span data-bind="text: options.value, rcc-var: options.value"></span>
						<!--/ko-->
						<!-- ko if: type == 'decimal' -->
							<input class="ctrl" type="text" data-bind="rcc-decimal: options, event: event" />
						<!--/ko-->
						<!-- ko if: type == 'textarea' -->
							<textarea cols="76" class="ctrl" type="text" data-bind="rcc-value: options.value, event: event" rows="4"></textarea>
						<!--/ko-->
						<!-- ko if: type == 'checkbox' -->
							<input type="checkbox" data-bind="rcc-checked: options.value, event: event">
						<!--/ko-->
					<!--/ko-->
					<!-- ko if: placeholder -->
						<!-- ko text: placeholder --><!--/ko-->
					<!--/ko-->

					<!-- ko if: hasErrors -->
					<div class="error-list" data-bind="foreach: value.rcc.validation.errors" >
						<span class="error-item" data-bind="css: { 'non-fatal': !fatal, 'fatal': fatal }">
							<span class="fa" data-bind="css: { 'fa-exclamation-triangle': !fatal, 'fa-exclamation-circle': fatal }"></span>
							<span data-bind="text: message"></span>
						</span>
					</div>
					<!--/ko-->
				</label>
			<!--/ko-->


		*/return undefined;})
	});
})(window['inca'], window['RCC'], window['ko'], window['_']);

(function(RCC, ko, _) {
	ko.components.register('m-a', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
			if (!params.table) throw new Error('Parameter "table" is required.');

			_.defaults(params, {
				question: '',
				info: '',
				minRows: 0,
				maxRows: Infinity,
				controls: [],
				showLegendIfEmpty: true
			});

			self.extendRoot = function($parent) {
				return new function() {
					var self = this;
					_.extend(self, _.pick(params, 'question', 'info', 'minRows', 'maxRows'));
					
					self.innerContent = (function() {
						var c = params.innerContent || [];
						return _.map(_.isArray(c) ? c : [c], function(x) {
							return _.defaults(x.object ? x : { object: x }, { type: 'component', margin: true });
						});
					})();

					self.tables = $parent.$$[params.table];
					self.showLegend = function() {
				 		if (self.tables().length > 0) return true;
						return (_.isBoolean(params.showLegendIfEmpty) ? params.showLegendIfEmpty : params.showLegendIfEmpty($parent));
					};

					self.controls = _.map(params.controls, function(control) {
						if (_.isString(control)) control = { name: control };
						var r = _.defaults(_.pick(control, 'question', 'info', 'placeholder', 'optionsText'), {
							question: '', info: '', placeholder: '', optionsText: 'text'
						});

						r.extendControl = function($parents) {
							return new function() {
								var x = this;
								x.value = $parents[0][control.name];
								x.type = control.type || { datetime: 'datepicker', list: 'select', boolean: 'checkbox' }[x.value.rcc.term.dataType] || 'text';
								x.options = _.defaults(control.options || {}, (function() {
									switch (control.type) {
										case 'datepicker': return { value: x.value, dateToday: inca.serverDate, readOnly: inca.form.isReadOnly };
										case 'decimal': return { valueObservable: x.value };
										case 'staticText':
											if( x.value() && typeof x.value() == 'object' && x.value().text ){
												return { value: x.value().text };
											}
											return { value: x.value };
										default: return { value: x.value };
									}
								})());

								if (_.isFunction(r.optionsText)) x.optionsText = function(listObject) {
									return control.optionsText(listObject, $parents);
								}
							};
						};
						return r;
					});
				}
			};
		},
		template: RCC.Vast.Utils.extractSource(function() {/**@preserve
			<div class="multi-answer-template" data-bind="with: _.extend($data, extendRoot($parent))">
				<!-- ko if: question && showLegend() -->
					<div class="r">
						<div class="b" data-bind="text: question"></div>
						<!-- ko if: info -->
							<span class="help" data-bind="html: info"></span>
						<!--/ko-->
					</div>
				<!--/ko-->
				<div data-bind="foreach: tables">
					<div data-bind="css: { 'multi-item': $parent.controls.length != 0 && $parent.maxRows != 1, 'single-item': $parent.controls.length == 7}">
						<div class="s-inl" data-bind="foreach: $parent.controls">
							<div class="s-inl" data-bind="with: _.extend($data, extendControl($parents))">
								<s-a params="value: value, question: question || value.rcc.regvar.label, ma: true"></s-a>
							</div>
						</div>
						<!-- ko foreach: $parent.innerContent -->
							<div class="lvl lvl-mark" data-bind="css: { 'no-inner-template-margin': !margin }">
								<!-- ko with: $parent -->
									<!-- ko if: $parent.type == 'component' -->
										<!--ko component: $parent.object --><!--/ko-->
									<!--/ko-->
									<!-- ko if: $parent.type == 'template' -->
										<!--ko template: $parent.object --><!--/ko-->
									<!--/ko-->
								<!--/ko-->
							</div>
						<!--/ko-->
						<!-- ko if: $parent.tables().length > $parent.minRows -->
							<div class="r"></div>
							<button class="btn btn-danger btn-xs" data-bind="enable: !$root.$form.isReadOnly, click: $parent.tables.rcc.remove">Ta bort</button>
						<!--/ko-->
					</div>
				</div>
				<div class="r" data-bind="visible: $parent.tables().length < maxRows && showLegend()">
					<button class="btn btn-primary btn-xs" style="background-color: #330855" data-bind="enable: !$root.$form.isReadOnly, click: function() { $parent.tables.rcc.add(); }">Lägg till</button>
				</div>
			</div>
		*/return undefined;})
	});
})(window['RCC'], window['ko'], window['_']);

(function(inca, RCC, ko, _) {
	ko.components.register('s-a-b', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
    			if (!params.value && params.controlType != 'staticText') throw new Error('Parameter "value" is required.');
    			var self = this;
    			self.regvar = params.value;
    			self.question = (params.value && params.value.rcc && params.value.rcc.regvar && params.value.rcc.regvar.label) ?
                    params.value.rcc.regvar.label : params.question;
    			self.info = params.info;
    			self.lastSelectedValue = ko.observable(self.regvar() && self.regvar().value);
    			self.tooltip = params.help || params.value.rcc.regvar.description;
    			self.fullWidthText = params.fullWidthText || false;
          self.info = params.info;
          self.indrag = params.indrag || false;

          
          if(!params.data) {
              self.data = [
                  { id: 'nej', text: 'Nej', value: 0 },
                  { id: 'ja', text: 'Ja', value: 1 },
                  { id: 'uppgift_saknas', text: 'Uppgift saknas', value: 96 },
              ];
          }
          else
              self.data = params.data;

          self.setRegvar = function(obj) {
              if(!inca.form.isReadOnly) {
                  if(self.regvar() && self.lastSelectedValue() == obj.value)
                      self.regvar(null);
                  else {
                      var res = _.find(self.data, function(item) { return obj.id == item.id; });
                      self.regvar(ko.utils.arrayFirst(self.regvar.rcc.term.listValues, function (list) {
                          return list.value == res.value;
                      }));
                  }
                  self.regvar.rcc.accessed(true);
                  self.lastSelectedValue(self.regvar() && self.regvar().value);
              } 
           };

           self.handleKeyEvent = function(data, event) {
               if(event.keyCode === 32 || event.which === 32 ||
                   event.keyCode === 13 || event.which === 13) {
                   self.setRegvar(data);
               }
           };

           self.handleBlurEvent = function() {
               self.regvar.rcc.accessed(true);
           };


           self.helpPopover = ko.pureComputed(function() {
               var regvar = params.value && params.value.rcc && params.value.rcc.regvar;
               var helpText = regvar && regvar.helpText ?
                   regvar.helpText : self.tooltip;
               return helpText ? {
                       title: undefined,
                       content:  helpText,
                       trigger: 'hover',
                       html: true
                   } : null;
           });

           if (self.showReviewerInclude)
               self.showReviewerInclude = !!(inca.user.role.isReviewer && !inca.form.isReadOnly && self.regvar.rcc && self.enabled);

           self.hasErrors = ko.computed(function () {
               var errors = params.value && params.value.rcc && params.value.rcc.validation && params.value.rcc.validation.errors();
               return errors && errors.length;
           });

           self.hasFatalErrors = ko.computed(function () {
               var errors = params.value && params.value.rcc && params.value.rcc.validation && params.value.rcc.validation.errors();
               return !!(errors && errors.filter(function (e) { return e.fatal; }).length);
           });
       },
       template: RCC.Vast.Utils.extractSource(function() {/**@preserve
           <div class="single-answer-template r" data-bind="attr: { title: tooltip }, style: { 'margin-left': indrag ? '20px' : '0px' }">
               <label data-bind="css: { 'accessed': regvar.rcc && regvar.rcc.accessed(), 'errors': hasErrors, 'fatal': hasFatalErrors }">
                   <span data-bind="popover: helpPopover, html: question + (helpPopover() ? ' <i class=\'fa fa-info-circle\'></i>' : '') + (info ? '<br /><span class=\'help\'>' + info + '</span>' : ''), css: { 'full-width-text': fullWidthText }" class="question"></span>
                   <!-- ko if: ko.utils.unwrapObservable($root.showRegvarNames) && regvar.rcc -->
                       <span class="regvar-name" data-bind="text: regvar.rcc.regvarName"></span>
                   <!-- /ko -->
                   <div class="btn-group" data-bind="foreach: data, css: { 's-a-b-fatal': regvar.rcc && regvar.rcc.accessed() && hasFatalErrors() }">   
                        <label
                            class="btn btn-default"
                            tabindex="0"
                            data-bind="click: $parent.setRegvar, css: {'buttondisabled': inca.form.isReadOnly, 'buttondisabled-selected': $parent.regvar() && $parent.regvar().value == value, 'btn-primary' : $parent.regvar() && $parent.regvar().value == value }, attr: { for: $parent.regvar.rcc.regvarName + text + $index() }, text: text, event: { keypress: $parent.handleKeyEvent, blur: $parent.handleBlurEvent }"
                        ></label>
                   </div>
                   <!-- ko if: hasErrors -->
                       <div class="error-list" data-bind="foreach: regvar.rcc.validation.errors" >
                           <span class="error-item" data-bind="css: { 'non-fatal': !fatal, 'fatal': fatal }">
                               <span class="fa" data-bind="css: { 'fa-exclamation-triangle': !fatal, 'fa-exclamation-circle': fatal }"></span>
                               <span data-bind="text: message"></span>
                           </span>
                       </div>
                   <!--/ko-->
               </label>
           </div>
       */return undefined;})
	});
})(window['inca'], window['RCC'], window['ko'], window['_']);

(function(inca, RCC, ko, _) {
	ko.components.register('intergrated-manual', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
			var self = this;
			self.title = params.title;
			self.label = params.label;
			self.header = params.header;
			self.table = params.table;
			self.visible = params.visible;
			self.registrering = params.registrering;
			self.filterValues = ['N07BC01', 'N07BC51', 'N07BC02', 'N07BB04', 'V03AB15', 'N07BB03', 'N07BB01', 'N07BB05', 'N07BC05', 'N06BA09', 'N06AX12', 'N06BA02', 'N06BA12', 'N06BA04',   'N05CD08', 'N03AE01', 'N05BA01',  'N05BA04',  'N05BA06', 'N05BA12', 'N05CM02', 'N05CF02', 'N05CF01', 'N03AX12', 'N03AF01', 'N03AX16', 'N03AX11', 'A11DB', 'N07BA01', 'N07BA03', 'C02AC01', 'N02AA05' ];
            self.lakemedelArr = [
                { header: 'Opioidberoende', data: [] },
                { header: 'Alkohol', data: [] },
                { header: 'ADHD', data: [] },
                { header: 'Bensodiazepiner', data: [] },
                { header: 'Antiepileptika', data: [] },
                { header: 'Vitamin B1', data: [] },
                { header: 'Nikotin', data: [] },
				{ header: 'Opioidberoende, övriga läkemedel', data: [] },
            ];

            _.each(self.filterValues, function(code) {
            	var lakemedelListValues = inca.offline ? inca.lakemedelListValue  : inca.form.metadata.Lakemedel.regvars.lakemedel.listValues;
				var obj =  _.find(lakemedelListValues, function(listvalue) { 
					return code == listvalue.value; 
				});
				if(obj.value == 'N07BB04')
					self.lakemedelArr[0].data.push(obj);
                if(obj.value.substring(0,5) == 'N07BC' || obj.value.substring(0,5) == 'V03AB')
                    self.lakemedelArr[0].data.push(obj);
                else if(obj.value.substring(0,5) == 'N07BB')
                    self.lakemedelArr[1].data.push(obj);
                else if(obj.value.substring(0,3) == 'N06')
                    self.lakemedelArr[2].data.push(obj);
                else if(obj.value.substring(0,3) == 'N05' || obj.value.substring(0,5) == 'N03AE')
                    self.lakemedelArr[3].data.push(obj);
                else if(obj.value.substring(0,3) == 'N03')
                    self.lakemedelArr[4].data.push(obj);
                else if(obj.value.substring(0,3) == 'A11')
                    self.lakemedelArr[5].data.push(obj);
                else if(obj.value.substring(0,5) == 'N07BA')
                    self.lakemedelArr[6].data.push(obj);
				else if(obj.value == 'C02AC01' || obj.value == 'N02AA05') 
                    self.lakemedelArr[7].data.push(obj);

					

            });

			self.currentData = ko.observableArray(self.lakemedelArr[0].data);
	

			self.addLakemedel = function(obj) {
				var row = self.table.rcc.add();
				row.lakemedel(ko.utils.arrayFirst(row.lakemedel.rcc.term.listValues, function (list) { 
					return list.value == obj.value;
				}));
			};

			self.setPage = function(obj) {
				self.currentData([]);
				_.each(self.lakemedelArr, function(lak) {
					if(lak.header == obj.header)
						self.currentData(lak.data);
				});
			};

			self.removeValue = function(obj) {
				self.regvar(null);
				self.visible(false);
				self.regvar.rcc.accessed(true);
			};

			self.hasErrors = ko.computed(function () {
                var errors = self.regvar && self.regvar.rcc && self.regvar.rcc.validation && self.regvar.rcc.validation.errors();
                return errors && errors.length;
            });

            self.hasFatalErrors = ko.computed(function () {
                var errors = self.regvar && self.regvar.rcc && self.regvar.rcc.validation && self.regvar.rcc.validation.errors();
                return !!(errors && errors.filter(function (e) { return e.fatal; }).length);
            });

            self.helpPopover = ko.pureComputed(function() {
            	var regvar = params.value && params.value.rcc && params.value.rcc.regvar;
            	var helpText = regvar && regvar.helpText ?
					regvar.helpText : params.help ? params.help :
						regvar && regvar.description ?  regvar.description : '';

                return helpText ? {
                        title: undefined,
                        content:  helpText,
                        trigger: 'hover',
                        html: true
                    } : null;
            });

		},

		template: RCC.Vast.Utils.extractSource(function() {/**@preserve
			<!-- ko if: visible() -->
			<div class="rcc-modal" tabindex="-1">
	            <div class="modal-dialog" style="width: 700px; margin-left: 40px">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <button data-bind="click: function() { visible(false); }" class="close">&times;</button>
	                        <h2 class="modal-title" data-bind="text: title"></h2>
	                        <ul class="pagination" style="margin-bottom: 0px">
							  <!-- ko foreach: lakemedelArr --> 
								  <li class="page-item"><a class="page-link" style="background-color: #330855; color: #FFFFFF" href="#" data-bind="text: header, click: $parent.setPage"></a></li>
							  <!--/ko-->
							</ul>
	                    </div>
	                    <div class="modal-body">
	                        <h4 style="color: red; margin-top: 0px" data-bind="text: header"></h4>
							<table class="table table-hover table-condensed" data-bind="visible: currentData().length > 0">
	                            <thead>
	                                <tr>
	                                    <th style="width: 10%">Kod</th>
	                                    <th style="width: 80%">Beskrivning</th>
	                                    <th style="width: 10%">Åtgärd</th>
	                                </tr>
	                            </thead>
	                            <tbody data-bind="foreach: currentData">
	                                <tr>
	                                    <td data-bind="html: '<b>' + value + '</b>'"></td>
	                                    <td data-bind="html: text, attr: { 'colspan': 1 }"></td>
	                                    <td>
	                                        <button class="btn btn-primary btn-xs" style="background-color: #330855" data-bind="enable: !$root.$form.isReadOnly, click: $parent.addLakemedel">Välj</button>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	            </div>
        	</div>
        	<!--/ko-->
        	<table class="table table-hover table-condensed" style="width: 800px; margin-bottom: 5px">
	            <thead>
	                <tr>
	                    <th style="width: 15%">Valt läkemedel</th>
	                    <th style="width: 15%"></th>
	                    <th style="width: 22%"></th>
	                    <th style="width: 40%"></th>
	                    <th style="width: 8%"></th>
	                </tr>
	            </thead>
	            <tbody data-bind="foreach: table()">
	                <tr>
						<td data-bind="text: lakemedel() && lakemedel().text"></td>
					    <td>
					    	<!-- ko if: $parent.registrering() && $parent.registrering().value == '1' && lakemedel() && (lakemedel().value == 'N07BC01' || lakemedel().value == 'N07BC02' || lakemedel().value == 'N07BC05' || lakemedel().value == 'V03AB15' || lakemedel().value == 'N07BB04' || lakemedel().value == 'N07BC51') -->
						        <b>Dos (mg)</b> <input class="ctrl" style="width: 40px" type="text" data-bind="rcc-value: dos"/><br />
						        <!-- ko if: dos.rcc.accessed() -->
							        <!-- ko foreach: dos.rcc.validation.errors() -->
							        	<span class="fa fa-exclamation-triangle" style="color: #ffd351"></span>
										<span style="font-size: 11px" data-bind="text: message"></span>
									<!--/ko-->
								<!--/ko-->
							<!--/ko-->
					    </td>
					    <td>
					    	<!-- ko if: $parent.registrering() && $parent.registrering().value == '1' && lakemedel() && (lakemedel().value == 'N07BC01' || lakemedel().value == 'N07BC02' || lakemedel().value == 'N07BC05' || lakemedel().value == 'V03AB15' || lakemedel().value == 'N07BB04' || lakemedel().value == 'N07BC51') -->
						    	<b>Dosform </b><select class="ctrl" data-bind="rcc-list: dosform, optionsCaption: '– Välj –', optionsText: 'text'"></select>
						    	<!-- ko if: dosform.rcc.accessed() -->
								    <!-- ko foreach: dosform.rcc.validation.errors() -->
								        	<span class="fa" data-bind="css: { 'fa-exclamation-triangle': !fatal, 'fa-exclamation-circle': fatal }, style: { color: fatal ? 'red' : '#ffd351' }"></span>
											<span style="font-size: 11px" data-bind="text: message"></span>
									<!--/ko-->
								<!--/ko-->
							<!--/ko-->
					    </td>
	                    <td>
	                    	<!-- ko if: $parent.registrering() && $parent.registrering().value == '1' && lakemedel() && (lakemedel().value == 'N07BC01' || lakemedel().value == 'N07BC02' || lakemedel().value == 'N07BC05' || lakemedel().value == 'V03AB15' || lakemedel().value == 'N07BB04' || lakemedel().value == 'N07BC51') -->
		                    	<b>Beredningsform </b><select class="ctrl" data-bind="rcc-list: beredningsform, optionsCaption: '– Välj –', optionsText: 'text'"></select><br />
		                    	<!-- ko if: beredningsform.rcc.accessed() -->
							        <!-- ko foreach: beredningsform.rcc.validation.errors() -->
							        	<span class="fa" data-bind="css: { 'fa-exclamation-triangle': !fatal, 'fa-exclamation-circle': fatal }, style: { color: fatal ? 'red' : '#ffd351' }"></span>
										<span style="font-size: 11px" data-bind="text: message"></span>
									<!--/ko-->
								<!--/ko-->
							<!--/ko-->
						</td>
	                    <td><button class="btn btn-primary btn-xs btn-danger" data-bind="enable: !$root.$form.isReadOnly, click: $parent.table.rcc.remove">Ta bort</button></td>
	                </tr>
	            </tbody>
        	</table>
        	<em data-bind="visible: table().length == 0">Inga valda läkemedel.</em><br />
        	<button data-bind="enable:!$root.$form.isReadOnly, click: function() { visible(true); }" class="btn btn-primary btn-xs" style="background-color: #330855; margin-top: 5px">
	            <span>Lägg till</span>
	        </button>
		*/return undefined;})
	});
})(window['inca'], window['RCC'], window['ko'], window['_']);


(function ( $, RCC, inca ) {
    var lists = {
        Registreringstyp: [
            { text: 'Nyregistrering', value: '1' },
            { text: '3-månadersuppföljning', value: '2' },
            { text: 'Årsuppföljning', value: '3' },
            { text: 'Avslut', value: '4' },
        ],

        JaNej: [
            { text: 'Ja', value: 'ja' },
            { text: 'Nej', value: 'nej' },
        ],

        JaNejIU: [
            { text: 'Ja', value: 'ja' },
            { text: 'Nej', value: 'nej' },
            { text: 'Uppgift saknas', value: 'uppgift_saknas' }
        ],

        Initiativtagare: [
            { text: 'Eget initiativ', value: '1' },
            { text: 'Familj, vänner', value: '2' },
            { text: 'Annan behandlingsenhet för beroendevård', value: '3' },
            { text: 'Primärvård', value: '4' },
            { text: 'Psykiatrisk hälso-och sjukvård (ej beroendevård)', value: '5' },
            { text: 'Somatisk slutenvård eller akutmottagning', value: '6' },
            { text: 'Socialtjänst', value: '7' },
            { text: 'HVB-hem', value: '8' },
            { text: 'Domstol', value: '9' },
            { text: 'Kriminalvård', value: '10' },
            { text: 'Polis', value: '11' },
            { text: 'Arbetsgivare, Företagshälsovård', value: '12' },
            { text: 'Hälsovård inom skola/utbildning', value: '13' },
            { text: 'Brukarorganisation', value: '14' },
            { text: 'Sprututbytesprogram', value: '15' },
            { text: 'Annan initiativtagare', value: '16' },
            { text: 'Uppgift saknas', value: '96' },
        ],

        InjiceratDroger: [
            { text: 'Nej, aldrig injicerat', value: '1' },
            { text: 'Ja, injicerat någon gång i livet, men inte under de senaste 12 månaderna', value: '2' },
            { text: 'Ja, injicerat någon gång de senaste 12 månaderna, men inte under de senaste 30 dagarna', value: '3' },
            { text: 'Ja, injicerar för närvarande (under de senaste 30 dagarna)', value: '4' },
            { text: 'Uppgift saknas', value: '96' },
        ],
        Boendeform: [
            { text: 'Egen bostad', value: '1' },
            { text: 'Inneboende', value: '2' },
            { text: 'Hemlös', value: '3' },
            { text: 'HVB-hem', value: '4' },
            { text: 'Kriminalvård', value: '5' },
            { text: 'Uppgift saknas', value: '96' },
        ],

        AvslutOrsak: [
            { text: 'Avliden', value: '1' },
            { text: 'Behandlingsmål har uppnåtts', value: '2' },
            { text: 'Patienten är av medicinska skäl förhindrad att fortsätta', value: '3' },
            { text: 'Annan orsak', value: '4' },
            { text: 'Patientens eget initiativ', value: '5' },
            { text: 'Vård enligt vårdplan har genomförts', value: '6' },
            { text: 'Patienten har uteblivit, information om orsak saknas', value: '7' },
            { text: 'Fortsatta insatser faller ej inom enhetens uppdrag', value: '8' },
            { text: 'Ytterligare insatser bedöms inte kunna ha effekt', value: '9' },
            { text: 'Patientens ålder gör att fortsatt insats ej ingår i enhetens uppdrag', value: '10' },
            { text: 'Avslut i förtid på vårdgivarens initiativ', value: '11' },
        ],

        Formulärtyp: [
            { text: 'Öppenvård', value: '1' },
            { text: 'Slutenvård', value: '2' },
        ],

        SlutenAvslut: [
            { text: 'Avliden', value: '1' },
            { text: 'Annan orsak', value: '2' },
            { text: 'Ej i behov av fortsatt slutenvård', value: '3' },
            { text: 'Behov av slutenvård inom annan specialitet', value: '4' },
            { text: 'Behov av slutenvård samma specialitet annan verksamhet', value: '5' },
            { text: 'Patienten medverkar ej i planerade åtgärder', value: '6' },
            { text: 'Disponibel vårdplats saknas', value: '7' },
            { text: 'Patientens eget initiativ', value: '8' },
            { text: 'HVB-hem, behandlingshem', value: '9' },
            { text: 'Uppgift saknas', value: '96' },
        ],

        CGIS: [
            { text: 'Ej bedömt', value: '1' },
            { text: 'Normal, inte alls sjuk', value: '2' },
            { text: 'Gränsfall för psykisk sjukdom', value: '3' },
            { text: 'Lindrigt sjuk', value: '4' },
            { text: 'Måttligt sjuk', value: '5' },
            { text: 'Påtagligt sjuk', value: '6' },
            { text: 'Allvarligt sjuk', value: '7' },
            { text: 'Bland de mest extremt sjuka patienterna', value: '8' },
        ],

        Vardplan: [
            { text: 'Ja', value: '1' },
            { text: 'Ja, en samordnad individuell plan mellan kommun och hälso- och sjukvård (SIP) (KVÅ AU124, AU125)', value: '2' },
            { text: 'Nej', value: '3' },
            { text: 'Uppgift saknas', value: '96' },
        ],

        EQ5D1: [
            { text: 'Jag har inga svårigheter med att gå omkring', value: '1' },
            { text: 'Jag har lite svårigheter med att gå omkring', value: '2' },
            { text: 'Jag har måttliga svårigheter med att gå omkring', value: '3' },
            { text: 'Jag har stora svårigheter med att gå omkring', value: '4' },
            { text: 'Jag kan inte gå omkring', value: '5' },
        ],

        EQ5D2: [
            { text: 'Jag har inga svårigheter med att tvätta mig eller klä mig', value: '1' },
            { text: 'Jag har lite svårigheter med att tvätta mig eller klä mig', value: '2' },
            { text: 'Jag har måttliga svårigheter med att tvätta mig eller klä mig', value: '3' },
            { text: 'Jag har stora svårigheter med att tvätta mig eller klä mig', value: '4' },
            { text: 'Jag kan inte tvätta mig eller klä mig', value: '5' },
        ],
        EQ5D3: [
            { text: 'Jag har inga svårigheter med att utföra mina vanliga aktiviteter', value: '1' },
            { text: 'Jag har lite svårigheter med att utföra mina vanliga aktiviteter', value: '2' },
            { text: 'Jag har måttliga svårigheter med att utföra mina vanliga aktiviteter', value: '3' },
            { text: 'Jag har stora svårigheter med att utföra mina vanliga aktiviteter', value: '4' },
            { text: 'Jag kan inte utföra mina vanliga aktiviteter', value: '5' },
        ],
        EQ5D4: [
            { text: 'Jag har varken smärtor eller besvär', value: '1' },
            { text: 'Jag har lätta smärtor eller besvär', value: '2' },
            { text: 'Jag har måttliga smärtor eller besvär', value: '3' },
            { text: 'Jag har svåra smärtor eller besvär', value: '4' },
            { text: 'Jag har extrema smärtor eller besvär', value: '5' },
        ],
        EQ5D5: [
            { text: 'Jag är varken orolig eller nedstämd', value: '1' },
            { text: 'Jag är lite orolig eller nedstämd', value: '2' },
            { text: 'Jag är ganska orolig eller nedstämd', value: '3' },
            { text: 'Jag är mycket orolig eller nedstämd', value: '4' },
            { text: 'Jag är extremt orolig eller nedstämd', value: '5' },
        ],

        HepatitBVaccination: [
            { text: 'Ja, en dos', value: '1' },
            { text: 'Ja, två doser', value: '2' },
            { text: 'Ja, tre doser', value: '3' },
            { text: 'Nej', value: '4' },
            { text: 'Uppgift saknas', value: '96' },
        ],

        Sysselsattning: [
            { text: 'Arbete på den reguljära arbetsmarknaden', value: '1' },
            { text: 'Arbete inom skyddad sysselsättning', value: '2' },
            { text: 'Studier utan särskilt stöd eller anpassad studiegång', value: '3' },
            { text: 'Studier med särskilt stöd eller anpassad studiegång', value: '4' },
            { text: 'Studier på grundsärskola, gymnasiesärskola eller särvux', value: '5' },
            { text: 'Arbetssökande utan arbetsmarknadsåtgärder', value: '6' },
            { text: 'Arbetssökande med arbetsmarknadsåtgärder', value: '7' },
            { text: 'Sjukskriven med arbetslivsrehabiliterande åtgärd. Saknar arbete på reguljära arbetsmarknaden', value: '8' },
            { text: 'Bland de mest extremt sjuka patienterna', value: '9' },
            { text: 'Pensionär', value: '10' },
            { text: 'Annan sysselsättning', value: '11' },
            { text: 'Saknar regelbunden sysselsättning utanför hemmet', value: '12' },
            { text: 'Uppgift saknas', value: '96' },
        ],
        
        Diagnostyp: [
            { text: 'Beroendediagnos', value: '1' },
            { text: 'Annan psykiatrisk diagnos', value: '2' },
            { text: 'Annan somatisk diagnos', value: '3' },
        ],

        Dosform: [
            { text: 'Dygnsdos', value: '1' },
            { text: 'Månadsdos', value: '2' },
            { text: 'Veckodos', value: '3' },
        ], 
        Beredningsform: [
            { text: 'Resori tablett', value: '1' },
            { text: 'Frystorkad tablett', value: '2' },
            { text: 'Injektionsvätska inkl. depot', value: '3' },
            { text: 'Film', value: '4' },
            { text: 'Oral lösning', value: '5' },
            { text: 'Tablett', value: '6' },
            { text: 'Nässpray', value: '7' },
        ],
        
        Vardatgard: [
            { text: 'AU117 12-stegsbehandling', value: 'AU117' },
            { text: 'DU008 Systematisk psykologisk behandling, psykodynamisk (PDT)', value: 'DU008' },
            { text: 'DU008 Systematisk psykologisk behandling, annan', value: 'DU008' },
            { text: 'DU010 Systematisk psykologisk behandling, kognitiv', value: 'DU010' },
            { text: 'DU011 Systematisk psykologisk behandling, kognitiv-beteendeterapeutisk (KBT)', value: 'DU011' },
            { text: 'DU013 Systematisk psykologisk behandling, mentaliseringsbaserad (MBT)', value: 'DU013' },
            { text: 'DU014 EMDR, systematisk psykologisk behandling', value: 'DU014' },
            { text: 'DU015 ERGT, systematisk psykologisk behandling', value: 'DU015' },
            { text: 'DU020 Systematisk psykologisk behandling, systemisk', value: 'DU020' },
            { text: 'DU021 Systematisk psykologisk behandling, dialektisk-beteendeterapeutisk (DBT)', value: 'DU021' },
            { text: 'DU022 Systematisk psykologisk behandling, interpersonell (IPT)', value: 'DU022' },
            { text: 'DU024 Familjeterapi, funktionell', value: 'DU024' },
            { text: 'DU118 Motiverande samtal (MI)', value: 'DU118' },
            { text: 'DU119 Återfallsprevention (ÅFP)', value: 'DU119' },
            { text: 'DU120 MET, Motivational Enhancement Therapy', value: 'DU120' },
            { text: 'DU023 Psykopedagogisk behandling', value: 'DU023' },
        ],

        Drogtyper: [
            { text: 'Alkohol', value: '1' },
            { text: 'Heroin', value: '2' },
            { text: 'Metadon', value: '3' },
            { text: 'Mono-buprenorfin (t.ex. subutex)', value: '4' },
            { text: 'Buprenorfin-naloxon (suboxone)', value: '5' },
            { text: 'Andra opioider (t.ex tramadol, oxikodon, fentanyl)', value: '6' },
            { text: 'Kokain', value: '7' },
            { text: 'Amfetaminer', value: '8' },
            { text: 'Stimulantia med hallucinogen effekt (t.ex. MDMA)', value: '9' },
            { text: 'Andra narkotikaklassade stimulantia', value: '10' },
            { text: 'Bensodiazepiner', value: '11' },
            { text: 'Zolpidem, zopiklon eller zaleplon', value: '12' },
            { text: 'Andra narkotikaklassade dämpande substanser (t.ex. pregabalin)', value: '13' },
            { text: 'Hallucinogener (inklusive LSD)', value: '14' },
            { text: 'Andra opioider (t.ex tramadol, oxikodon, fentanyl)', value: '15' },
            { text: 'Syntetiska cannabinoider (t.ex. Spice)', value: '16' },
            { text: 'Lösningsmedel', value: '17' },
            { text: 'GHB', value: '18' },
            { text: 'Anabola androgena steroider (inklusive testosteron)', value: '19' },
            { text: 'Andra prestationshöjande substanser', value: '20' },
            { text: 'Annan substans', value: '21' },
            { text: 'Uppgift saknas', value: '96' },
        ],
        Lakemedel: [
            { value: 'N05BA07', text: 'Adinazolam' },
            { value: 'N07BB03', text: 'Akamprosat' },
            { value: 'N05BA12', text: 'Alprazolam' },
            { value: 'N05AX12', text: 'Aripiprazol' },
            { value: 'N05AH05', text: 'Asenapin' },
            { value: 'N06BA09', text: 'Atomoxetin' },
            { value: 'M03BX01', text: 'Baklofen' },
            { value: 'A11DA03', text: 'Benfotiamin' },
            { value: 'N05AX16', text: 'Brexpiprazol' },
            { value: 'N05BA08', text: 'Bromazepam' },
            { value: 'N07BC01', text: 'Buprenorfin' },
            { value: 'N07BC51', text: 'Buprenorfin, kombinationer (Ex. Buprenorfin + Naloxon)' },
            { value: 'N06AX12', text: 'Bupropion' }, 
            { value: 'J05AX14', text: 'Daclatasvir' },
            { value: 'J05AX16', text: 'Dasabuvir' },
            { value: 'N06BA02', text: 'Dexamfetamin' },
            { value: 'N05BA01', text: 'Diazepam' },
            { value: 'N05BA05', text: 'Dikaliumklor' },  
            { value: 'N06AA18', text: 'Dimetakrin' },
            { value: 'N07BB01', text: 'Disulfiram' },
            { value: 'J05AP54', text: 'Elbasvir och Grazoprevir' },
            { value: 'N05BA17', text: 'Fludiazepam' },
            { value: 'N05AF01', text: 'Flupentixol' },   
            { value: 'N03AX12', text: 'Gabapentin' },    
            { value: 'C02AC02', text: 'Guanfacin' }, 
            { value: 'N05BA13', text: 'Halazepam' },  
            { value: 'N05AD01', text: 'Haloperidol' },   
            { value: 'N03AF01', text: 'Karbamazepin' },
            { value: 'N05AX15', text: 'Kariprazin' }, 
            { value: 'N05BA10', text: 'Ketazolam' }, 
            { value: 'N05BA09', text: 'Klobazam' }, 
            { value: 'N05CM02', text: 'Klometiazol' }, 
            { value: 'N06AA04', text: 'Klomipramin' }, 
            { value: 'N03AE01', text: 'Klonazepam' },
            { value: 'C02AC01', text: 'Klonidin' },
            { value: 'N05AF02', text: 'Klopentixol' }, 
            { value: 'N05BA02', text: 'Klordiazepox' },  
            { value: 'N05AF03', text: 'Klorprotixen' },  
            { value: 'N05AH02', text: 'Klozapiz' },
            { value: 'N05AH04', text: 'Kvetiapin' },
            { value: 'N03AX09', text: 'Lamotrigin' },
            { value: 'N05AA02', text: 'Levomepromazin' }, 
            { value: 'N07BC05', text: 'Levometadon' }, 
            { value: 'N06BA12', text: 'Lisdexamfetamin' },
            { value: 'N05AN01', text: 'Litium' },
            { value: 'N05BA06', text: 'Lorazepan' },     
            { value: 'N05CD06', text: 'Lormetazepam' },  
            { value: 'N05AH01', text: 'Loxapin' },
            { value: 'N05AE05', text: 'Lurasidon' }, 
            { value: 'N05BA03', text: 'Medazepam' }, 
            { value: 'N07BC02', text: 'Metadon' },
            { value: 'N06BA04', text: 'Metylfenidat' },  
            { value: 'N05CD08', text: 'Midazolam' },
            { value: 'N06AG02', text: 'Moklobemid' },
            { value: 'N05AE02', text: 'Molindon' }, 
            { value: 'N05AX10', text: 'Mosapramin' }, 
            { value: 'N05BA15', text: 'Kamazepam' },   
            { value: 'N05BA16', text: 'Nordazepam' },  
            { value: 'N05BA18', text: 'Etylloflazepat' },
            { value: 'N05BA22', text: 'Kloxazolam' },
            { value: 'N05BA24', text: 'Bentazepam' }, 
            { value: 'N05CD01', text: 'Flurazepam' }, 
            { value: 'N05CD02', text: 'Nitrazepam' }, 
            { value: 'N05CD02', text: 'Nitrazepam' },
            { value: 'N05CD03', text: 'Flunitrazepam' },
            { value: 'N05CD04', text: 'Estazolam' }, 
            { value: 'N05CD05', text: 'Triazolam' }, 
            { value: 'N05CD07', text: 'Temazepam' }, 
            { value: 'N05CD09', text: 'Brotizolam' }, 
            { value: 'N05CD10', text: 'Kvazepam' }, 
            { value: 'N05CD11', text: 'Loprazolam' },
            { value: 'N05CD13', text: 'Cinolazepam' },     
            { value: 'N05CD14', text: 'Remimazolam' },
            { value: 'N06AA01', text: 'Desipramin' }, 
            { value: 'N06AA02', text: 'Imipramin' }, 
            { value: 'N06AA03', text: 'Imipraminoxid' }, 
            { value: 'N06AA05', text: 'Opipramol' }, 
            { value: 'N06AA06', text: 'Trimipramin' },
            { value: 'N06AA07', text: 'Lofepramin' }, 
            { value: 'N06AA08', text: 'Dibensepin' },
            { value: 'N06AA09', text: 'Amitriptylin' },    
            { value: 'N06AA10', text: 'Nortriptylin' }, 
            { value: 'C02AC01', text: 'Klonidin' },
            { value: 'N02AA05', text: 'Oxikodon' },
            { value: 'N06AA12', text: 'Doxepin' }, 
            { value: 'N06AA13', text: 'Iprindol' },    
            { value: 'N06AA14', text: 'Melitracen' },
            { value: 'N06AA15', text: 'Butriptylin' }, 
            { value: 'N06AA16', text: 'Dosulepin' },   
            { value: 'N06AA17', text: 'Amoxapin' },    
            { value: 'N06AA21', text: 'Maprotilin' },  
            { value: 'N06AB02', text: 'Zimelidin' },   
            { value: 'N06AB03', text: 'Fluoxetin' },   
            { value: 'N06AB04', text: 'Citalopram' },  
            { value: 'N06AB05', text: 'Paroxetin' }, 
            { value: 'N06AB06', text: 'Sertralin' }, 
            { value: 'N06AB07', text: 'Alaproklat' },
            { value: 'N06AB08', text: 'Fluvoxamin' }, 
            { value: 'N06AB09', text: 'Etoperidon' }, 
            { value: 'N06AB10', text: 'Escitalopram' }, 
            { value: 'N06AC01', text: 'Maprotilin' },
            { value: 'N06AF01', text: 'Isokarboxazid' }, 
            { value: 'N06AF03', text: 'Fenelzin' },
            { value: 'N06AF04', text: 'Tranylcypromin' }, 
            { value: 'N06AF06', text: 'Iproklozid' }, 
            { value: 'N06AX01', text: 'Oxitriptan' }, 
            { value: 'N06AX02', text: 'Tryptofan' }, 
            { value: 'N06AX03', text: 'Mianserin' },
            { value: 'N06AX04', text: 'Nomifensin' }, 
            { value: 'N06AX05', text: 'Trazodon' }, 
            { value: 'N06AX06', text: 'Nefazodon' }, 
            { value: 'N06AX07', text: 'Minaprin' }, 
            { value: 'N06AX08', text: 'Bifemelan' }, 
            { value: 'N06AX09', text: 'Viloxazin' }, 
            { value: 'N06AX10', text: 'Oxaflozan' }, 
            { value: 'N06AX11', text: 'Mirtazapin' }, 
            { value: 'N06AX16', text: 'Venlafaxin' },
            { value: 'N06AX17', text: 'Milnacipran' },
            { value: 'N06AX18', text: 'Reboxetin' }, 
            { value: 'N06AX19', text: 'Gepiron' },
            { value: 'N06AX21', text: 'Duloxetin' },   
            { value: 'N06AX22', text: 'Agomelatin' },
            { value: 'N06AX23', text: 'Desvenlafaxin' }, 
            { value: 'N06AX26', text: 'Vortioxetin' },     
            { value: 'N06AX27', text: 'Esketamin' }, 
            { value: 'N06BA01', text: 'Amfetamin' }, 
            { value: 'N07BB05', text: 'Nalmefen' }, 
            { value: 'V03AB15', text: 'Naloxon' },
            { value: 'N07BB04', text: 'Naltrexon' },
            { value: 'N06AF02', text: 'Nialamid' },  
            { value: 'N07BA01', text: 'Nikotin' },
            { value: 'N05AH03', text: 'Olanzapin' },
            { value: 'J05AX67', text: 'Ombitasvir/Paritaprevir/Ritonavir' },
            { value: 'N05BA04', text: 'Oxazepam' },
            { value: 'N05AE01', text: 'Oxipertin' }, 
            { value: 'N03AF02', text: 'Oxkarbazepin' }, 
            { value: 'N05AX13', text: 'Paliperidon' },
            { value: 'N05AB03', text: 'Perfenazin' }, 
            { value: 'N05AX17', text: 'Pimavanserin' }, 
            { value: 'N05BA14', text: 'Pinazepam' },
            { value: 'N05BA11', text: 'Prazepam' },
            { value: 'N03AX16', text: 'Pregabalin' }, 
            { value: 'N05AX07', text: 'Protipendyl' },
            { value: 'N06AA11', text: 'Protriptylin' },  
            { value: 'N05AX08', text: 'Risperidon' },
            { value: 'N05AE03', text: 'Sertindol' }, 
            { value: 'J05AE14', text: 'Simeprevir' },
            { value: 'J05AX15', text: 'Sofosbuvir' },
            { value: 'J05AP55', text: 'Sofosbuvir och Velpatasvir' }, 
            { value: 'J05AP56', text: 'Sofosbuvir, Velpatasvir och Voxilaprevir' }, 
            { value: 'J05AX65', text: 'Sofosbuvir/Ledipasvir' }, 
            { value: 'A11DA02', text: 'Sulbutiamin' }, 
            { value: 'A11DA01', text: 'Tiamin (vitamin B1)' },
            { value: 'N05AF04', text: 'Tiotixen' }, 
            { value: 'N03AX11', text: 'Topiramat' },
            { value: 'N03AG01', text: 'Valproat (Valproinsyra)' }, 
            { value: 'N07BA03', text: 'Vareniklin' },    
            { value: 'A11DB', text: 'Vitamin B1 i kombination med B6 och/eller B12' },    
            { value: 'N05AE04', text: 'Ziprasidon' },
            { value: 'N05CF02', text: 'Zolpidem' },  
            { value: 'N05CF01', text: 'Zopiklon' },  
            { value: 'N05AX11', text: 'Zotepin' },
            { value: 'N05AF05', text: 'Zuklopentixol' },
        ]
    };

    var n = 0;
    $.each( lists,
    function (unused, list) {
        n++;
        $.each(list, function ( i, opt ) {
            if ( !opt.hasOwnProperty('id')) {
                opt.id = n * 10000 + i;
            }
        });
    });

    inca.lakemedelListValue = lists.Lakemedel;

    $.extend( true, inca.form, RCC.Utils.createDummyForm(
    {
        rootTable: 'BehandlingUppfoljning',

        regvars: 
        {
            abstinenskramper: lists.JaNejIU,
            deliriumTremens: lists.JaNejIU,
            registreringstyp: lists.Registreringstyp,
            informationsdatum: 'datetime',
            initiativtagare: lists.Initiativtagare,
            injiceratDroger: lists.InjiceratDroger,
            rokerTobak: lists.JaNejIU,
            vardatagandeStart: 'datetime',
            vardformLPT: lists.JaNejIU,
            barn: lists.JaNejIU,
            eq5d: 'decimal',
            hivProv: lists.JaNejIU,
            vikt: 'integer',
            boendeform: lists.Boendeform,
            avslut: lists.AvslutOrsak,
            langd: 'integer',
            formulartyp: lists.Formulärtyp,
            slutenAvslut: lists.SlutenAvslut,
            periodnummer: 'integer',
            audit: 'integer',
            auditC: 'integer',
            cgis: lists.CGIS,
            dudit: 'integer',
            hepatitBProv: lists.JaNejIU,
            hepatitCProv: lists.JaNejIU,
            inskrivningsdatum: 'datetime',
            laroprogram: lists.JaNejIU,
            psykiatriskDiagnos: lists.JaNejIU,
            somatiskDiagnos: lists.JaNejIU,
            substansdagarTotalt: 'integer',
            substansfriaManader: 'integer',
            utskrivningsdatum: 'datetime',
            vardplan: lists.Vardplan,
            vardformLVM: lists.JaNejIU,
            eq5d1: lists.EQ5D1,
            eq5d2: lists.EQ5D2,
            eq5d3: lists.EQ5D3,
            eq5d4: lists.EQ5D4,
            eq5d5: lists.EQ5D5,
            eq5d6: 'integer',
            avslutJaNej: lists.JaNejIU,
            hepatitBVaccination: lists.HepatitBVaccination,
            vardatagandeAvslut: 'datetime',
            sysselsattning: lists.Sysselsattning,
            vardplanSIP: lists.JaNejIU,
        },

        subTables:
        {
            Diagnoser: {
                regvars: {
                    diagnosVD: 'vd',
                    diagnostyp: lists.Diagnostyp
                },
                subTables: {}
            },
            Lakemedel: {
                regvars: {
                    lakemedel: lists.Lakemedel,
                    dos: 'decimal',
                    dosform: lists.Dosform,
                    beredningsform: lists.Beredningsform,
                },
                subTables: {}
            },
            Psykosocialbehandling: {
                regvars: {
                    vardatgard: lists.Vardatgard
                },
                subTables: {}
            },
            Substansbruk: {
                regvars: {
                    substans: lists.Drogtyper,
                    substansdagar: 'integer'
                },
                subTables: {}
            },
        }
    }));

})( window['jQuery'], window['RCC'], window['inca']);
(function (inca, window) {

	var Utils = {};

	if (!window.RCC)
		window.RCC = {};
	if (!window.RCC.Vast)
		window.RCC.Vast = {};
	if (!window.RCC.Vast.Utils)
		window.RCC.Vast.Utils = {};

	window.RCC.Vast.Utils.Offline = Utils;

	window.queryString = (function () {
		var r = {};
		if (location.search) {
			var col = location.search.substr(1).split('&');
			for (var i = 0; i < col.length; i++) {
				var objName = col[i].split('=')[0];
				if (objName) r[objName] = col[i].substr(objName.length + 1);
			}
		}
		return r;
	})();

	window.showRegvarNames = (window.queryString['regvarNames'] == 'true');

	try
	{
		if (parent != window) parent.SandboxFrame = window;	
	}
	catch (ex) { }
	
	/**
	* Ändrar rottabellen genom att radera angivna tabeller med tillhörande undertabeller. Raderar även värdedomäner
	* som inte hör till den nya rottabellen.
	*
	* @param {String} rootTableName Namn på den nya rottabellen.
	* @param {String[]} tablesToDelete Namn på tabeller som ska raderas. 
	* @param {String[]} [tablesToKeep=[rootTableName]] Namn på tabeller som ska exkluderas.
	*/
	Utils.changeRootTable = function (rootTableName, tablesToDelete, tablesToKeep) {
		if (!tablesToKeep) {
			tablesToKeep = [rootTableName];
		} else if ($.inArray(rootTableName, tablesToKeep) == -1) {
			tablesToKeep.push(rootTableName);
		}

		var toDelete = [];
		function getTableNames(rootTableName) {
			$.each(inca.form.metadata[rootTableName].subTables, function (i, o) {
				if ($.inArray(o, tablesToKeep) == -1) {
					getTableNames(inca.form.metadata[rootTableName].subTables[i]);
				}
			});

			if ($.inArray(rootTableName, toDelete) == -1) {
				toDelete.push(rootTableName);
			}

		}

		$.each(tablesToDelete, function (i, o) { getTableNames(o); });
		$.each(toDelete, function (i, o) { delete inca.form.metadata[o]; });

		$.each(tablesToKeep, function (toKeepIndex, toKeep) {
			var st_del = [];
			$.each(inca.form.metadata[toKeep].subTables, function (subTableIndex, subTable) {
				if ($.inArray(subTable, toDelete) != -1) {
					st_del.push(subTableIndex);
				}
			});
			$.each(st_del, function (i, o) {
				inca.form.metadata[toKeep].subTables.splice(o, 1);
			});
		});

		// Delete all vdlists that do not belong to the root table.
		$.each(inca.form.metadata, function ( tableName, tableMetadata ) {
			if ( tableName !== rootTableName ) {
				tableMetadata.vdlists = {};
			}
		});

		inca.form.data = inca.form.createDataRow(rootTableName);
	};

})(window['inca'], window);

(function() {

	if (queryString.birthDate) {
		var birthDateInt = parseInt(queryString.birthDate);
		if (!isNaN(birthDateInt)) {
			inca.form.env._FODELSEDATUM = RCC.Utils.ISO8601(new Date(birthDateInt));
		}
	}

	inca.user.role.isReviewer = queryString.reviewer && queryString.reviewer === 'true' ? true : false;
	
	inca.form.env._SEX = (queryString.gender == 'man' ? 'M' : 'F');
	inca.user.position.id = parseInt(queryString.formType);

	$(function () {
		$("body").prepend($('<button class="btn btn-primary" style="background-color: #330855">Skicka</button>').click(function ( ) { SendForm(); }));
	});
})();

var eventHandlers = {}, subTableConditions;

function getVal(regvar) {
    return regvar() ? regvar().value : undefined;
}

function setValue(obs, val) {
    obs(ko.utils.arrayFirst(obs.rcc.term.listValues, function (list) {
        return list.text === val || list.value === val;
    }));
}

function setCondition(condition, regvars) {
    _.each(regvars, function(regvar) {
        if(!condition) regvar(null);
        regvar.rcc.validation.required({ fatal: false, data: { canBeSaved: true } });
    });
    return condition;
}

function oneOf(regvar, arr) {
    try {
        return !!(regvar() && $.inArray(regvar().value, arr) >= 0);
    } catch(e) {
        console.error(e);
    }
}


(function() {

	var markAsFatal = RCC.Vast.Utils.markAsFatal,
		addNumMinMaxValidation = RCC.Vast.Utils.addNumMinMaxValidation,
		addYearValidation = RCC.Vast.Utils.addYearValidation,
		removeAllRows = RCC.Vast.Utils.removeAllRows,
		addRowIfEmpty = RCC.Vast.Utils.addRowIfEmpty;

    eventHandlers.rowAdded = {
		BehandlingUppfoljning: function(r) {

            r.showBeroendeValidation = ko.observable(false);

            if(inca.errand && inca.errand.status.val() == 'Nytt ärende') {
                var slutenvardId = [
                    1598,1644,1732,1734,1735,1749,1775,1776,1783,
                    1790,1955,1939,2120,2254,2266,2292,2362,2685,
                    2669,2738,2739,2733,2783,2912,2991,3011,3021
                ];
                var res = _.find(slutenvardId, function(unitId) { return unitId == inca.user.position.id; }); 
                setValue(r.formulartyp, res ? '2' : '1');
            }

            r.removeDiagnosRows = function(regvar, typ) {
                if(!regvar() || regvar().value != '1') {
                    $.each(r.$$.Diagnoser().slice(0), function (i, o) {
                        if(o.diagnostyp() && o.diagnostyp().value == typ)
                            r.$$.Diagnoser.rcc.remove(o);
                    });
                }
            }

            r.psykiatriskDiagnos.subscribe(function() { r.removeDiagnosRows(r.psykiatriskDiagnos, '2'); });
            r.somatiskDiagnos.subscribe(function() { r.removeDiagnosRows(r.somatiskDiagnos, '3'); }); 

            r.laroprogram.subscribe(function() { 
            if(r.laroprogram()&& r.laroprogram().value =='1')
                r.lakemedelVisible(true);  
            });


            r.lakemedelVisible = ko.observable(false);
            markAsFatal(r.formulartyp);
            markAsFatal(r.informationsdatum);
            markAsFatal(r.laroprogram);
        },
        Diagnoser: function(r, p) {
            r.diagnostyp.subscribe(function() {
                if(r.diagnostyp() && r.diagnostyp().value == '1')
                    p[0].showBeroendeValidation(false);
            });
        },
        Lakemedel: function(r) {
            r.beredningsform.rcc.validation.add(function() {
                var beredningsform = r.beredningsform();
                var lakemedel = r.lakemedel();
                if(!beredningsform || !lakemedel) return;
                if (lakemedel.value == 'N07BC02' && oneOf(r.beredningsform, ['1','2','4','7'])) { 
                    return { result: 'failed', message: 'Vald beredningsform är inte möjlig för läkemedlet', fatal: true };
                }
                else if (lakemedel.value == 'N07BC01' && oneOf(r.beredningsform, ['5','4','7'])) {
                    return { result: 'failed', message: 'Vald beredningsform är inte möjlig för läkemedlet', fatal: true };
                }
                else if (lakemedel.value == 'N07BC51' && oneOf(r.beredningsform, ['2','3','5','6','7'])) {
                    return { result: 'failed', message: 'Vald beredningsform är inte möjlig för läkemedlet', fatal: true };
                }
                else if (lakemedel.value == 'N07BC05' && oneOf(r.beredningsform, ['1','2','3','4','6','7'])) {
                    return { result: 'failed', message: 'Vald beredningsform är inte möjlig för läkemedlet', fatal: true };
                }
                else if (lakemedel.value == 'V03AB15' && oneOf(r.beredningsform, ['1','2','4','6'])) {
                    return { result: 'failed', message: 'Vald beredningsform är inte möjlig för läkemedlet', fatal: true };
                }
                else if (lakemedel.value == 'N07BB04' && oneOf(r.beredningsform, ['1','2','3','5','6','7'])) {
                    return { result: 'failed', message: 'Vald beredningsform är inte möjlig för läkemedlet', fatal: true };
                }
            });

            r.dos.subscribe(function() {
                if(r.dos()) {
                    var mvarDec = parseFloat(r.dos().replace(',', '.'));
                    if (!isNaN(mvarDec)) {
                        r.dos(mvarDec.toFixed(2).replace('.', ','));
                    }
                }
            });
        }
    };
})();

$(function () {
    var actions = { save: ['Spara'], abort: ['Avbryt', 'Radera'], pause: ['Pausa'] }; 
    try {
        var vm = new RCC.ViewModel({
            validation: new RCC.Validation(),
            events: eventHandlers
        });
        vm.showRegvarNames = ko.observable(false);
        vm.enableHelpText = ko.observable(true);
        vm.errors = ko.observableArray([]);
        var ready = ko.observable(false);
        vm.showForm = ko.computed(function () {return ready() && vm.errors().length == 0 }); 
        var SendForm = function () {
            var selectedAction = inca.errand && inca.errand.action.selectedText();
            if ($.inArray(selectedAction, actions.abort) >= 0) {
                return true;
            }
            var res = _.find(vm.$$.Diagnoser(), function(dia) { 
                return dia.diagnostyp() && dia.diagnostyp().value == '1'; 
            }); 
            if(!res) {
                vm.showBeroendeValidation(true);
                vm.$validation.markAllAsAccessed();
                return false;
            } 
            var errors = vm.$validation.errors();
            if (errors.length > 0) {
            vm.$validation.markAllAsAccessed();
                var isPause = ($.inArray(selectedAction, actions.pause) >= 0);
                var fatalErrors = $.grep(errors, function (error) { return (isPause && error.data && error.data.canBeSaved) ? false : error.fatal; });
                if (fatalErrors.length > 0) {
                    alert("Formuläret innehåller " + fatalErrors.length + " fel.");
                    return false;
                }
                return true;
            }
            return true;
        };

       try{
            vm.ERB_user = JSON.stringify(inca.user);
        } catch (e) {
            console.error(e);
        }
        vm.ERB_patientId = inca.form.env._PATIENTID;
        vm.ERB_register = 'Bättre Beroendevård';
        vm.ERB_form = inca.form.name;
        
        inca.on('validation', SendForm);
        window.SendForm = SendForm;
        ko.applyBindings(vm);
        ready(true);
        window.vm = vm;
     }   
     catch (ex) {
        inca.on("validation", function () {
            if (inca.errand && inca.errand.action && $.inArray(inca.errand.action.find ? inca.errand.action.find(':selected').text() : undefined, actions.abort) != -1) return false;
        });
        alert("Ett fel inträffade\r\n\r\n" + ex + "\r\n" + JSON.stringify(ex));
    }
});
