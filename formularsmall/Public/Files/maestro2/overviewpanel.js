(function () {
    ko.components.register('overviewpanel', {
        synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
        viewModel: function(params) {
            self.data = params.data;
            self.collapse = params.collapse;
            self.overviewData = ko.observable();

            self.HuvuddiagnosByValue = {
                1: 'Ovarial Cancer',
                2: 'Corpus Cancer',
                3: 'Cervix/Vagina Cancer',
                4: 'Vulva Cancer',
                5: 'Övriga Cancerformer'
            };

            self.TypAvKirurgi = {
                1: 'Primäroperation terapeutisk',
                2: 'Primäroperation diagnostisk',
                3: 'Stagingoperation/Restaging',
                4: 'Fördröjd primäroperation/intervalloperation',
                5: 'Operation vid progress/recidiv',
                99: 'Annan'
            };

            self.createOverviewObject = function() {
                var overviewObj = {};
                overviewObj.kirurgi = [];
                overviewObj.recidiv = [];

               if(inca.errand) {
                    _.each(params.kirurgi(), function(row) {
                        var kirObj = {
                            ki_opupp_kirurgidat: row.data.ki_opupp_kirurgidat || '',
                            ki_kir_typkirurgi: self.TypAvKirurgi[row.data.ki_kir_typkirurgi_Värde] || '',
                            komplikationsDatum: null
                        };
                        _.each(params.komplikation(), function(komp) { 
                            if(row.data.R2686T33116_ID == komp.data.R2686T33164_R2686T33116_ID) kirObj.komplikationsDatum = komp.data.ko_inr_inrappdat; 
                        }); 
                        overviewObj.kirurgi.push(kirObj);
                    });

                    _.each(params.recidiv(), function(row) {
                        overviewObj.recidiv.push({
                            r_rec_recidivprogdataktuellt: row.data.r_rec_recidivprogdataktuellt || '',
                            r_rec_ordningsnr: row.data.r_rec_ordningsnr || ''
                        })
                    });
                }
                else {
                    self.data.$$.Kirurgi().forEach(function (kir) {
                        var kirObj = {
                            ki_opupp_kirurgidat: kir.ki_opupp_kirurgidat() || '',
                            ki_kir_typkirurgi: kir.ki_kir_typkirurgi() && self.TypAvKirurgi[kir.ki_kir_typkirurgi().value] || '',
                            komplikationsDatum: null
                        };
                        if(kir.$$.Komplikation()[0]) kirObj.komplikationsDatum = kir.$$.Komplikation()[0].ko_inr_inrappdat();
                        overviewObj.kirurgi.push(kirObj);
                    });

                    self.data.$$.Recidiv().forEach(function (rec) {
                        overviewObj.recidiv.push({
                            r_rec_recidivprogdataktuellt: rec.r_rec_recidivprogdataktuellt() || '',
                            r_rec_ordningsnr: rec.r_rec_ordningsnr() || ''
                        });
                    });
                }


                overviewObj.anmalanDatum = self.data.huvuddiagnos() && self.data.huvuddiagnos().value <= '4' ?
                    self.data.a_inr_provdat_padcyt() || '' :
                    self.data.a_inr_diagdat() || '';
                overviewObj.icd10 = (self.data.a_diag_icd10() && self.data.a_diag_icd10().text) || '';
                overviewObj.l_inr_inrappdat = self.data.l_inr_inrappdat() || '';
                overviewObj.ts_inr_inrappdat = self.data.ts_inr_inrappdat() || '';
                overviewObj.ep_inr_inrappdat = self.data.ep_inr_inrappdat() || '';
                overviewObj.ep_beh_primbehavslutaddatum = self.data.ep_beh_primbehavslutaddatum() || '';
                overviewObj.u_uppf_datum = self.data.u_uppf_datum() || '';
                overviewObj.recidivprogdatforsta = self.data.recidivprogdatforsta() || '';
                overviewObj.antalRecidiv = inca.errand ? params.recidiv().length : self.data.$$.Recidiv().length;
                self.overviewData(overviewObj);
         
            };

            self.getHuvudDiagnosByName = function() {
                if(data.huvuddiagnos()) {
                    return self.HuvuddiagnosByValue[data.huvuddiagnos().value];
                }
            };

            self.createOverviewObject(); 
        },
        template: RCC.Vast.Utils.extractSource(function() {/**@preserve
        <!-- ko if: overviewData().anmalanDatum -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Översikt
                        <span data-bind="click: function() { collapse(collapse() ? false : true) }" class="icon-right glyphicon glyphicon-chevron-down"></span>
                    </h3>
                </div>
                <div class="panel-body" data-bind="visible: collapse">
                    <table id="oversiktsTable" class="table table-striped table-hover table-responsive">
                        <caption><h4 style="font-weight: 600;" data-bind="text: getHuvudDiagnosByName()"></h4></caption>
                        <tr>
                            <th class="col-sm-1">Anmälan:<br />(Diagnosdatum)</th>
                            <td class="col-sm-2" data-bind="text: overviewData().anmalanDatum"></td>
                            <td class="col-sm-3"data-bind="text: overviewData().icd10"></td>
                            <td class="col-sm-6"></td>
                        </tr>
                        <!-- ko foreach: overviewData().kirurgi-->
                            <tr>
                                <th class="col-sm-1">Kirurgi:<br />(Datum för kirurgi)</th>
                                <td class="col-sm-2" data-bind="text: $data.ki_opupp_kirurgidat"></td>
                                <td class="col-sm-2" data-bind="text: $data.ki_kir_typkirurgi"></td>
                                <td>
                                    <div data-bind="visible: $data.komplikationsDatum, text: 'Komplikation:'"></div>
                                    <div data-bind="text: $data.komplikationsDatum"></div>
                                </td>
                            </tr>
                        <!--/ko-->
                        <!-- ko if: !!overviewData().l_inr_inrappdat-->
                            <tr>
                                <th class="col-sm-1">Ledtider:<br />(Inrapporteringsdatum)</th>
                                <td class="col-sm-2" data-bind="text: overviewData().l_inr_inrappdat"></td>
                                <td class="col-sm-4"></td>
                                <td class="col-sm-5"></td>
                            </tr>
                        <!--/ko-->
                        <!-- ko if: !!overviewData().ts_inr_inrappdat-->
                            <tr>
                                <th class="col-sm-3">Tumör och sjukdomsdata:<br />(Inrapporteringsdatum)</th>
                                <td class="col-sm-2" data-bind="text: overviewData().ts_inr_inrappdat"></td>
                                <td class="col-sm-4"></td>
                                <td class="col-sm-3"></td>
                            </tr>
                        <!--/ko-->
                        <!-- ko if: !!overviewData().ep_inr_inrappdat-->
                            <tr>
                                <th class="col-sm-3">Efter primärbehandling:<br />(Inrapporteringsdatum)</th>
                                <td class="col-sm-2" data-bind="text: overviewData().ep_inr_inrappdat"></td>
                                <td class="col-sm-4"></td>
                                <td class="col-sm-2"></td>
                            </tr>
                        <!--/ko-->
                        <!-- ko if: !!overviewData().ep_beh_primbehavslutaddatum-->
                            <tr>
                                 <th class="col-sm-3">Avslutad primärbehandling:<br />(Datum för avslutad primärbehandling)</th>
                                 <td class="col-sm-2" data-bind="text: overviewData().ep_beh_primbehavslutaddatum"></td>
                                 <td class="col-sm-4"></td>
                                 <td class="col-sm-2"></td>
                            </tr>
                        <!--/ko-->
                        <!-- ko if: !!overviewData().u_uppf_datum-->
                            <tr>
                                <th class="col-sm-2">Uppföljning:<br />(Datum för senaste uppföljning)</th>
                                <td class="col-sm-2" data-bind="text: overviewData().u_uppf_datum"></td>
                                <td class="col-sm-4"></td>
                                <td class="col-sm-4"></td>
                            </tr>
                        <!--/ko-->
                        <!-- ko if: !!overviewData().recidivprogdatforsta-->
                            <tr>
                                <th class="col-sm-3">Datum för första recidiv:</th>
                                <td class="col-sm-2" data-bind="text: overviewData().recidivprogdatforsta"></td>
                                <td class="col-sm-3"></td>
                                <td class="col-sm-6"></td>
                            </tr>
                        <!--/ko--> 
                        <!-- ko if: overviewData().recidiv.length > 0 -->   
                            <tr>
                                <th class="col-sm-1">Antal recidiv:</th>
                                <td class="col-sm-2" data-bind="text: overviewData().antalRecidiv"></td>
                                <td class="col-sm-3"></td>
                                <td class="col-sm-6"></td>
                            </tr>
                             <!-- ko foreach: overviewData().recidiv-->
                                 <tr>
                                     <th class="col-sm-1">
                                         <div data-bind="text: 'Recidiv ' + $data.r_rec_ordningsnr"></div>
                                         <div>(Datum för aktuellt recidiv)</div>
                                     </th>
                                     <td class="col-sm-2" data-bind="text: $data.r_rec_recidivprogdataktuellt"></td>
                                     <td class="col-sm-3"></td>
                                     <td class="col-sm-6"></td>
                                 </tr>
                             <!--/ko-->
                        <!--/ko-->
                        <!-- ko if: !!inca.form.env._DATEOFDEATH-->
                            <tr>
                                <th class="col-sm-1">Dödsdatum:</th>
                                <td class="col-sm-2" data-bind="text: inca.form.env._DATEOFDEATH"></td>
                                <td class="col-sm-4"></td>
                                <td class="col-sm-5"></td>
                            </tr>
                        <!--/ko-->
                    </table>
                </div>
            </div>
        <!--/ko-->  
        */return undefined;})
    });
})();
