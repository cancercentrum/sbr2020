/**
*/

(function(inca, RCC, ko, _) {
	ko.components.register('icd10-picker', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
            var self = this;
            self.icd10Visible = ko.observable(false);
            self.table = params.table;
            self.icd10Typ = params.icd10Typ;
            self.showValidation = params.showValidation || null;

            self.icd10filter = function(o) {
                return o.data.kod.match(/^F1/) || o.data.kod == "F630";
            };

            self.icd10Picker = new ICD10.Picker({
                defaultChapter: self.icd10Typ == '3' ? null : 'Psykiska sjukdomar och syndrom samt beteendestörningar',
                addCode: function(code) {
                    var row = self.table.rcc.add();
                    row.diagnosVD(code.id);
                    row.diagnostyp(ko.utils.arrayFirst(row.diagnostyp.rcc.term.listValues, function (list) {
                        return list.value === self.icd10Typ;
                    }));
                },
               manualFilter: function(objs) {
                    var res = null;
                    if(self.icd10Typ == '1')
                        res = _.filter(objs, self.icd10filter);
                    else if(self.icd10Typ == '2')
                        res = _.reject(objs, self.icd10filter);
                    else 
                        res = objs;
                    return res;
                }
            });
       },
           template: RCC.Vast.Utils.extractSource(function() {/**@preserve
          <div data-bind="visible: icd10Visible()" class="rcc-modal" tabindex="-1">
            <div class="rcc-modal-overlay"></div>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button data-bind="click: function() { icd10Visible(false); }" class="close">&times;</button>
                        <h4 class="modal-title">Välj diagnos</h4>
                    </div>
                    <div class="modal-body">
                    <!-- ko if: icd10Picker.errors() && icd10Picker.errors().length > 0 -->
                        <h1>Tekniskt fel</h1>
                        <ul class="icd10-errors" data-bind="foreach: icd10Picker.errors">
                            <li data-bind="text: $data"></li>
                        </ul>
                    <!-- /ko -->
                    <!-- ko ifnot: icd10Picker.chapters -->
                        <em>Laddar kapitel...</em>
                    <!-- /ko -->
                    <!-- ko if: icd10Picker.chapters -->
                        <select class="form-control" data-bind="
                            value: icd10Picker.chapter,
                            options: icd10Picker.chapters,
                            optionsText: function ( chapter ) { return chapter.data.kapitel + ' (' + chapter.data.forstakod + '–' + chapter.data.sistakod + ')'; },
                            optionsCaption: '– Välj kapitel –'">
                        </select>
                        <form>
                            <input type="search" style="margin: 10px 0;" class="form-control" placeholder="Sök" data-bind="value: icd10Picker.query, valueUpdate: 'afterkeydown', visible:icd10Picker.chapter" />
                        </form>
                            <!-- ko if: icd10Picker.chapter -->
                                <!-- ko if: icd10Picker.filteredCodes -->
                                    <table class="table table-hover table-condensed">
                                        <thead>
                                            <tr>
                                                <th>Kod</th>
                                                <th>Beskrivning</th>
                                                <th>Åtgärd</th>
                                            </tr>
                                        </thead>
                                        <tbody data-bind="foreach: icd10Picker.filteredCodes().slice( 0, icd10Picker.maxCodes() )">
                                            <tr>
                                                <td data-bind="text: data.kod"></td>
                                                <td data-bind="text: data.beskrivning"></td>
                                                <td><button class="btn btn-primary btn-xs" style="background-color: #330855" data-bind="click: $parent.icd10Picker.addCode">Välj</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <button class="btn btn-default" data-bind="enable: icd10Picker.filteredCodes().length > icd10Picker.maxCodes(), click: function ( ) { icd10Picker.maxCodes( icd10Picker.maxCodes() + 30 ); }">Visa fler</button> 
                                <!-- /ko -->
                                <!-- ko ifnot: icd10Picker.filteredCodes -->
                                    <em>Laddar koder...</em>
                                <!-- /ko -->
                            <!-- /ko -->
                    <!-- /ko -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-bind="click: function() { icd10Visible(false); }">Stäng</button>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-hover table-condensed" style="margin-bottom: 0px">
            <thead>
                <tr>
                    <th>Vald diagnos</th>
                    <th>Åtgärd</th>
                </tr>
            </thead>
            <tbody data-bind="foreach: table">
                <tr>
                    <!-- ko if: diagnostyp() && diagnostyp().value == $parent.icd10Typ -->
                        <td data-bind="text: ICD10.informationForId(diagnosVD()).text"></td>
                        <td><button class="btn btn-primary btn-xs btn-danger" data-bind="enable: !$root.$form.isReadOnly, click: $parent.table.rcc.remove">Ta bort</button></td>
                    <!--/ko-->
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td>
                        <em data-bind="visible: table().length == 0">Inga valda diagnoser.</em>
                    </td>
                    <td>
                        <button data-bind="enable:!$root.$form.isReadOnly, click: function() { icd10Visible(true); }" class="btn btn-primary btn-xs" style="background-color: #330855">
                            <span>Lägg till</span>
                        </button>
                    </td>
                </tr>
            </tfoot>
        </table>
        <!-- ko if: showValidation && showValidation() -->
            <span style="display: inline-block; margin-bottom: 10px">
                <span class="fa fa-exclamation-circle" style="color: red; font-size: 16px; margin-left: 400px"></span>
                <span class="help" data-bind="text: 'Fyll i beroendediagnos'"></span>
            </span>
        <!-- /ko-->
       */return undefined;})
	});
})(window['inca'], window['RCC'], window['ko'], window['_']);
