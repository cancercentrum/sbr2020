/**
* Hanterar frågor med flersvars alternativ.
* Användning:
*	<m-a params="@param: värde"></m-a>
*
* @param {String} table Namn på tabell för frågesvar.
* @param {Number} [minRows] Minsta tillåtna antal rader i tabell.
* @param {Number} [maxRows] Största tillåtna antal rader i tabell.
* @param {String} [question] Frågetext.
* @param {String} [info] Frågans hjälptext.
* @param {Object[]|String[]} [controls] Listor eller textfält för datakoppling. Kan anges med sträng där endast 'name'-egenskapen behövs.
* @param {String} [controls[].name] Namn på ko.observable som används som datakoppling på kontroll.
* @param {String|Object} [controls[].type] Typ av kontroll, 'select', 'text', 'datepicker', 'decimal', 'textarea' eller 'checkbox'. Sätts automatiskt från datakopplingens datatyp om ej parameter anges.
* @param {String} [controls[].type.name] Typ av kontroll.
* @param {Object} [controls[].type.options] Argument för datakoppling. Använder standardvärden för de argument som inte anges.
* @param {String} [controls[].question] Frågetext för kontroll.
* @param {String} [controls[].info] Hjälptext för kontroll.
* @param {String} [controls[].placeholder] Text som visas efter kontroll.
* @param {function(Object, Object[])} [controls[].optionsText] optionsText för select-kontroll. Listobjekt och $parents finns tillgängliga som argument.
* @param {Boolean|function(ko.observable)} [showLegendIfEmpty=true] Hanterar visning av frågetext, hjälptext och lägg till-knapp. $parent finns tillgänglig som argument.
* @param {ko.component|ko.component[]|Object|Object[]} [innerContent] Innehåll som skapas i varje underrad.
* @param {String} [innerContent.type='component'] Typ av innehåll, 'component' eller 'template'.
* @param {ko.component|ko.template} [innerContent.object] Objekt för koppling med ko.component eller ko.template.
* @param {Boolean} [innerContent.margin=true] Sätter en vänstermarginal på innerContent.
*/
(function(RCC, ko, _) {
	ko.components.register('m-a', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
			if (!params.table) throw new Error('Parameter "table" is required.');

			_.defaults(params, {
				question: '',
				info: '',
				minRows: 0,
				maxRows: Infinity,
				controls: [],
				showLegendIfEmpty: true
			});

			self.extendRoot = function($parent) {
				return new function() {
					var self = this;
					_.extend(self, _.pick(params, 'question', 'info', 'minRows', 'maxRows'));
					
					self.innerContent = (function() {
						var c = params.innerContent || [];
						return _.map(_.isArray(c) ? c : [c], function(x) {
							return _.defaults(x.object ? x : { object: x }, { type: 'component', margin: true });
						});
					})();

					self.tables = $parent.$$[params.table];
					self.showLegend = function() {
				 		if (self.tables().length > 0) return true;
						return (_.isBoolean(params.showLegendIfEmpty) ? params.showLegendIfEmpty : params.showLegendIfEmpty($parent));
					};

					self.controls = _.map(params.controls, function(control) {
						if (_.isString(control)) control = { name: control };
						var r = _.defaults(_.pick(control, 'question', 'info', 'placeholder', 'optionsText'), {
							question: '', info: '', placeholder: '', optionsText: 'text'
						});

						r.extendControl = function($parents) {
							return new function() {
								var x = this;
								x.value = $parents[0][control.name];
								x.type = control.type || { datetime: 'datepicker', list: 'select', boolean: 'checkbox' }[x.value.rcc.term.dataType] || 'text';
								x.options = _.defaults(control.options || {}, (function() {
									switch (control.type) {
										case 'datepicker': return { value: x.value, dateToday: inca.serverDate, readOnly: inca.form.isReadOnly };
										case 'decimal': return { valueObservable: x.value };
										case 'staticText':
											if( x.value() && typeof x.value() == 'object' && x.value().text ){
												return { value: x.value().text };
											}
											return { value: x.value };
										default: return { value: x.value };
									}
								})());

								if (_.isFunction(r.optionsText)) x.optionsText = function(listObject) {
									return control.optionsText(listObject, $parents);
								}
							};
						};
						return r;
					});
				}
			};
		},
		template: RCC.Vast.Utils.extractSource(function() {/**@preserve
			<div class="multi-answer-template" data-bind="with: _.extend($data, extendRoot($parent))">
				<!-- ko if: question && showLegend() -->
					<div class="r">
						<div class="b" data-bind="text: question"></div>
						<!-- ko if: info -->
							<span class="help" data-bind="html: info"></span>
						<!--/ko-->
					</div>
				<!--/ko-->
				<div data-bind="foreach: tables">
					<div data-bind="css: { 'multi-item': $parent.controls.length != 0 && $parent.maxRows != 1, 'single-item': $parent.controls.length == 7}">
						<div class="s-inl" data-bind="foreach: $parent.controls">
							<div class="s-inl" data-bind="with: _.extend($data, extendControl($parents))">
								<s-a params="value: value, question: question || value.rcc.regvar.label, ma: true"></s-a>
							</div>
						</div>
						<!-- ko foreach: $parent.innerContent -->
							<div class="lvl lvl-mark" data-bind="css: { 'no-inner-template-margin': !margin }">
								<!-- ko with: $parent -->
									<!-- ko if: $parent.type == 'component' -->
										<!--ko component: $parent.object --><!--/ko-->
									<!--/ko-->
									<!-- ko if: $parent.type == 'template' -->
										<!--ko template: $parent.object --><!--/ko-->
									<!--/ko-->
								<!--/ko-->
							</div>
						<!--/ko-->
						<!-- ko if: $parent.tables().length > $parent.minRows -->
							<div class="r"></div>
							<button class="btn btn-danger btn-xs" data-bind="enable: !$root.$form.isReadOnly, click: $parent.tables.rcc.remove">Ta bort</button>
						<!--/ko-->
					</div>
				</div>
				<div class="r" data-bind="visible: $parent.tables().length < maxRows && showLegend()">
					<button class="btn btn-primary btn-xs" style="background-color: #330855" data-bind="enable: !$root.$form.isReadOnly, click: function() { $parent.tables.rcc.add(); }">Lägg till</button>
				</div>
			</div>
		*/return undefined;})
	});
})(window['RCC'], window['ko'], window['_']);
