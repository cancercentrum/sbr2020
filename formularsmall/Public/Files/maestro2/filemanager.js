/**
* FileManager
* Användning:
*	<file-manager params="@param: värde"></file-manager>
*
* @param {ko.observable} value Datakoppling för fil.
* @param {Boolean} [allowRename=true] Tillåter ändring av filnamn.
*/
(function(RCC, ko, _) {
	ko.components.register('file-manager', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
			if (!params.value) throw new Error('Parameter "value" is required.');

			var self = this;
			self.allowRename = params.allowRename;
			_.defaults(self, { allowRename: true });
			self.isUploading = ko.observable(false);
			self.file = new function() {
				var m = this;
				_.each(['filename', 'data', 'url'], function(propName) {
					m[propName] = ko.computed({
						read: function () {
							var v = params.value();
							if (v) return v[propName];
						},
						write: function (value) {
							var v = params.value() || {};
							v[propName] = value;
							params.value(v);
						}
					})
				});
			};

			self.toggleEditMode = function() {
				self.editMode(!self.editMode());
			};

			self.setFile = function(fileObject) {
				self.isUploading(true);
				self.editMode(false);
				var reader = new FileReader();
				reader.onload = function() {
					self.file.filename(fileObject.name);
					self.file.url(undefined);
					self.file.data((function(data){
						var blocksize = 512, blocks = [];
						for(var i = 0; i < data.length; i += blocksize)
							blocks.push(String.fromCharCode.apply(String, data.subarray(i, i + blocksize)));
						return btoa(blocks.join(''));
					})(new Uint8Array(reader.result)));
					self.isUploading(false);
				};

				reader.readAsArrayBuffer(fileObject);
			};

			self.editMode = ko.observable(false);

			self.compatibility = {
				FileAPI: !!window.FileReader
			};

			self.dragHandler = new function() {
				this.event = {};
				if (self.compatibility.FileAPI) {
					var enterCount = ko.observable(0);
					this.active = ko.computed(function() {
						return enterCount() > 0; 
					});

					this.event.dragenter = function() {
						enterCount(enterCount() + 1);
					};

					this.event.dragover = function() {
						return false;
					};

					this.event.dragleave = function() {
						enterCount(enterCount() - 1);
					};

					this.event.drop = function(a, e) {
						var files = e.originalEvent.dataTransfer.files;
						if (files.length > 0) self.setFile(files[0]);
						enterCount(0);
					};
				}
			};

			if (self.compatibility.FileAPI) {
				self.fileUploaderEvent = {
					change: function(a, e) {
						var files = e.currentTarget.files;
						if (files.length > 0) self.setFile(files[0]);
					}
				};
			}
		},
		template: RCC.Vast.Utils.extractSource(function() {/**@preserve
			<div class="m-file-manager" data-bind="event: dragHandler.event, css: { 'drag': dragHandler.active }">
				<!-- ko ifnot: file.isUploading -->
					<div class="pull-left">				
						<!-- ko if: editMode -->
							<input type="text" class="ctrl" data-bind="value: file.filename" />
						<!-- /ko -->
						<!-- ko ifnot: editMode -->
							<!-- ko if: file.url -->
								<a target="_blank" data-bind="text: file.filename, attr: { 'href': file.url }"></a>
							<!-- /ko -->
							<!-- ko if: file.data -->
								<span data-bind="text: file.filename"></span>
							<!-- /ko -->
						<!-- /ko -->
						<!-- ko if: allowRename && (file.url() || file.data()) -->
							<button type="button" class="btn btn-default btn-xs toggle-edit" title="Ändra namn" data-bind="click: toggleEditMode">
								<span class="glyphicon" data-bind="css: { 'glyphicon-pencil': !editMode(), 'glyphicon-ok': editMode }"></span>
							</button>
						<!-- /ko -->
						<!-- ko if: !file.url() && !file.data() -->
							<em>Fil saknas.</em>
						<!-- /ko -->
					</div>
					<div class="pull-right">
						<!-- ko if: compatibility.FileAPI -->
							<input type="file" data-bind="event: fileUploaderEvent" />
						<!-- /ko -->
						<!-- ko ifnot: compatibility.FileAPI -->
							<div class="bg-warning no-support">Webbläsaren du använder saknar stöd för filuppladdning.</div>
						<!-- /ko -->
					</div>
					<div class="clearfix"></div>
				<!-- /ko -->
			</div>
		*/return undefined;})
	});
})(window['RCC'], window['ko'], window['_']);
