/**
*/

(function(inca, RCC, ko, _) {
	ko.components.register('s-a-b', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
    			if (!params.value && params.controlType != 'staticText') throw new Error('Parameter "value" is required.');
    			var self = this;
    			self.regvar = params.value;
    			self.question = (params.value && params.value.rcc && params.value.rcc.regvar && params.value.rcc.regvar.label) ?
                    params.value.rcc.regvar.label : params.question;
    			self.info = params.info;
    			self.lastSelectedValue = ko.observable(self.regvar() && self.regvar().value);
    			self.tooltip = params.help || params.value.rcc.regvar.description;
    			self.fullWidthText = params.fullWidthText || false;
          self.info = params.info;
          self.indrag = params.indrag || false;

          
          if(!params.data) {
              self.data = [
                  { id: 'nej', text: 'Nej', value: 0 },
                  { id: 'ja', text: 'Ja', value: 1 },
                  { id: 'uppgift_saknas', text: 'Uppgift saknas', value: 96 },
              ];
          }
          else
              self.data = params.data;

          self.setRegvar = function(obj) {
              if(!inca.form.isReadOnly) {
                  if(self.regvar() && self.lastSelectedValue() == obj.value)
                      self.regvar(null);
                  else {
                      var res = _.find(self.data, function(item) { return obj.id == item.id; });
                      self.regvar(ko.utils.arrayFirst(self.regvar.rcc.term.listValues, function (list) {
                          return list.value == res.value;
                      }));
                  }
                  self.regvar.rcc.accessed(true);
                  self.lastSelectedValue(self.regvar() && self.regvar().value);
              } 
           };

           self.handleKeyEvent = function(data, event) {
               if(event.keyCode === 32 || event.which === 32 ||
                   event.keyCode === 13 || event.which === 13) {
                   self.setRegvar(data);
               }
           };

           self.handleBlurEvent = function() {
               self.regvar.rcc.accessed(true);
           };


           self.helpPopover = ko.pureComputed(function() {
               var regvar = params.value && params.value.rcc && params.value.rcc.regvar;
               var helpText = regvar && regvar.helpText ?
                   regvar.helpText : self.tooltip;
               return helpText ? {
                       title: undefined,
                       content:  helpText,
                       trigger: 'hover',
                       html: true
                   } : null;
           });

           if (self.showReviewerInclude)
               self.showReviewerInclude = !!(inca.user.role.isReviewer && !inca.form.isReadOnly && self.regvar.rcc && self.enabled);

           self.hasErrors = ko.computed(function () {
               var errors = params.value && params.value.rcc && params.value.rcc.validation && params.value.rcc.validation.errors();
               return errors && errors.length;
           });

           self.hasFatalErrors = ko.computed(function () {
               var errors = params.value && params.value.rcc && params.value.rcc.validation && params.value.rcc.validation.errors();
               return !!(errors && errors.filter(function (e) { return e.fatal; }).length);
           });
       },
       template: RCC.Vast.Utils.extractSource(function() {/**@preserve
           <div class="single-answer-template r" data-bind="attr: { title: tooltip }, style: { 'margin-left': indrag ? '20px' : '0px' }">
               <label data-bind="css: { 'accessed': regvar.rcc && regvar.rcc.accessed(), 'errors': hasErrors, 'fatal': hasFatalErrors }">
                   <span data-bind="popover: helpPopover, html: question + (helpPopover() ? ' <i class=\'fa fa-info-circle\'></i>' : '') + (info ? '<br /><span class=\'help\'>' + info + '</span>' : ''), css: { 'full-width-text': fullWidthText }" class="question"></span>
                   <!-- ko if: ko.utils.unwrapObservable($root.showRegvarNames) && regvar.rcc -->
                       <span class="regvar-name" data-bind="text: regvar.rcc.regvarName"></span>
                   <!-- /ko -->
                   <div class="btn-group" data-bind="foreach: data, css: { 's-a-b-fatal': regvar.rcc && regvar.rcc.accessed() && hasFatalErrors() }">   
                        <label
                            class="btn btn-default"
                            tabindex="0"
                            data-bind="click: $parent.setRegvar, css: {'buttondisabled': inca.form.isReadOnly, 'buttondisabled-selected': $parent.regvar() && $parent.regvar().value == value, 'btn-primary' : $parent.regvar() && $parent.regvar().value == value }, attr: { for: $parent.regvar.rcc.regvarName + text + $index() }, text: text, event: { keypress: $parent.handleKeyEvent, blur: $parent.handleBlurEvent }"
                        ></label>
                   </div>
                   <!-- ko if: hasErrors -->
                       <div class="error-list" data-bind="foreach: regvar.rcc.validation.errors" >
                           <span class="error-item" data-bind="css: { 'non-fatal': !fatal, 'fatal': fatal }">
                               <span class="fa" data-bind="css: { 'fa-exclamation-triangle': !fatal, 'fa-exclamation-circle': fatal }"></span>
                               <span data-bind="text: message"></span>
                           </span>
                       </div>
                   <!--/ko-->
               </label>
           </div>
       */return undefined;})
	});
})(window['inca'], window['RCC'], window['ko'], window['_']);
