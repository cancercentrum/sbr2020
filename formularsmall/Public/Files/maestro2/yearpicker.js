/**

*/

(function () {
    ko.components.register('year-picker', {
        synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
        viewModel: function(params) {
            var self = this;
            self.yearsBack = params.yearsBack;
            self.hideShow = ko.observable();
            self.value = params.value;
            self.label = params.label;
            self.years = [];

            self.countYears = function(){
                var currentYear = new Date().getFullYear();
                var i= (currentYear - self.yearsBack);

                for(i; i<currentYear; currentYear--) 
                    self.years.push(currentYear);
            };

            self.setYear = function(data) { 
                params.value(data.toString()); 
            };

            self.hideShowList = function(data) { 
                self.hideShow(self.hideShow() ? false : true);
            };

            self.value.subscribe(function() { self.hideShow(false); });

            self.hasErrors = ko.computed(function () {
                var errors = self.value && self.value.rcc && self.value.rcc.validation && self.value.rcc.validation.errors();
                return errors && errors.length;
            });

            self.hasFatalErrors = ko.computed(function () {
                var errors = self.value && self.value.rcc && self.value.rcc.validation && self.value.rcc.validation.errors();
                return !!(errors && errors.filter(function (e) { return e.fatal; }).length);
            });

            self.countYears(self.years);
        },
        template: RCC.Vast.Utils.extractSource(function() {/**@preserve
            <div class="single-answer-template r">
                <label data-bind="css: { 'accessed': value.rcc && value.rcc.accessed(), 'errors': hasErrors, 'fatal': hasFatalErrors }">
                    <span class="question" data-bind="text: label"></span>
                    <div class="year-picker-list">
                        <input class="form-control" type="text" data-bind="enable: value.rcc.enabled, rcc-var: value, value: value, event: { click: hideShowList }"/>
                        <div class="list-group listmenu" data-bind="foreach: years, visible: hideShow">
                            <a href="#" class="list-group-item" data-bind="text: $data, click: $parent.setYear"></a>
                        </div> 
                    </div> 
                    <!-- ko if: hasErrors -->
                        <div class="error-list" data-bind="foreach: value.rcc.validation.errors" >
                            <span class="error-item" data-bind="css: { 'non-fatal': !fatal, 'fatal': fatal }">
                                <span class="fa" data-bind="css: { 'fa-exclamation-triangle': !fatal, 'fa-exclamation-circle': fatal }"></span>
                                <span data-bind="text: message"></span>
                            </span>
                        </div>
                    <!--/ko-->
                </label>
            </div>    
        */return undefined;})
    });
})();
