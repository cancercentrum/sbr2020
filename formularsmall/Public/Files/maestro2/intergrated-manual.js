(function(inca, RCC, ko, _) {
	ko.components.register('intergrated-manual', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
			var self = this;
			self.title = params.title;
			self.label = params.label;
			self.header = params.header;
			self.table = params.table;
			self.visible = ko.observable(false);
			self.registrering = params.registrering;
			self.filterValues = ['N07BC01', 'N07BC51', 'N07BC02', 'N07BB04', 'V03AB15', 'N07BB03', 'N07BB01', 'N07BB05', 'N07BC05', 'N06BA09', 'N06AX12', 'N06BA02', 'N06BA12', 'N06BA04',   'N05CD08', 'N03AE01', 'N05BA01',  'N05BA04',  'N05BA06', 'N05BA12', 'N05CM02', 'N05CF02', 'N05CF01', 'N03AX12', 'N03AF01', 'N03AX16', 'N03AX11', 'A11DB', 'N07BA01', 'N07BA03', 'C02AC01', 'N02AA05' ];
            self.lakemedelArr = [
                { header: 'Opioidberoende', data: [] },
                { header: 'Alkohol', data: [] },
                { header: 'ADHD', data: [] },
                { header: 'Bensodiazepiner', data: [] },
                { header: 'Antiepileptika', data: [] },
                { header: 'Vitamin B1', data: [] },
                { header: 'Nikotin', data: [] },
				{ header: 'Opioidberoende, övriga läkemedel', data: [] },
            ];

            _.each(self.filterValues, function(code) {
            	var lakemedelListValues = inca.offline ? inca.lakemedelListValue  : inca.form.metadata.Lakemedel.regvars.lakemedel.listValues;
				var obj =  _.find(lakemedelListValues, function(listvalue) { 
					return code == listvalue.value; 
				});
				if(obj.value == 'N07BB04')
					self.lakemedelArr[0].data.push(obj);
                if(obj.value.substring(0,5) == 'N07BC' || obj.value.substring(0,5) == 'V03AB')
                    self.lakemedelArr[0].data.push(obj);
                else if(obj.value.substring(0,5) == 'N07BB')
                    self.lakemedelArr[1].data.push(obj);
                else if(obj.value.substring(0,3) == 'N06')
                    self.lakemedelArr[2].data.push(obj);
                else if(obj.value.substring(0,3) == 'N05' || obj.value.substring(0,5) == 'N03AE')
                    self.lakemedelArr[3].data.push(obj);
                else if(obj.value.substring(0,3) == 'N03')
                    self.lakemedelArr[4].data.push(obj);
                else if(obj.value.substring(0,3) == 'A11')
                    self.lakemedelArr[5].data.push(obj);
                else if(obj.value.substring(0,5) == 'N07BA')
                    self.lakemedelArr[6].data.push(obj);
				else if(obj.value == 'C02AC01' || obj.value == 'N02AA05') 
                    self.lakemedelArr[7].data.push(obj);
            });

			self.currentData = ko.observableArray(self.lakemedelArr[0].data);
	

			self.addLakemedel = function(obj) {
				var row = self.table.rcc.add();
				row.lakemedel(ko.utils.arrayFirst(row.lakemedel.rcc.term.listValues, function (list) { 
					return list.value == obj.value;
				}));
			};

			self.setPage = function(obj) {
				self.currentData([]);
				_.each(self.lakemedelArr, function(lak) {
					if(lak.header == obj.header)
						self.currentData(lak.data);
				});
			};

			self.removeValue = function(obj) {
				self.regvar(null);
				self.visible(false);
				self.regvar.rcc.accessed(true);
			};

			self.hasErrors = ko.computed(function () {
                var errors = self.regvar && self.regvar.rcc && self.regvar.rcc.validation && self.regvar.rcc.validation.errors();
                return errors && errors.length;
            });

            self.hasFatalErrors = ko.computed(function () {
                var errors = self.regvar && self.regvar.rcc && self.regvar.rcc.validation && self.regvar.rcc.validation.errors();
                return !!(errors && errors.filter(function (e) { return e.fatal; }).length);
            });

            self.helpPopover = ko.pureComputed(function() {
            	var regvar = params.value && params.value.rcc && params.value.rcc.regvar;
            	var helpText = regvar && regvar.helpText ?
					regvar.helpText : params.help ? params.help :
						regvar && regvar.description ?  regvar.description : '';

                return helpText ? {
                        title: undefined,
                        content:  helpText,
                        trigger: 'hover',
                        html: true
                    } : null;
            });

		},

		template: RCC.Vast.Utils.extractSource(function() {/**@preserve
			<!-- ko if: visible() -->
			<div class="rcc-modal" tabindex="-1">
	            <div class="modal-dialog" style="width: 700px; margin-left: 40px">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <button data-bind="click: function() { visible(false); }" class="close">&times;</button>
	                        <h2 class="modal-title" data-bind="text: title"></h2>
	                        <ul class="pagination" style="margin-bottom: 0px">
							  <!-- ko foreach: lakemedelArr --> 
								  <li class="page-item"><a class="page-link" style="background-color: #330855; color: #FFFFFF" href="#" data-bind="text: header, click: $parent.setPage"></a></li>
							  <!--/ko-->
							</ul>
	                    </div>
	                    <div class="modal-body">
	                        <h4 style="color: red; margin-top: 0px" data-bind="text: header"></h4>
							<table class="table table-hover table-condensed" data-bind="visible: currentData().length > 0">
	                            <thead>
	                                <tr>
	                                    <th style="width: 10%">Kod</th>
	                                    <th style="width: 80%">Beskrivning</th>
	                                    <th style="width: 10%">Åtgärd</th>
	                                </tr>
	                            </thead>
	                            <tbody data-bind="foreach: currentData">
	                                <tr>
	                                    <td data-bind="html: '<b>' + value + '</b>'"></td>
	                                    <td data-bind="html: text, attr: { 'colspan': 1 }"></td>
	                                    <td>
	                                        <button class="btn btn-primary btn-xs" style="background-color: #330855" data-bind="enable: !$root.$form.isReadOnly, click: $parent.addLakemedel">Välj</button>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                     <div class="modal-footer">
                        	<button type="button" class="btn btn-default" data-bind="click: function() { visible(false); }">Stäng</button>
                    	</div>
	                </div>
	            </div>
        	</div>
        	<!--/ko-->
        	<table class="table table-hover table-condensed" style="width: 800px; margin-bottom: 5px">
	            <thead>
	                <tr>
	                    <th style="width: 15%">Valt läkemedel</th>
	                    <th style="width: 15%"></th>
	                    <th style="width: 22%"></th>
	                    <th style="width: 40%"></th>
	                    <th style="width: 8%"></th>
	                </tr>
	            </thead>
	            <tbody data-bind="foreach: table()">
	                <tr>
						<td data-bind="text: lakemedel() && lakemedel().text"></td>
					    <td>
					    	<!-- ko if: $parent.registrering() && $parent.registrering().value == '1' && lakemedel() && (lakemedel().value == 'N07BC01' || lakemedel().value == 'N07BC02' || lakemedel().value == 'N07BC05' || lakemedel().value == 'V03AB15' || lakemedel().value == 'N07BB04' || lakemedel().value == 'N07BC51') -->
						        <b>Dos (mg)</b> <input class="ctrl" style="width: 40px" type="text" data-bind="rcc-value: dos"/><br />
						        <!-- ko if: dos.rcc.accessed() -->
							        <!-- ko foreach: dos.rcc.validation.errors() -->
							        	<span class="fa fa-exclamation-triangle" style="color: #ffd351"></span>
										<span style="font-size: 11px" data-bind="text: message"></span>
									<!--/ko-->
								<!--/ko-->
							<!--/ko-->
					    </td>
					    <td>
					    	<!-- ko if: $parent.registrering() && $parent.registrering().value == '1' && lakemedel() && (lakemedel().value == 'N07BC01' || lakemedel().value == 'N07BC02' || lakemedel().value == 'N07BC05' || lakemedel().value == 'V03AB15' || lakemedel().value == 'N07BB04' || lakemedel().value == 'N07BC51') -->
						    	<b>Dosform </b><select class="ctrl" data-bind="rcc-list: dosform, optionsCaption: '– Välj –', optionsText: 'text'"></select>
						    	<!-- ko if: dosform.rcc.accessed() -->
								    <!-- ko foreach: dosform.rcc.validation.errors() -->
								        	<span class="fa" data-bind="css: { 'fa-exclamation-triangle': !fatal, 'fa-exclamation-circle': fatal }, style: { color: fatal ? 'red' : '#ffd351' }"></span>
											<span style="font-size: 11px" data-bind="text: message"></span>
									<!--/ko-->
								<!--/ko-->
							<!--/ko-->
					    </td>
	                    <td>
	                    	<!-- ko if: $parent.registrering() && $parent.registrering().value == '1' && lakemedel() && (lakemedel().value == 'N07BC01' || lakemedel().value == 'N07BC02' || lakemedel().value == 'N07BC05' || lakemedel().value == 'V03AB15' || lakemedel().value == 'N07BB04' || lakemedel().value == 'N07BC51') -->
		                    	<b>Beredningsform </b><select class="ctrl" data-bind="rcc-list: beredningsform, optionsCaption: '– Välj –', optionsText: 'text'"></select><br />
		                    	<!-- ko if: beredningsform.rcc.accessed() -->
							        <!-- ko foreach: beredningsform.rcc.validation.errors() -->
							        	<span class="fa" data-bind="css: { 'fa-exclamation-triangle': !fatal, 'fa-exclamation-circle': fatal }, style: { color: fatal ? 'red' : '#ffd351' }"></span>
										<span style="font-size: 11px" data-bind="text: message"></span>
									<!--/ko-->
								<!--/ko-->
							<!--/ko-->
						</td>
	                    <td><button class="btn btn-primary btn-xs btn-danger" data-bind="enable: !$root.$form.isReadOnly, click: $parent.table.rcc.remove">Ta bort</button></td>
	                </tr>
	            </tbody>
        	</table>
        	<em data-bind="visible: table().length == 0">Inga valda läkemedel.</em><br />
        	<button data-bind="enable:!$root.$form.isReadOnly, click: function() { visible(true); }" class="btn btn-primary btn-xs" style="background-color: #330855; margin-top: 5px">
	            <span>Lägg till</span>
	        </button>
		*/return undefined;})
	});
})(window['inca'], window['RCC'], window['ko'], window['_']);
