(function ( inca, ko, window )
{
/**
 * @namespace
 */
var ICD10 = {};

/**
 * Name of the value domains.
 * @private
 */
ICD10.valueDomainNames =
{
	code: 'VD_ICD10_Kod',
	chapters: 'VD_ICD10_Kapitel',
	chapterCodes: 'VD_ICD10_Kapitelkoder'
};

/**
 * Cached codes; id is key and value is the object that
 * ICD10.informationForId returns.
 * @private
 */
ICD10._cache = {};

/**
 * Return an object containing observables describing the code with the given id.
 * @param id {Number} The number.
 * @returns {Object} An object with properties (ko.observable:s)
 *		- text: human readable representation
 *		- error: true if an error occurred and false if not, undefined if pending.
 *		- pending: true if the request is pending, false otherwise.
 *		- [obj]: raw data object from value domain
 */
ICD10.informationForId =
function ( id )
{
	if ( !ICD10._cache.hasOwnProperty(id) )
	{
		var info =
		{
			text: ko.observable('Laddar...'),
			error: ko.observable(undefined),
			pending: ko.observable(true),
			obj: ko.observable(undefined)
		};

		ICD10._cache[id] = info;

		inca.form.getValueDomainValues(
			{
				vdlist: ICD10.valueDomainNames.code,
				parameters: { kod: id },
				success:
					function ( list )
					{
						info.pending(false);

						if ( list.length != 1 )
						{
							info.error(true);
							info.obj(undefined);
							info.text( 'Oväntat radantal (id ' + id + ', n = ' + list.length + ')' );
						}
						else
						{
							info.error(false);
							info.obj( list[0] );
							info.text( ICD10._formatCode(list[0]) );
						}
					},
				error:
					function ( err )
					{
						info.pending(false);
						info.error(true),
						info.obj(undefined);
						info.text( 'Oväntat fel (id ' + id + '): ' + err );
					}
			} );
	}
	
	return ICD10._cache[id];
};

/**
 * Return a human readable representation of a code object.
 *
 * @param row A single data row describing a code.
 * @returns {String} A human readable representation of the code.
 * @private
 */
ICD10._formatCode =
function ( row )
{
	return row.data.beskrivning + ' (' + row.data.kod + ')'
};

/**
 * Creates a view model suitable for rendering in template 'icd10-picker'.
 * @param opt {Object} Options.
 * @param opt.addCode Callback when a new code is added.
 * @param {String} [opt.defaultChapter] Default chapter to select.
 * @param {Function} [opt.manualFilter] Filter function that takes an array of codes and returns the filtered array.
 * @constructor
 */
ICD10.Picker =
function ( opt )
{
	var self = this;

	self.errors = ko.observableArray([]);
	self.addCode = opt.addCode;

	self.query = ko.observable('').extend( { throttle: 300 } );
	self.maxCodes = ko.observable(10);

	// [ { id: chapter-id, codes: observableArray-of-codes, data: column-data }, ... ]
	self.chapters = ko.observableArray();
	self.chapters(undefined);
	self._loadChapters( opt.defaultChapter );

	self.chapter = ko.observable(undefined);
	self.chapter.subscribe(
		function ( chapter )
		{
			if ( chapter && !chapter.codes() )
				self._loadCodesForSelectedChapter();

			self.query('');
			self.maxCodes(10);
		} );

	self.manualFilter = opt.manualFilter;

	self.filteredCodes = ko.computed( self._filteredCodes, self );
};

/**
 * Return the codes containing the search query. The results are
 * sorted ascending by the code.
 * @private
 */ 
ICD10.Picker.prototype._filteredCodes =
function ( )
{
	var self = this;

	if ( !self.chapter() )
		return undefined;

	var codes = self.chapter().codes();
	if ( !codes )
		return undefined;

	var query = self.query().toLowerCase();

	var filter = ko.utils.arrayFilter( codes,
		function ( code )
		{
			return (code.data.kod + code.data.beskrivning).toLowerCase().indexOf(query) >= 0;
		} ).sort(
			function ( a, b )
			{
				var aa = a.data.kod;
				var bb = b.data.kod;

				return aa > bb ? +1 : (aa < bb ? -1 : 0);
			} );

	return self.manualFilter ? self.manualFilter(filter) : filter;
};

/**
 * Attempts to load a value domain containing the available chapters.
 *
 * @param {String} [defaultChapter] Default chapter to select after loading list of chapters.
 * @private
 */
ICD10.Picker.prototype._loadChapters =
function ( defaultChapter )
{
	var self = this;

	inca.form.getValueDomainValues(
		{
			vdlist: ICD10.valueDomainNames.chapters,
			success:
				function ( list )
				{
					var chapters = 
						ko.utils.arrayMap( list,
							function ( chapter )
							{
								chapter.codes = ko.observableArray();
								chapter.codes(undefined);
								return chapter;
							} );
					chapters.sort(
							function ( a, b )
							{
								var aa = a.data.forstakod;
								var bb = b.data.forstakod;
		
								return aa > bb ? +1 : (aa < bb ? -1 : 0);
							} );
					self.chapters( chapters );
					if ( defaultChapter )
						self.chapter( ko.utils.arrayFirst( chapters, function ( chapter ) { return chapter.data.kapitel == defaultChapter; } ) );
				},
			error: function ( err ) { self.errors.push( 'Lyckades inte ladda kapitlen: ' + JSON.stringify(err) ); }
		} );
};

/**
 * Attempts to load a value domain containing the codes for the currently selected chapter.
 * Retrieved codes are saved to the cache.
 *
 * @private
 */
ICD10.Picker.prototype._loadCodesForSelectedChapter =
function ( )
{
	var self = this;
	var chapterId = self.chapter().id;
	inca.form.getValueDomainValues(
		{
			vdlist: ICD10.valueDomainNames.chapterCodes,
			parameters: { kapitel: chapterId },
			success:
				function ( list )
				{
					self.chapter().codes( list );
					// Populate the cache.
					ko.utils.arrayForEach( list,
						function ( row )
						{
							if ( ICD10._cache.hasOwnProperty(row.id) )
								return;

							ICD10._cache[row.id] =
							{
								text: ko.observable( ICD10._formatCode(row) ),
								error: ko.observable(false),
								pending: ko.observable(false),
								obj: ko.observable(row)
							};
						} );
				},
			error: function ( err ) { self.errors.push( 'Lyckades inte ladda koder för kapitel med id ' + chapterId + ': ' + JSON.stringify(err) ); }
		} );
};

window.ICD10 = ICD10;

})( inca, ko, window );
