(function(window, _) {	
	if (!window.RCC) window.RCC = {};
	if (!window.RCC.Vast) window.RCC.Vast = {};
	if (!window.RCC.Vast.Psy) window.RCC.Vast.Psy = {};

	window.RCC.Vast.Psy.DefaultForm = new function DefaultForm() {
		var self = this;

		self.selectFollowUpRegistration = function(vm) {
            var firstUppf = _.find(vm.$$.Uppföljning(), function(x) { return x.informationsdatum() == vm.inregistreringsdatum(); });
            if (firstUppf) {
	            // Väv in nyregistreringsuppgifterna med vald uppföljning...
	            firstUppf.Nyreg = vm;
	            // ... och synka uppföljningens informationsdatum till nyregistreringens inregistreringsdatum.
	            firstUppf.informationsdatum.subscribe(function (date) { vm.inregistreringsdatum(date); });
        	}
        	return firstUppf;
		};
	};
})(window, window['_']);