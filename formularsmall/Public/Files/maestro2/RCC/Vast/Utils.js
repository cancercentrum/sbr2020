(function ( inca, window )
{
	var Utils = {};

	if ( !window.RCC )
		window.RCC = {};
	if ( !window.RCC.Vast )
		window.RCC.Vast = {};
	if ( !window.RCC.Vast.Utils )
		window.RCC.Vast.Utils = Utils;

	/**
	 * Beräknar antalet "fyllda år" för en person, där någon född på skottdagen
	 * anses fylla år på skottdagen vid skottår och annars den 1 mars.
	 *
	 * @param {Date} dateOfBirth Födelsedatum, i lokala tidszonen.
	 * @param {Date} date Datum vid vilken åldern ska beräknas, i lokala tidszonen.
	 * @returns {Integer} Antal fyllda år.
	 */
	Utils.calculateAge = function ( dateOfBirth, date )
	{
		if ( date.getTime() < dateOfBirth.getTime() )
			return undefined;

		function y ( date ) { return date.getFullYear(); };
		function m ( date ) { return date.getMonth(); };
		function d ( date ) { return date.getDate(); };

		// Beräkna först åldern vid årsslut (31 december).
		var age = y(date) - y(dateOfBirth);

		// Dra bort 1 år om aktuellt års födelsedag inte passerats. (Avgör hur skottårspersoner hanteras.)
		if ( m(date) < m(dateOfBirth) || (m(date) == m(dateOfBirth) && d(date) < d(dateOfBirth)) )
			age -= 1;

		return age;
	};

	/**
	 * Raderar alla rader i en tabell.
	 * @param table Tabell vars rader ska raderas.
	 */
    Utils.removeAllRows = function ( table )
    {
        $.each( table().slice(0), function (i, o)
        {
            table.rcc.remove(o);
        });

    };

	/**
	 * Radera eller lägg till rader i undertabeller baserat på en variabels värde.
	 * Samma undertabell får inte hanteras i flera villkor.
	 *
	 * @param row Raden vars undertabeller ska administreras.
	 * @param currentValue Värdet som villkoren ska jämföras med. En ko.subscribable unwrappas 
	 *				först, och ett objekt reduceras till värdet av egenskapen "value".
	 * @param {Object[]} conditions Villkor av typen { table, value }, där tabellen table töms
	 *				om (i) aktuella värdet är skilt (!==) från value, eller (ii) om value är en array
	 *				skilt (!==) från varje element i arrayn, eller (iii) om value är en funktion och
	 *				denna funktion returnerar sant när den anropas med currentValue som argument.
	 *				I annat fall tillgodoses att tabellen har minst en rad.
	 */
	Utils.subscribeHelper = function ( row, currentValue, conditions )
	{
		// Unwrap observables and objects.
		currentValue = ko.utils.unwrapObservable(currentValue);
		if ( typeof currentValue == 'object' && currentValue !== null )
			currentValue = currentValue.value;

		$.each( conditions,
			function ( unused, condition )
			{
				var table = row.$$[condition.table];
				var shouldDelete;
				if ( $.isFunction(condition.value) )
					shouldDelete = !condition.value(currentValue, row);
				else
					shouldDelete = ($.inArray( currentValue, [].concat(condition.value) ) < 0);

				if ( shouldDelete )
					RCC.Vast.Utils.removeAllRows(table);
				else if ( table().length == 0 )
					table.rcc.add();
			} );
	};
	
	/**
	* Lägg till en rad i given tabell om denna saknar rader.
	*
	* @param table Tabellen vars rader ska administreras (en ko.observableArray med egenskap "rcc").
	* @returns {undefined|Object} Raden som lades till, eller undefined om ingen rad lades till.
	*/
	Utils.addRowIfEmpty = function ( table ) {
		if (table().length == 0) {
			return table.rcc.add();
		}
	};


	/**
	 * Tar fram vilken tabell som är rottabell, utgående från given metadata.
	 *
 	 * @param {Object} [metadata=inca.form.metadata] Metadataobjektet.
	 * @returns {String|undefined} Rottabellens namn, eller undefined om en unik rottabell 
	 * 			inte kan bestämmas.
	 */
	Utils.rootTable = function ( metadata )
	{
		if ( !metadata )
			metadata = inca.form.metadata;

		var tables = [], isSubTable = {};
		for ( var table in metadata )
		{
			if ( metadata.hasOwnProperty(table) )
			{
				tables.push( table );

				var subTables = metadata[table].subTables;
				for ( var i = 0; i < subTables.length; i++ )
					isSubTable[ subTables[i] ] = true;
			}
		}

		var rootTables = ko.utils.arrayFilter( tables, function ( table ) { return !isSubTable[table]; } );

		return rootTables.length == 1 ? rootTables[0] : undefined;
	};

	Utils.SubTableConditionHandler = function(rowAddedEventHandler, conditions) {

		// Sortera in villkor i en tabellstruktur och få ett objekt som visar vilka tabeller som ska få ett nytt rowAdded-event.
		var n_cond = {}
		$.each(inca.form.metadata, function (tableName, tableObj) {
			$.each(tableObj.regvars, function (regvarName) {
				if (conditions[regvarName]) {
					if (!n_cond[tableName]) { n_cond[tableName] = []; }
					n_cond[tableName].push({ name: regvarName, conditions: $.isFunction(conditions[regvarName]) ? conditions[regvarName]() : conditions[regvarName] });
				}
			});
		});

		// Skapa rowAdded-event.
		$.each(n_cond, function (tableName, tableConditions) {
			function subTableConditionHandlerEvent(row) {
				$.each(tableConditions, function (i, condition) {
					row[condition.name].subscribe(function () {
						Utils.subscribeHelper(row, row[condition.name], condition.conditions);
					});
				});
			}

			if (rowAddedEventHandler[tableName]) {
				rowAddedEventHandler[tableName] = [].concat(rowAddedEventHandler[tableName], subTableConditionHandlerEvent);
			} else {
				rowAddedEventHandler[tableName] = subTableConditionHandlerEvent;
			}
		});

		/**
		* Hämtar värdet på ett villkor.
		*
		* @param {String} regvarName Namn på egenskap.
		* @param {String} tableName Namn på tabellen där raderna hanteras av subscribeHelper.
		* @returns {String|String[]} Värdet av villkoret.
		*/

		this.getConditionValue = function (regvarName, tableName) {
			var rval;
			var regvarConditions = $.isFunction(conditions[regvarName]) ? conditions[regvarName]() : conditions[regvarName];
			$.each(regvarConditions, function (i, o) {
				if (o.table == tableName) {
					rval = o.value;
					return false;
				}
			});
			return rval;
		};

	};

	/**
	* Returnerar en array med samtliga value-egenskaper i angiven listvariabel.
	* @param {String} varName Namn på listvariabel.
	* @param {String|String[]} excludeValue Värde/värden som ska exkluderas.
	*/

	Utils.getListValuesArray = function(varName, excludeValue)
	{
		var listValues = (function() {
			var rv = null;
			$.each(inca.form.metadata, function(n, o) {
				if (rv) return false;
				$.each(o.regvars, function(nn, oo) {
					if (varName == nn)
					{
						rv = o.terms[oo.term].listValues;
						return false;
					}
				});
			});
			return rv;
		})();

		if (!listValues) throw new Error('List values for ' + varName + ' not found.');
		
		var rv = [];
		excludeValue = (excludeValue ? [].concat(excludeValue) : []);

		$.each(listValues, function(i, o) {
			if ($.inArray(o.value, excludeValue) == -1)
			{
				rv.push(o.value)
			}
		});

		return rv;
	};

	/**
	 * Markera given observable som "required" och "fatal". Felets dataobjekt innehåller
	 * objekt med egenskap "canBeSaved" satt till true.
	 *
	 * @param {ko.observable} Observable från en RCC.ViewModel, med .rcc.validation-egenskap.
	 */

	Utils.markAsFatal = function(observable) {
		observable.rcc.validation.required({ fatal: true, data: { canBeSaved: true } });
	};

	/**
	 * Lägg till validering som kontrollerar att ett numeriskt värde ligger inom
	 * givna gränser, inklusivt. Valideringen görs endast om det verkligen är ett
	 * giltigt numeriskt värde.
	 * 
	 * @param {ko.observable} observable Observable från en RCC.ViewModel, med .rcc.validation-egenskap.
	 * @param {Number} min Minsta tillåtna värde.
	 * @param {Number} max Största tillåtna värde.
	 * @param {Boolean} [isFatal=false] Om sant markeras felet som "fatal".
	 */

	Utils.addNumMinMaxValidation = function(observable, min, max, isFatal) {
		observable.rcc.validation.add(function () {
			var v = observable();
			if (typeof v == 'string')
				v = parseFloat(v.replace(/,/g, '.'));
			isFatal = !!isFatal;

			// Annan validering fångar felaktiga numeriska värden.
			if (v === undefined || v === null || isNaN(v) || !isFinite(v))
				return;
			else if (v > max)
				return { result: 'failed', message: 'Största tillåtna värde: ' + max.toString().replace('.', ','), fatal: isFatal };
			else if (v < min)
				return { result: 'failed', message: 'Minsta tillåtna värde: ' + min.toString().replace('.', ','), fatal: isFatal };
			else
				return;
		});
	};


	/**
	 * Lägg till validering, markerad som "fatal", som kontrollerar att en 
	 * observable innehåller ett värde som "rimligen" representerar ett årtal.
	 * @param {ko.observable} Observable från en RCC.ViewModel,  med .rcc.validation-egenskap.
	 */

	Utils.addYearValidation = function(observable) {
		observable.rcc.validation.add(function () {
			var v = observable();
			if (v !== null && v !== undefined && v !== '') {
				if (!(v + '').match(/^\d{4}$/)) {
					return { result: 'failed', message: 'Felaktigt årtal', fatal: true };
				}
			}
		});

	};

	/**
	 * Returnera en funktion som kan användas som inca.on('validation')-callback.
	 *
	 * @param {RCC.Validation} validation Valideringsobjektet.
	 * @param {Object} [actions={}] Åtgärder.
	 * @param {String[]} [actions.abort=[]] Åtgärder som inte kräver någon validering.
	 * @param {String[]} [actions.pause=[]] Åtgärder som inte kräver strikt validering.
	 * @returns {Function} Funktion som kan användas som inca.on('validation')-callback.
	 */
	Utils.getFormValidator = function ( validation, actions ) {
		if ( !actions ) actions = {};
		if ( !actions.pause ) actions.pause = [];
		if ( !actions.abort ) actions.abort = [];

		return function ( ) {
			var selectedAction = (function() {
				if (inca.errand && inca.errand.action && inca.errand.action.find)
					return inca.errand.action.find(':selected').text();
			})();

			if ($.inArray(selectedAction, actions.abort) >= 0)
				return true;

			var errors = validation.errors();

			if (errors.length > 0) {
				validation.markAllAsAccessed();

				var isPause = ($.inArray(selectedAction, actions.pause) >= 0);
				var fatalErrors = $.grep(errors,
					function (error) {
						if (isPause && error.data && error.data.canBeSaved )
							return false;
						else
							return error.fatal;
					});

				if (fatalErrors.length > 0) {
					alert("Formuläret innehåller " + fatalErrors.length + " fel.");
					return false;
				} else if (isPause && fatalErrors.length == 0) return true;

			   return confirm('Valideringen av frågosvaren gav ' + errors.length + ' varning' + (errors.length == 1 ? '' : 'ar') + '.\r\nVill du gå vidare ändå?');
			}

			return true;
		};
	};

	/**
	 * Extraherar innehållet av den första kommentaren i funktionens kod.
	 *
	 * @param {Function} func Funktion som innehåller utkommenterad kod.
	 * @returns {String}
	 */
	Utils.extractSource = function(func) {
		var m = func.toString().match(/\/\*(?:\*\s*@(?:preserve|license))?([\s\S]*?)\*\//);
		return m && m[1];
	};
	
	/**
	 * Knockouts metod för att detektera version av Internet Explorer. Från knockout-3.2.0.debug.js
	 *
	 * @returns {Number|undefined}
	 */
    Utils.ieVersion = document && (function() {
        var version = 3, div = document.createElement('div'), iElems = div.getElementsByTagName('i');

        // Keep constructing conditional HTML blocks until we hit one that resolves to an empty fragment
        while (
            div.innerHTML = '<!--[if gt IE ' + (++version) + ']><i></i><![endif]-->',
            iElems[0]
        ) {}
        return version > 4 ? version : undefined;
    }());

})( window['inca'], window );
