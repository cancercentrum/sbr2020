var setPopover = function (element, options) {
    if (options) {
        var o = _.defaults(options, {
            trigger: 'focus'
        });
        $(element).popover(o);
    }
};

window.ko.bindingHandlers.popover = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        if (valueAccessor && valueAccessor()) {
            var val = valueAccessor();
            if (ko.isObservable(val)) {
                val.subscribe(function () {
                    setPopover(element, val());
                });
                setPopover(element, val());
            } else {
                setPopover(element, val);
            }
        }
    }
};
