/**
*
*/

(function(inca, RCC, ko, _) {
	ko.components.register('sjukhus', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
			var self = this;
			self.inrappEnhet = params.inrappEnhet;
			self.sjukhusKod = params.sjukhusKod;
			self.klinikKod = params.klinikKod;
			self.vdList = params.vdList;
            self.required = params.required;
            self.vdListArr = params.vdListArr;
            self.patCyt = params.patCyt;
            self.inrapp = params.inrapp;
            self.indrag = params.indrag || false;
            self.parseCodes = function(code) {
				if(!self.inrappEnhet.regvar() || !code) 
					return;
				var splitCodes = code.split('-');
				if(self.sjukhusKod && splitCodes[1])
					self.sjukhusKod.regvar(splitCodes[1].trim());
				if(self.klinikKod && splitCodes[2])
					self.klinikKod.regvar(splitCodes[2].trim());
			};


			self.filterInrapp  = function() {
				self.autoComplete.data(_.filter(self.autoComplete.data(), function(obj) {
					var posArr = obj.data.PosCode.split(" - ");
					return posArr && posArr.length === 3;
				}));
			};


			self.filterPatCyt = function() {
				self.autoComplete.data(_.filter(self.autoComplete.data(), function(obj) {
					var posArr = obj.data.PosCode.split(" - ");
					return posArr[1] && posArr[1].length === 3;
				}));
			};
			self.filterDemoPosition = function() {
				self.autoComplete.data(_.filter(self.autoComplete.data(), function(obj) {
					var posArr = obj.data.PosCode.split(" - ");
					return posArr[0] != '0' && posArr.length > 1;
				}));
			};

			self.autoComplete = {
				searchValueObservable: ko.observable(''),
				filter: function(x) {					
					return RCC.AutoCompleteFilters.containsEachWord(this.searchValueObservable(), x.data.PosNameWithCode);
				},

				format: function(x) {
					return '<b>' + x.data.PosNameWithCode + '</b> ';
				},

				selectRow: function(x) {
					self.inrappEnhet.regvar(x.data.data.PosNameWithCode);
					self.parseCodes(x.data.data.PosCode);
					this.searchValueObservable(self.inrappEnhet.regvar());
				},

				data: ko.observable(undefined),
				selectOnTab: true
			};

			ko.computed(function() {
				self.autoComplete.data(self.vdListArr());
				if(self.inrapp) 
					self.filterInrapp();
				if(self.patCyt) 
					self.filterPatCyt();
				if(inca.user.region.name != 'Region Demo')
					self.filterDemoPosition();
				if(self.inrappEnhet.regvar()) self.autoComplete.searchValueObservable(self.inrappEnhet.regvar());
				var res = _.find(self.autoComplete.data(), function(obj) {
					return self.inrappEnhet.regvar() && self.inrappEnhet.regvar() == obj.data.PosNameWithCode;
				});
				if(res) self.parseCodes(res.data.PosCode)
			});

			self.inrappEnhet.regvar.subscribe(function() {
				if(!self.inrappEnhet.regvar()) self.autoComplete.searchValueObservable(null);
			});

			self.hasErrors = ko.computed(function () {
				var errors = self.inrappEnhet.regvar && self.inrappEnhet.regvar.rcc && self.inrappEnhet.regvar.rcc.validation && self.inrappEnhet.regvar.rcc.validation.errors();
				return errors && errors.length;
			});

			self.hasFatalErrors = ko.computed(function () {
				var errors = self.inrappEnhet.regvar && self.inrappEnhet.regvar.rcc && self.inrappEnhet.regvar.rcc.validation && self.inrappEnhet.regvar.rcc.validation.errors();
				return !!(errors && errors.filter(function (e) { return e.fatal; }).length);
			});

			self.getHelpText = ko.computed(function () {
				var regvar = self.inrappEnhet.regvar && self.inrappEnhet.regvar.rcc && self.inrappEnhet.regvar.rcc.regvar;
				return regvar && regvar.helpText ?
					regvar.helpText : self.inrappEnhet.help ?
						self.inrappEnhet.help : regvar.description ?
							regvar.description : '';
			});

			self.helpPopover = ko.pureComputed(function() {
				var helpText = self.getHelpText();
                return helpText ? {
                        title: undefined,
                        content:  helpText,
                        trigger: 'hover',
                        html: true
                    } : null;
            });
		},
		template: RCC.Vast.Utils.extractSource(function() {/**@preserve
			<div class="single-answer-template r" data-bind="style: { 'margin-left': indrag ? '20px' : '0px' }">
				<label data-bind="css: { 'accessed': inrappEnhet.regvar.rcc && inrappEnhet.regvar.rcc.accessed(), 'errors': hasErrors, 'fatal': hasFatalErrors }">
		 			<!-- ko if: !!getHelpText() -->
						<span class="question" data-bind="popover: helpPopover, html: (inrappEnhet.regvar.rcc && inrappEnhet.regvar.rcc.regvar && inrappEnhet.regvar.rcc.regvar.label || inrappEnhet.label) + ' <i class=\'fa fa-info-circle\'></i>'"><span></span></span>
		 			<!--/ko-->
		 			<!-- ko ifnot: !!getHelpText() -->
		 				<span class="question" data-bind="popover: helpPopover, html: (inrappEnhet.regvar.rcc && inrappEnhet.regvar.rcc.regvar && inrappEnhet.regvar.rcc.regvar.label || inrappEnhet.label)"><span></span></span>
		 			<!--/ko-->
					<input class="form-control" style="display: block; width: 560px; margin-top: 5px" type="text" data-bind="enable: inrappEnhet.regvar.rcc.enabled, rcc-autocomplete: autoComplete, value: autoComplete.searchValueObservable, rcc-var: inrappEnhet.regvar, valueUpdate: 'afterkeydown'" />
					<!-- ko if: hasErrors -->
						<div class="error-list" data-bind="foreach: inrappEnhet.regvar.rcc.validation.errors" >
							<span class="error-item" data-bind="css: { 'non-fatal': !fatal, 'fatal': fatal }">
								<span class="fa" data-bind="css: { 'fa-exclamation-triangle': !fatal, 'fa-exclamation-circle': fatal }"></span>
								<span data-bind="text: message"></span>
							</span>
						</div>
					<!--/ko-->
				</label>
			</div>
			<!-- ko if: inca.user.role.isReviewer -->
				<div style="color: mediumblue">
				<!-- ko if: sjukhusKod && ko.isObservable(sjukhusKod.regvar) -->
					<s-a params="question: sjukhusKod.label, value: sjukhusKod.regvar, help: sjukhusKod.help"></s-a>
				<!-- /ko -->
				<!-- ko if: klinikKod && ko.isObservable(klinikKod.regvar) -->
					<s-a params="question: klinikKod.label, value: klinikKod.regvar, help: klinikKod.help"></s-a>
				<!-- /ko -->
				</div>
			<!--/ko -->
		*/return undefined;})
	});
})(window['inca'], window['RCC'], window['ko'], window['_']);
