/**
* AutoComplete
* Användning:
*   data-bind="rcc-autocomplete: { @param: värde }"
*
* @param {Object[]} data Data som ska användas för automatisk komplettering.
* @param {function(Object)} filter Anger huruvida den aktuella raden i 'data' ska ingå i filtreringen. Funktionen ska returnera ett boolean-värde.
* @param {function(AutoCompleteRow)} selectRow Funktion som anropas när en rad har valts av användaren.
* @param {function(Object)} [format] Anger hur den aktuella raden i 'data' ska visas genom att returnera en sträng.
* @param {Number|Object} [visibleRowCount=10] Anger antalet synliga rader samt hur många nya rader som ska visas när man väljer 'Visa fler'. Kan även ta emot ett objekt med egenskaperna 'inital' och 'delta'.
* @param {Boolean} [hideOnSelectRow=true] Anger huruvida komponenten ska döljas när en rad har valts av användaren.
* @param {Boolean} [showOnFocus=true] Anger huruvuda komponenten ska visas när textfältet får fokus.
* @param {Boolean} [hideOnBlur=true] Anger huruvida komponenten ska döljas när textfältet tappar fokus.
* @param {Boolean} [hideOnEsc=true] Anger huruvida komponenten ska döljas när användaren trycker på Escape.
* @param {Boolean} [autoMarkFirstRow=false] Markerar automatiskt första raden i resultatet.
* @param {Boolean} [selectOnTab=false] Väljer markerad rad när användaren trycker på Tab.
*/
(function ($, _, RCC, ko) {
	function AutoCompleteViewModel(valueAccessor, element) {
		var va = valueAccessor(),
			self = this,
			_options = _.defaults(_.clone(va), {
				format: function(o) { return o; },
				hideOnSelectRow: true,
				hideOnBlur: true,
				hideOnEsc: true,
				showOnFocus: true,
				visibleRowCount: { initial: 10, delta: 10 },
				autoMarkFirstRow: false,
				selectOnTab: false
			});

		if (!isNaN(_options.visibleRowCount))
			_options.visibleRowCount = { initial: _options.visibleRowCount, delta: _options.visibleRowCount };

		function AutoCompleteRow(data, formatted) {
			var me = this;
			me.data = data;
			me.formatted = formatted;
			me.active = ko.observable(false);
			me.select = function() {
				_options.selectRow(me);
				if (_options.hideOnSelectRow) self.visible(false);
			};
		}

		self.visibleRowCount = ko.observable(_options.visibleRowCount.initial);
		self.visible = ko.observable(false);
		self.showMoreVisible = ko.observable(false);
		self.showMoreEvent = {
			mouseover: function() { self.visibleRowCount(self.visibleRowCount() + _options.visibleRowCount.delta); }
		};

		self.filteredData = ko.computed(function() {
			var r = _.map(_.filter(ko.unwrap(_options.data), function(x) { return _options.filter(x) === true; }),
				function(x, index) { return new AutoCompleteRow(x, _options.format(x)); });

			self.visible(r.length > 0);
			if (_options.autoMarkFirstRow && r.length > 0) r[0].active(true);
			return r;
		});

		self.filteredData.subscribe(
			function ( ) {
				self.visibleRowCount(_options.visibleRowCount.initial);
			} );
		
		var keyCodes = {
			ArrowUp: 38,
			ArrowDown: 40,
			Enter: 13,
			Escape: 27,
			Tab: 9
		};

		var elementEvent = {};

		elementEvent.keydown = function(a, b) {

			if (!_.contains(keyCodes, b.keyCode) || b.metaKey || b.shiftKey || b.altKey || b.ctrlKey) {
				return true; 
			}

			var col = self.filteredData();
			if (col.length == 0) return true;

			var useDefaultAction = false;

			function selectActiveRow() {
				if (self.visible()) {
					var active = _.find(col, function(x) { return x.active(); });
					if (active) active.select();
				}
			}

			switch(b.keyCode) {
				case keyCodes.ArrowDown:
				case keyCodes.ArrowUp:
					var activeIndex = _.indexOf(_.invoke(col, 'active'), true),
						newIndex = 0,
						l = Math.min(self.visibleRowCount(), col.length);
				
					if (activeIndex == -1) newIndex = (b.keyCode == keyCodes.ArrowDown ? 0 : l - 1);
					else {
						col[activeIndex].active(false);
						newIndex = ((activeIndex + l) + (b.keyCode == keyCodes.ArrowDown ? 1 : -1)) % l;
					}
					col[newIndex].active(true);
					break;
				case keyCodes.Enter:
					selectActiveRow();
					break;
				case keyCodes.Tab:
					if (_options.selectOnTab) selectActiveRow();
					useDefaultAction = true;
					break;
				case keyCodes.Escape:
					if (_options.hideOnEsc) self.visible(false);
					break;
			}

			return useDefaultAction;
		};

		if (_options.hideOnBlur) elementEvent.blur = function() { self.visible(false); }
		if (_options.showOnFocus) elementEvent.focus = function() { if (self.filteredData().length > 0) self.visible(true); }
		
		ko.applyBindingsToNode(element, { 'event': elementEvent });
	}

	if (!RCC.Vast.Components) RCC.Vast.Components = {};
	RCC.Vast.Components.AutoComplete = {
		Filters: {
			containsEachWord: function(searchFor, searchIn) {
				if (searchFor && searchIn) {
					searchIn = searchIn.toLowerCase();
					return _.every(searchFor.toLowerCase().replace(/^\s+|\s+$/g, '').split(/\s+/),
						function(s) {
							var containsWord = (searchIn.indexOf(s.replace(/^-/,'')) != -1);
							return s.match(/^-/) ? !containsWord || s.length == 1 : containsWord;
						});
				}
				return false;
			}
		}
	};
	RCC.AutoCompleteFilters = RCC.Vast.Components.AutoComplete.Filters;

	$(function () {
		$('head').append(RCC.Vast.Utils.extractSource(function() {/**@preserve
			<script type="text/html" id="rcc-autocomplete">
		
				<div class="rcc-autocomplete-inner" data-bind="visible: visible">
					<div class="items" data-bind="foreach: filteredData().slice(0, visibleRowCount())">
						<div data-bind="html: formatted, css: { 'active': active }, event: { mousedown: select } "></div>
					</div>
					<div class="show-more" data-bind="visible: filteredData().length > visibleRowCount(), event: showMoreEvent">Visa fler...</div>
				
				</div>

			</script>
		*/}));
	});

	ko.bindingHandlers[RCC.bindingsPrefix + 'autocomplete'] = {
		init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
			ko.applyBindingsToNode($('<div></div>').addClass('rcc-autocomplete').insertAfter(element)[0], { 'template': { name: 'rcc-autocomplete' } }, new AutoCompleteViewModel(valueAccessor, element));
		}
	};

})(jQuery, _, RCC, ko);
