(function() {

	if (queryString.birthDate) {
		var birthDateInt = parseInt(queryString.birthDate);
		if (!isNaN(birthDateInt)) {
			inca.form.env._FODELSEDATUM = RCC.Utils.ISO8601(new Date(birthDateInt));
		}
	}

	inca.user.role.isReviewer = queryString.reviewer && queryString.reviewer === 'true' ? true : false;
	
	inca.form.env._SEX = (queryString.gender == 'man' ? 'M' : 'F');
	inca.user.position.id = parseInt(queryString.formType);

	$(function () {
		$("body").prepend($('<button class="btn btn-primary" style="background-color: #330855">Skicka</button>').click(function ( ) { SendForm(); }));
	});
})();