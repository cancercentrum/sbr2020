window.inca = {
    offline: true,
    user: {
        firstName: 'Test',
        lastName: 'Testsson',
        role: { isReviewer: false },
        region: { id: -1, name: 'Väst' },
        position: { id: -1, fullNameWithCode: 'OC Demo (0) - SU/Sahlgrenska (500010) - Medicinkliniken (501)' }
    },
    on: function () {
    },
    form:
        {
            isReadOnly: false,
            getRegisterRecordData:
                function () {
                    throw new Error('unimplemented');
                },
            getValueDomainValues:
                function (opt) {
                    if (opt.success) {
                        setTimeout(
                            function () {
                                switch (opt.vdlist) {
                                    case 'VD_ICD10_Kapitel':
                                        opt.success(recording.getValueDomainValues["[[\"error\",null],[\"success\",null],[\"vdlist\",\"VD_ICD10_Kapitel\"]]"][0]);
                                    break;
                                    case 'VD_ICD10_Kapitelkoder':
                                        opt.success(recording.getValueDomainValues["[[\"error\",null],[\"parameters\",[[\"kapitel\"," + opt.parameters.kapitel + "]]],[\"success\",null],[\"vdlist\",\"VD_ICD10_Kapitelkoder\"]]"][0]);
                                    break;
                                }
                            }, (1 + Math.random()) * 250);
                    }
                }
        },
    errand: {
        status: {
            val: function () {
                return 'Nytt ärende';
            }
        }, action: {
            selectedText: function () {
                return 'Klar, sänd till RCC';
            }
        }
    },
    serverDate: new Date()
};

inca.form.env = $.extend({}, inca.form.env, { _DISTRICT: '20' });

