
var eventHandlers = {}, subTableConditions;

function getVal(regvar) {
    return regvar() ? regvar().value : undefined;
}

function setValue(obs, val) {
    obs(ko.utils.arrayFirst(obs.rcc.term.listValues, function (list) {
        return list.text === val || list.value === val;
    }));
}

function setCondition(condition, regvars) {
    _.each(regvars, function(regvar) {
        if(!condition) regvar(null);
        regvar.rcc.validation.required({ fatal: false, data: { canBeSaved: true } });
    });
    return condition;
}

function oneOf(regvar, arr) {
    try {
        return !!(regvar() && $.inArray(regvar().value, arr) >= 0);
    } catch(e) {
        console.error(e);
    }
}


(function() {

	var markAsFatal = RCC.Vast.Utils.markAsFatal,
		addNumMinMaxValidation = RCC.Vast.Utils.addNumMinMaxValidation,
		addYearValidation = RCC.Vast.Utils.addYearValidation,
		removeAllRows = RCC.Vast.Utils.removeAllRows,
		addRowIfEmpty = RCC.Vast.Utils.addRowIfEmpty;

    eventHandlers.rowAdded = {
		BehandlingUppfoljning: function(r) {

            r.showBeroendeValidation = ko.observable(false);

            if(inca.errand && inca.errand.status.val() == 'Nytt ärende') {
                var slutenvardId = [
                    1598,1644,1732,1734,1735,1749,1775,1776,1783,
                    1790,1955,1939,2120,2254,2266,2292,2362,2685,
                    2669,2738,2739,2733,2783,2912,2991,3011,3021
                ];
                var res = _.find(slutenvardId, function(unitId) { return unitId == inca.user.position.id; }); 
                setValue(r.formulartyp, res ? '2' : '1');
            }

            r.removeDiagnosRows = function(regvar, typ) {
                if(!regvar() || regvar().value != '1') {
                    $.each(r.$$.Diagnoser().slice(0), function (i, o) {
                        if(o.diagnostyp() && o.diagnostyp().value == typ)
                            r.$$.Diagnoser.rcc.remove(o);
                    });
                }
            }

            r.psykiatriskDiagnos.subscribe(function() { r.removeDiagnosRows(r.psykiatriskDiagnos, '2'); });
            r.somatiskDiagnos.subscribe(function() { r.removeDiagnosRows(r.somatiskDiagnos, '3'); }); 

            r.laroprogram.subscribe(function() { 
            if(r.laroprogram()&& r.laroprogram().value =='1')
                r.lakemedelVisible(true);  
            });


            r.lakemedelVisible = ko.observable(false);
            markAsFatal(r.formulartyp);
            markAsFatal(r.informationsdatum);
            markAsFatal(r.laroprogram);
        },
        
        Lakemedel: function(r) {
            r.beredningsform.rcc.validation.add(function() {
                var beredningsform = r.beredningsform();
                var lakemedel = r.lakemedel();
                if(!beredningsform || !lakemedel) return;
                if (lakemedel.value == 'N07BC02' && oneOf(r.beredningsform, ['1','2','4','7'])) { 
                    return { result: 'failed', message: 'Vald beredningsform är inte möjlig för läkemedlet', fatal: true };
                }
                else if (lakemedel.value == 'N07BC01' && oneOf(r.beredningsform, ['5','4','7'])) {
                    return { result: 'failed', message: 'Vald beredningsform är inte möjlig för läkemedlet', fatal: true };
                }
                else if (lakemedel.value == 'N07BC51' && oneOf(r.beredningsform, ['2','3','5','6','7'])) {
                    return { result: 'failed', message: 'Vald beredningsform är inte möjlig för läkemedlet', fatal: true };
                }
                else if (lakemedel.value == 'N07BC05' && oneOf(r.beredningsform, ['1','2','3','4','6','7'])) {
                    return { result: 'failed', message: 'Vald beredningsform är inte möjlig för läkemedlet', fatal: true };
                }
                else if (lakemedel.value == 'V03AB15' && oneOf(r.beredningsform, ['1','2','4','6'])) {
                    return { result: 'failed', message: 'Vald beredningsform är inte möjlig för läkemedlet', fatal: true };
                }
                else if (lakemedel.value == 'N07BB04' && oneOf(r.beredningsform, ['1','2','3','5','6','7'])) {
                    return { result: 'failed', message: 'Vald beredningsform är inte möjlig för läkemedlet', fatal: true };
                }
            });

            r.dos.subscribe(function() {
                if(r.dos()) {
                    var mvarDec = parseFloat(r.dos().replace(',', '.'));
                    if (!isNaN(mvarDec)) {
                        r.dos(mvarDec.toFixed(2).replace('.', ','));
                    }
                }
            });
        }
    };
})();

$(function () {
    var actions = { save: ['Spara'], abort: ['Avbryt', 'Radera'], pause: ['Pausa'] }; 
    try {
        var vm = new RCC.ViewModel({
            validation: new RCC.Validation(),
            events: eventHandlers
        });
        vm.showRegvarNames = ko.observable(false);
        vm.enableHelpText = ko.observable(true);
        vm.errors = ko.observableArray([]);
        var ready = ko.observable(false);
        vm.showForm = ko.computed(function () {return ready() && vm.errors().length == 0 }); 
        var SendForm = function () {
            var selectedAction = inca.errand && inca.errand.action.selectedText();
            if ($.inArray(selectedAction, actions.abort) >= 0) {
                return true;
            }
            
            var errors = vm.$validation.errors();
            if (errors.length > 0) {
            vm.$validation.markAllAsAccessed();
                var isPause = ($.inArray(selectedAction, actions.pause) >= 0);
                var fatalErrors = $.grep(errors, function (error) { return (isPause && error.data && error.data.canBeSaved) ? false : error.fatal; });
                if (fatalErrors.length > 0) {
                    alert("Formuläret innehåller " + fatalErrors.length + " fel.");
                    return false;
                }
                return true;
            }
            return true;
        };

       try{
            vm.ERB_user = JSON.stringify(inca.user);
        } catch (e) {
            console.error(e);
        }
        vm.ERB_patientId = inca.form.env._PATIENTID;
        vm.ERB_register = 'Bättre Beroendevård';
        vm.ERB_form = inca.form.name;
        
        inca.on('validation', SendForm);
        window.SendForm = SendForm;
        ko.applyBindings(vm);
        ready(true);
        window.vm = vm;
     }   
     catch (ex) {
        inca.on("validation", function () {
            if (inca.errand && inca.errand.action && $.inArray(inca.errand.action.find ? inca.errand.action.find(':selected').text() : undefined, actions.abort) != -1) return false;
        });
        alert("Ett fel inträffade\r\n\r\n" + ex + "\r\n" + JSON.stringify(ex));
    }
});
